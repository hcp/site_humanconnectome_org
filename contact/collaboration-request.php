<?php
//If the form is submitted
if(isset($_POST['submit'])) {

	//Check to make sure that the name field is not empty
	if(trim($_POST['contactname']) == '') {
		$hasError = 'empty';
	} else {
		$name = trim($_POST['contactname']);
	}

	//Check to make sure that the subject field is not empty
	if(trim($_POST['subject']) == '') {
		$hasError = 'empty';
	} else {
		$subject = "[Contact Form]: ".trim($_POST['subject']);
	}

	//Check to make sure sure that a valid email address is submitted
	if(trim($_POST['email']) == '')  {
		$hasError = 'empty';
	} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
		$hasError = 'email';
	} else {
		$email = trim($_POST['email']);
	}
	
	//Check to make sure that the institution field is not empty
	if(trim($_POST['institution']) == '') {
		$hasError = 'empty';
	} else {
		$institution = trim($_POST['institution']);
	}

	//Check to make sure comments were entered
	if(trim($_POST['message']) == '') {
		$hasError = 'empty';
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['message']));
		} else {
			$comments = trim($_POST['message']);
		}
	}
	
	//Check to make sure that the institution field is not empty
	if(trim($_POST['ReOT']) == '') {
		$hasError = 'empty';
	} else {
		/* create FOR loop that assigns email recipients according to OT selection(s). */ 
		$relevantOTs = explode (',',$_POST['ReOT']); 
	}

	//If there is no error, send the email
	if(!isset($hasError)) {
		$emailTo = 'info@humanconnectome.org'; //Put your own email address here
		$body = "Name: $name \n\nInstitution: #institution \n\nEmail: $email \n\nRequest Title: $subject \n\nRequest Details:\n $comments \n\nSelected OTs: $relevantOTs";
		$headers = 'From: no-reply@humanconnectome.org' . "\r\n" . 'Reply-To: ' . $email;

		mail($emailTo, $subject, $body, $headers);
		$emailSent = true;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/hcp-temp-main.dwt" codeOutsideHTMLIsLocked="false" --><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Contact Us | Human Connectome Project</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" src="http://dev.jquery.com/view/trunk/plugins/validate/jquery.validate.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#contactform").validate();
});
</script>
<!-- InstanceEndEditable -->
<link href='http://fonts.googleapis.com/css?family=Droid+Sans+Mono' rel='stylesheet' type='text/css'>
<link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/js/global.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.2.js"></script>
</head>

<body>
<!-- site container -->
<div id="site_container">

<div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div>

<!-- Header -->
<div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
  <map name="Header" id="Header">
    <area shape="rect" coords="14,7,404,123" href="/" alt="Human Connectome Project Logo" />
  </map>
</div> 
<!-- end Header -->

<!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" -->
<div id="topnav">
  <ul id="nav">
    <li><a href="/">Home</a></li>
    <li><a href="/about/">About the Project</a>
      <ul>
        <li><a href="/about/project/">Project Overview &amp; Components</a></li>
        <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
        <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
        <li><a href="/about/teams.html">Operational Teams</a></li>
        <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
        <li><a href="/about/publications.html">Publications</a></li>
        <li><a href="/courses">HCP Course Materials</a></li>
        <li><a href="/about/pressroom/">News &amp; Updates</a></li>
      </ul>
    </li>
    <li><a href="/data/">Data</a>
      <ul>
        <li><a href="/data/">Get Access to Public Data Releases</a></li>
        <li><a href="/data/connectome-in-a-box.html">Order Connectome in a Box</a></li>
        <li><a href="/data/data-use-terms/">HCP Data Use Terms</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Home" target="_blank">HCP Wiki: Data Resources</a></li>
      </ul>
    </li>
    <li><a href="/software/">Software</a>
      <ul>
        <li><a href="/software/connectome-workbench.html">What is Connectome Workbench</a></li>
        <li><a href="/software/get-connectome-workbench.html">Get Connectome Workbench</a></li>
        <li><a href="/software/workbench-command.php">Using Workbench Command</a></li>
        <li><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></li>
        <li><a href="/documentation/HCP-pipelines/index.html">HCP MR Pipelines</a></li>
        <li><a href="/documentation/HCP-pipelines/meg-pipeline.html">HCP MEG Pipelines</a></li>
      </ul>
    </li>
    <li><a href="/documentation/">Documentation</a>
      <ul>
      	<li><a href="/documentation/">All Documentation</a></li>
        <li><a href="/documentation/citations.html">How to Cite HCP</a></li>
        <li><a href="/documentation/tutorials/">Software Tutorials</a></li>
        <li><a href="/documentation/S900/">900 Subjects Data Release</a></li>
        <li><a href="/documentation/HCP-pipelines/">HCP Pipelines</a></li>
          <li><a href="/documentation/subject-key">How to Cite HCP Subjects with a Subject Key</a></li>
      </ul>
    </li>
    <li><a href="/contact/">Contact</a>
      <ul class="subnav">
      	<li><a href="/contact/">General Project Inquiries</a></li>
        <li><a href="/contact/#subscribe">Subscribe to Mailing Lists</a></li>
        <li><a href="/careers/">Career Opportunities</a></li>
        <li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
        <li> <a href="/contact/feature-request.php">Feature Request</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Media+Images+from+the+Human+Connectome+Project+WU-Minn+Consortium" target="_blank">HCP Wiki: Media Images</a></li>
      </ul>
    </li>
    <li><a href="/resources/">Other Resources</a>
      <ul class="subnav">
        <li><a href="http://wiki.humanconnectome.org/" target="_blank">HCP Wiki</a></li>
        <li><a href="http://lifespan.humanconnectome.org/" target="_blank">HCP Lifespan Pilot Project</a></li>
        <li><a href="/documentation/MGH-diffusion/">MGH Adult Diffusion Project</a></li>        
        <li><a href="/resources/faq-disease-related-connectome-research.html">FAQ: Disease-Related Connectome Research</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="search">
    <!--   If Javascript is not enabled, nothing will display here -->
    <script>
      (function() {
        var cx = '000073750279216385221:vnupk6whx-c';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      })();
    </script>
    <gcse:search></gcse:search>	
</div> <!-- Search box -->
<link rel="stylesheet" href="/css/google-cse-customizations.css" />
<!-- #EndLibraryItem --><!-- End Nav -->

<!-- Breadcrumbs -->
<div id="breadcrumbs">
  <p>&raquo; <!-- InstanceBeginEditable name="Breadcrumbs" --><a href="/contact/">contact us</a> &gt; collaborative opportunities<!-- InstanceEndEditable --></p></div>
<!-- end breadcrumbs -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">
  <div id="content" class="editable"> 
  <!-- InstanceBeginEditable name="Main Content" -->
    <h1>Collaborative Opportunities</h1>
    <p>The  WU-Minn HCP consortium aims to promote widespread data sharing and to maximize  the benefits of the Human Connectome Project to the neuroscience community.  This will entail extensive cooperation,  communication, and (in selected cases) collaboration with external  investigators who are not part of the WU-Minn consortium.  Investigators interested in exploring  opportunities along such lines should follow the guidelines summarized below. </p>

    <h3><strong>Ideas or  methods potentially relevant to the HCP</strong>.  </h3>
    <p>Investigators may have ideas for improvements in  data acquisition, analysis, and/or dissemination that might directly benefit  the WU-Minn HCP effort.  To explore such possibilities: </p>
    <ul type="disc">
      <li>Submit an <strong>HCP Collaboration Request</strong>, using the form at right. </li>
      <li>Be specific in describing the idea and how it might benefit the HCP.  </li>
    </ul>
    <h3><strong>Promoting  common data formats</strong>. </h3>
    <p>The HCP endorses the growing effort to promote  sharing of neuroimaging data using common data formats.  </p>
    <ul type="disc">
      <li>The HCP will use existing, widely-adopted data formats when feasible (e.g., NIFTI, GIFTI)</li>
      <li>As new data formats are developed (e.g., for connectivity matrices and other connectome-specific datasets), we will communicate and coordinate with other stakeholders in an effort to converge on common formats. Investigators interested in participating in a discussion group on HCP data formats should contact John Harwell using the form at right. </li>
      <li>As standard formats that will be used for HCP data emerge during Phase I (years 1 and 2), they will be made available on the OT7 (Informatics) section of the HCP website.</li>
    </ul>
    <h3><strong>Encouraging  standardized protocols</strong>.</h3>
    <ul type="disc">
      <li>Numerous protocols for data acquisition and analysis will be designed, implemented, and evaluated during Phase I.  The general strategies involved are summarized briefly in:  <em>About the Project: Project Overview and Components</em>.  These will be updated periodically as refinements occur.</li>
      <li>Specific protocols will be shared on the HCP website as they become finalized, near the end of Phase I. Investigators who would like to discuss HCP protocols under development (e.g., to explore possible synergies with other large-scale imaging projects) contact the appropriate Operations Team leader(s)<em>.  </em></li>
    </ul>
<p>&nbsp;</p>
<p>&nbsp;</p>

    <!-- Acknowledgement and thanks: PHP/JQuery contact page uses code writen by Raymond Selda: http://www.raymondselda.com/php-contact-form-with-jquery-validation/ --><!-- end contact-wrapper -->

<!-- InstanceEndEditable -->  
</div>

  <!-- Primary page Content -->

<div id="sidebar-rt">
    
        <div id="sidebar-content" class="editable">
        <!-- InstanceBeginEditable name="Sidebar Content" -->
        <div class="sidebox">
          <h2>Collaboration Request Form</h2>
          <p>(This is a rudimentary contact form that will open an email using your primary email application. You will have to complete and send this email. A fully-fledged contact form is in development.) </p>
          <p>&nbsp;</p>
          <h3>Collaboration Request</h3>
          <ul>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT1 Collaboration Request">OT 1: MR Hardware</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT2 Collaboration Request">OT 2: HARDI Analysis</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT3 Collaboration Request">OT 3: fMRI Analysis</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT4 Collaboration Request">OT 4: MEG/EEG</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT5 Collaboration Request">OT 5: Human Subjects</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT6 Collaboration Request">OT 6: Data Modeling</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT7 Collaboration Request">OT 7: Informatics</a></li>
          </ul>
          <p>&nbsp;</p>
          <h3>Promoting Data Formats</h3>
          <ul>
            <li><a href="mailto:john&#64;brainvis&#46;wustl&#46;edu?cc=info&#64;humanconnectome&#46;org?subject=[HCP Contact] Data Format Request">Contact Data Format Group</a></li>
          </ul>
          
          <p>&nbsp;</p>
	      <h3>Protocol  Discussion Request</h3>	
          <ul>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT1 Protocol Request">OT 1: MR Hardware</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT2 Protocol Request">OT 2: HARDI Analysis</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT3 Protocol Request">OT 3: fMRI Analysis</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT4 Protocol Request">OT 4: MEG/EEG</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT5 Protocol Request">OT 5: Human Subjects</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT6 Protocol Request">OT 6: Data Modeling</a></li>
            <li><a href="mailto:info&#64;humanconnectome&#46;org?subject=[HCP Contact] OT7 Protocol Request">OT 7: Informatics</a></li>
          </ul>
          <p>&nbsp;</p>
          <p>For all other requests, please use the standard <a href="/contact/index.php">contact form</a>. </p>
        </div>
          <div class="sidebox-bottom">
          </div>
          

        <!-- InstanceEndEditable -->
        <p>&nbsp;</p>
        </div>

        <!-- editable sidebar content region -->
    </div> <!-- sidebar -->
    
</div> <!-- middle -->

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a> - <a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a></span><br />
<a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a> - <a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a><br />
<a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a> - <a href="http://duke.edu/" title="Duke University" target="_new">Duke University</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>. 
</p>

<p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p> 

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script> 

<!-- google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18630139-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
<!-- InstanceEnd --></html>
