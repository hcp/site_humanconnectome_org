<?php
require_once('../Library/recaptchalib.php');
$privatekey = "6Lfxw8QSAAAAAHbhsBm0l4YSlIioR6Vpy0grqhNI";
$publickey = "6Lfxw8QSAAAAAKoFS0E8YVZ-DU-Y68P4G6G0YzKO"; // you got this from the signup page

//If the form is submitted
if(isset($_POST['submit'])) {

	//Software (not required)
	if(trim($_POST['software'])) {
		$software = trim($_POST['software']);
		$emailBody = "\n\nApplication: ".$software."\n\n";
	}
	
	//Functional Area (required)
	if(trim($_POST['function']) == '') {
		$hasError = 'empty';
	} else {
		$function = trim($_POST['function']);
		$emailBody .= "Function: ".$function."\n\n";
	}
	
	//Suggestion (required)
	if(trim($_POST['message']) == '') {
		$hasError = 'empty';
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['message']));
		} else {
			$comments = trim($_POST['message']);
		}
		$emailBody .= "Suggestion: ".$comments."\n\n";
	}
	
	//Name (required)
	if(trim($_POST['contactname']) == '') {
		$hasError = 'empty';
	} else {
		$name = trim($_POST['contactname']);
		$emailIntro = "From: $name "; 
	}

	//Email (required and needs validation)
	if(trim($_POST['email']) == '')  {
		$hasError = 'empty';
	} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
		$hasError = 'email';
	} else {
		$email = trim($_POST['email']);
		$emailIntro .= "($email)"; 
	}
	
	//Institution (not required)
	if(trim($_POST['institution'])) {
		$institution = trim($_POST['institution']);
		$emailIntro .= ", ".ucwords($institution);
	}
	
	//Scientific Interest (not required)
	if(trim($_POST['interest'])) {
		$interest = trim($_POST['interest']);
		$emailIntro .= "\n\nArea of Scientific Interest: $interest";
	}
	
	//Verify reCaptcha input
	
	$resp = recaptcha_check_answer ($privatekey,
								  $_SERVER["REMOTE_ADDR"],
								  $_POST["recaptcha_challenge_field"],
								  $_POST["recaptcha_response_field"]);
  
	if (!$resp->is_valid) {
	  // What happens when the CAPTCHA was entered incorrectly
	  $hasError = 'uncaptcha';
	} else {


		//If there is no error, send the email
		if(!isset($hasError)) {
			$emailTo = 'info@humanconnectome.org'; //Put your own email address here
			$body= $emailIntro.$emailBody;
			$headers = 'From: info@humanconnectome.org' . "\r\n" . 'Reply-To: ' . $email;
	
			mail($emailTo, 'Feature Request Submission', $body, $headers);
			
			mail($email, 'Thank you for your feature request submission', 'Thank you for your suggestion. We have received and logged the following feature request: "'.$comments.'" ', 'From: info@humanconnectome.org');
			$emailSent = true;
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/hcp-temp-main.dwt" codeOutsideHTMLIsLocked="false" --><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Connectome Software Feature Request Form | Human Connectome Project</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<meta http-equiv="description" content="Help us develop functionality for the ConnectomeDB and Connectome Workbench software tools." />
<meta http-equiv="keywords" content="ConnectomeDB, Connectome Workbench, Feature Request, Bug Tracker" />
<script type="text/javascript" src="http://dev.jquery.com/view/trunk/plugins/validate/jquery.validate.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#contactform").validate();
});
</script>

<!-- ReCAPTCHA initialization script -->
<script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>

<script type="text/javascript">
  Recaptcha.create("6Lfxw8QSAAAAAKoFS0E8YVZ-DU-Y68P4G6G0YzKO",
    "captcha",
    {
      theme: "red",
      callback: Recaptcha.focus_response_field
    }
  );
</script>

<!-- InstanceEndEditable -->
<link href='http://fonts.googleapis.com/css?family=Droid+Sans+Mono' rel='stylesheet' type='text/css'>
<link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/js/global.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.2.js"></script>
</head>

<body>
<!-- site container -->
<div id="site_container">

<div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div>

<!-- Header -->
<div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
  <map name="Header" id="Header">
    <area shape="rect" coords="14,7,404,123" href="/" alt="Human Connectome Project Logo" />
  </map>
</div> 
<!-- end Header -->

<!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" -->
<div id="topnav">
  <ul id="nav">
    <li><a href="/">Home</a></li>
    <li><a href="/about/">About the Project</a>
      <ul>
        <li><a href="/about/project/">Project Overview &amp; Components</a></li>
        <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
        <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
        <li><a href="/about/teams.html">Operational Teams</a></li>
        <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
        <li><a href="/about/publications.html">Publications</a></li>
        <li><a href="/courses">HCP Course Materials</a></li>
        <li><a href="/about/pressroom/">News &amp; Updates</a></li>
      </ul>
    </li>
    <li><a href="/data/">Data</a>
      <ul>
        <li><a href="/data/">Get Access to Public Data Releases</a></li>
        <li><a href="/data/connectome-in-a-box.html">Order Connectome in a Box</a></li>
        <li><a href="/data/data-use-terms/">HCP Data Use Terms</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Home" target="_blank">HCP Wiki: Data Resources</a></li>
      </ul>
    </li>
    <li><a href="/software/">Software</a>
      <ul>
        <li><a href="/software/connectome-workbench.html">What is Connectome Workbench</a></li>
        <li><a href="/software/get-connectome-workbench.html">Get Connectome Workbench</a></li>
        <li><a href="/software/workbench-command.php">Using Workbench Command</a></li>
        <li><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></li>
        <li><a href="/documentation/HCP-pipelines/index.html">HCP MR Pipelines</a></li>
        <li><a href="/documentation/HCP-pipelines/meg-pipeline.html">HCP MEG Pipelines</a></li>
      </ul>
    </li>
    <li><a href="/documentation/">Documentation</a>
      <ul>
      	<li><a href="/documentation/">All Documentation</a></li>
        <li><a href="/documentation/citations.html">How to Cite HCP</a></li>
        <li><a href="/documentation/tutorials/">Software Tutorials</a></li>
        <li><a href="/documentation/S900/">900 Subjects Data Release</a></li>
        <li><a href="/documentation/HCP-pipelines/">HCP Pipelines</a></li>
          <li><a href="/documentation/subject-key">How to Cite HCP Subjects with a Subject Key</a></li>
      </ul>
    </li>
    <li><a href="/contact/">Contact</a>
      <ul class="subnav">
      	<li><a href="/contact/">General Project Inquiries</a></li>
        <li><a href="/contact/#subscribe">Subscribe to Mailing Lists</a></li>
        <li><a href="/careers/">Career Opportunities</a></li>
        <li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
        <li> <a href="/contact/feature-request.php">Feature Request</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Media+Images+from+the+Human+Connectome+Project+WU-Minn+Consortium" target="_blank">HCP Wiki: Media Images</a></li>
      </ul>
    </li>
    <li><a href="/resources/">Other Resources</a>
      <ul class="subnav">
        <li><a href="http://wiki.humanconnectome.org/" target="_blank">HCP Wiki</a></li>
        <li><a href="http://lifespan.humanconnectome.org/" target="_blank">HCP Lifespan Pilot Project</a></li>
        <li><a href="/documentation/MGH-diffusion/">MGH Adult Diffusion Project</a></li>        
        <li><a href="/resources/faq-disease-related-connectome-research.html">FAQ: Disease-Related Connectome Research</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="search">
    <!--   If Javascript is not enabled, nothing will display here -->
    <script>
      (function() {
        var cx = '000073750279216385221:vnupk6whx-c';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      })();
    </script>
    <gcse:search></gcse:search>	
</div> <!-- Search box -->
<link rel="stylesheet" href="/css/google-cse-customizations.css" />
<!-- #EndLibraryItem --><!-- End Nav -->

<!-- Breadcrumbs -->
<div id="breadcrumbs">
  <p>&raquo; <!-- InstanceBeginEditable name="Breadcrumbs" -->contact us<!-- InstanceEndEditable --></p></div>
<!-- end breadcrumbs -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">
  <div id="content" class="editable"> 
  <!-- InstanceBeginEditable name="Main Content" -->
    <div id="cse" style="width:100%; display:none;"></div>
    <h1>Feature Request Form</h1>
    <p>As we develop our software tools for navigating the connectivity data of 1,200 human brains, we want to be sure that we are responsive to the needs of the scientific community at large. <strong>ConnectomeDB</strong> is our data archiving application that allows you to manage, sort and create your own collections of HCP subject data. <strong>Connectome Workbench</strong> is our platform for visualizing and exploring functional connectivity data on an anatomical map of the brain. </p>
    <p>Do you have a feature or piece of functionality that you would love to have, or need to have for your research? Let us know, using this web form. As we start building a critical mass of suggestions, we will publish that list and use it to guide our development process. </p>
    
    <!-- Acknowledgement and thanks: PHP/JQuery contact page uses code writen by Raymond Selda: http://www.raymondselda.com/php-contact-form-with-jquery-validation/ --> 
    
    <div class="hcpForm-wrapper">
    <!-- PHP messages to send --> 
	<?php if(isset($hasError)) { //If errors are found 
			if ($hasError=='email') { // If the error is not related to the email address	?>
    	<p class="error">Please enter a valid email address. Thank you. </p>
    <?php  	} elseif ($hasError=='uncaptcha') { ?>
    	<p class="error">Please make sure your recaptcha entry is correct. Thank you.</p>
    <?php 	} else { ?>
    	<p class="error">Please make sure you have filled each field with valid information. Thank you.</p>
	<?php 	}
		  }?>
    
    <?php if(isset($emailSent) && $emailSent == true) { //If email is sent ?>
        <p><strong>Email Successfully Sent!</strong></p>
        <p>Thank you <strong><?php echo $name;?></strong> for contacting us! Your email was successfully sent and we will be in touch with you soon.</p>
    <?php } ?>

	<!-- the form. required fields are listed as class="required" -->
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="contactform" class="hcpForm">
            <div class="hcpForm-block">
                <label for="software"><strong>Software:</strong></label>
                <p><span style="margin-right:3em"><input type="checkbox" name="software" value="ConnectomeDB" /> ConnectomeDB</span> <input type="checkbox" name="software" value="Connectome Workbench" /> Connectome Workbench</p>
            </div>
            
            <div class="hcpForm-block">
                <label for="function"><strong>Functional Area / category</strong> *</label>
                <select name="function" id="function" class="required">
                  <option value="parcellation">Parcellation &amp; Network Analysis</option>
                  <option value="rfMRI">Resting State fMRI</option>
                  <option value="tfMRI">Task fMRI</option>
                  <option value="diffusion">Diffusion MRI</option>
                  <option value="MEG">MEG/EEG</option>
                  <option value="stats">Statistical Tools</option>
                  <option value="UX">User Experience</option>
                  <option value="data">Data import/export</option>
                  <option value="visualization">Visualization</option>
                </select>
            </div>
            <div class="hcpForm-block">
                <label for="message"><strong>Suggestion/recommendation:</strong> *</label>
                <textarea rows="5" cols="60" name="message" id="message" class="required"></textarea>
            </div>
            <hr size="1" color="#808080" />
            <div class="hcpForm-block">
                <label for="name"><strong>Your Name:</strong> *</label>
                <input type="text" size="60" name="contactname" id="contactname" value="" class="required" />
            </div>
        
            <div class="hcpForm-block">
                <label for="email"><strong>Email:</strong> *</label>
                <input type="text" size="60" name="email" id="email" value="" class="required email" />
            </div>
        
            <div class="hcpForm-block">
                <label for="institution"><strong>Institution:</strong> </label>
                <input type="text" size="60" name="institution" id="institution" value="" />
            </div>
            
            <div class="hcpForm-block">
                <label for="interest"><strong>Area of Scientific Interest:</strong></label>
                <input type="text" size="60" name="interest" id="interest" value="" />
            </div>
            <hr size="1" color="#808080" />
            <div class="hcpForm-block">
                <label for="recaptcha"><strong>Recaptcha Spam Protection</strong></label>
				        <?php
                  echo recaptcha_get_html($publickey);
                ?>
            </div>
            <input type="submit" value="Send Message" name="submit" />
        </form>
        
    </div> <!-- end contact-wrapper -->

<!-- InstanceEndEditable -->  
</div>

  <!-- Primary page Content -->

<div id="sidebar-rt">
    
        <div id="sidebar-content" class="editable">
        <!-- InstanceBeginEditable name="Sidebar Content" --><!-- InstanceEndEditable -->
        <p>&nbsp;</p>
        </div>

        <!-- editable sidebar content region -->
    </div> <!-- sidebar -->
    
</div> <!-- middle -->

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a> - <a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a></span><br />
<a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a> - <a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a><br />
<a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a> - <a href="http://duke.edu/" title="Duke University" target="_new">Duke University</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>. 
</p>

<p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p> 

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script> 

<!-- google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18630139-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
<!-- InstanceEnd --></html>
