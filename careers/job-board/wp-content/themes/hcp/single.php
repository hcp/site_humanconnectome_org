<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>


<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<p class="date"><?php echo get_the_term_list( $post->ID, 'institution', '<strong>HCP Institution: </strong>', ', ', '' ); ?></p>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h3 class="storytitle"><?php the_title(); ?></h3>
					
					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                        
                        <!-- include MagicFields content -->
			
                        <h3 class="jobdetails">Job Details</h3>
                        <div class="jobdetails">
                        <p><strong>Location: </strong><?php echo get('Location'); ?></p>
                        <?php if (get('Department')) {  echo '<p><strong>Department Summary: </strong>' . get('Department') .'</p>';  } ?>
                        <?php if (get('Qualifications')) {  echo '<p><strong>Qualifications: </strong></p>' . get('Qualifications');  } ?>
                        <?php if (get('Salary')) {  echo '<p><strong>Salary: </strong>' . get('Salary') .'</p>';  } ?>
                        <?php if (get('Projected_Hire_Date')) {  echo '<p><strong>Projected Hire Date: </strong>' . get('Projected_Hire_Date') .'</p>';  } ?>
                        <?php if (get('Contact')) {  echo '<p><strong>Contact: </strong><a href=\'mailto:' . get('Contact') .'\'>' .get('Contact') . '</a></p>';  } ?>
                        </div>
					</div><!-- .entry-content -->

					<div class="meta"> 
						<?php the_tags(__('Tags: '), ', ', '<br clear="all" />'); ?> 
                        Posted by <?php the_author() ?> @ <?php the_time() ?> <?php edit_post_link(__(' -- Edit This')); ?>
                    </div>

				</div><!-- #post-## -->

				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'twentyten' ) . '</span> %title' ); ?></div>
					<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'twentyten' ) . '</span>' ); ?></div>
				</div><!-- #nav-below -->


<?php endwhile; // end of the loop. ?>



<?php get_footer(); ?>
