<?php
/**
 * @package WordPress
 * @subpackage Classic_Theme
 */

add_theme_support( 'automatic-feed-links' );

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '',
		'after_title' => '',
	));


//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_institution_taxonomies', 0 );

//create two taxonomies, genres and writers for the post type "book"
function create_institution_taxonomies() 
{
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Institutions', 'taxonomy general name' ),
    'singular_name' => _x( 'Institution', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Institutions' ),
    'all_items' => __( 'All Institutions' ),
    'edit_item' => __( 'Edit Institution' ), 
    'update_item' => __( 'Update Institution' ),
    'add_new_item' => __( 'Add New Institution' ),
    'new_item_name' => __( 'New Institution Name' ),
  ); 	

  register_taxonomy('institution',array('post'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
	'public' => true,
    'rewrite' => array( 'slug' => 'institution' ),
  ));

}

/* CUSTOMIZE EXCEPRT HANDLING */ 
function new_excerpt_more($more) {
	return '... <p><a href="'. get_permalink($post->ID) . '">' . '-> Full listing' . '</a></p>';
}
add_filter('excerpt_more', 'new_excerpt_more');


?>


