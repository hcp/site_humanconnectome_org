<?php

/**

 * @package WordPress

 * @subpackage Classic_Theme

 */

get_header();

?>



<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



<p class="date"><?php echo get_the_term_list( $post->ID, 'institution', '<strong>HCP Institution: </strong>', ', ', '' ); ?></p>



<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

	 <h3 class="storytitle"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>

	



	<div class="storycontent">

		<?php if ( is_category() || is_archive() ) {
			the_excerpt();
		} else {
			the_content();
			?>
           	<!-- include MagicFields content -->
			
   			<h3 class="jobdetails">Job Details</h3>
            <div class="jobdetails">
            <p><strong>Location: </strong><?php echo get('Location'); ?></p>
            <?php if (get('Department')) {  echo '<p><strong>Department Summary: </strong>' . get('Department') .'</p>';  } ?>
            <?php if (get('Qualifications')) {  echo '<p><strong>Qualifications: </strong></p>' . get('Qualifications');  } ?>
            <?php if (get('Salary')) {  echo '<p><strong>Salary: </strong>' . get('Salary') .'</p>';  } ?>
            <?php if (get('Projected_Hire_Date')) {  echo '<p><strong>Projected Hire Date: </strong>' . get('Projected_Hire_Date') .'</p>';  } ?>
            <?php if (get('Contact')) {  echo '<p><strong>Contact: </strong><a href=\'mailto:' . get('Contact') .'\'>' .get('Contact') . '</a></p>';  } ?>
            </div>
			<?php
		} ?>

    
	</div>



	<div class="meta"> <?php the_tags(__('Tags: '), ', ', '<br clear="all" />'); ?> Posted by <?php the_author() ?> @ <?php the_time() ?> <?php edit_post_link(__(' -- Edit This')); ?></div>



</div>





<?php endwhile; else: ?>

<p><?php _e('Many of the institutions associated with the HCP are or will be staffing up to prepare to fulfill the duties of this project. Please check back to this space often for new opportunities.'); ?></p>

<!-- <p>Many of the institutions assocuated with the HCP are or will be staffing up to prepare to fulfill the duties of this project. Please check back to this space often for new opportunities.</p>
<h3>Jobs at Washington University in Saint Louis</h3>

<p><a href="http://jobs.wustl.edu/">http://jobs.wustl.edu/</a></p>

<h3>Jobs at University of Minnesota</h3>

<p><a href="http://www1.umn.edu/ohr/employment/">http://www1.umn.edu/ohr/employment/</a></p> -->

<?php endif; ?>



<?php posts_nav_link(' &#8212; ', __('&laquo; Newer Listings'), __('Older Listings &raquo;')); ?>



<?php get_footer(); ?>

