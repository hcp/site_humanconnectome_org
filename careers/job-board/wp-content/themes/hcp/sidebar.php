<?php

/**

 * @package WordPress

 * @subpackage Classic_Theme

 */

?>

<!-- begin sidebar -->

<div id="menu">

<div id="sidebar-content">

  <div class="sidebox">

    <h2>Job Listings</h2>

	<ul>



<?php 	/* Widgetized sidebar, if you have the plugin installed. */

		if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>

            <?php wp_list_categories('title_li=' . __('Open Positions at HCP Institutions:').'&taxonomy=institution&show_count=1&hide_empty=0'); ?>

         

             </ul>

        <?php endif; ?>
        
    <ul>
        <li class="institution">
        	University Job Boards
            <ul>
            	<li><a href="http://jobs.wustl.edu/" target="_jobs">Jobs at WashU</a></li>
                <li><a href="http://www1.umn.edu/ohr/employment/" target="_jobs">Jobs at UMinn</a></li>
                <li><a href="http://www.ox.ac.uk/about_the_university/jobs/index.html" target="_jobs">Jobs at Oxford</a></li>
                <li><a href="http://www.advancedmri.com/jobs.html" target="_jobs">Jobs at AMRIT</a></li>
                <li><a href="http://www.slu.edu/jobs/" target="_jobs">Jobs at SLU</a></li>
                <li><a href="https://jobs.iu.edu/" target="_jobs">Jobs at IU</a></li>
                <li><a href="http://www.mpg.de/english/careerOpportunities/index.html" target="_jobs">Jobs at ESI</a></li>
                <li><a href="http://www2.warwick.ac.uk/services/humanresources/jobsintro" target="_jobs">Jobs at Warwick</a></li>
                <li><a href="http://jobs.berkeley.edu/" target="_jobs">Jobs at UC Berkeley</a></li>
             </ul>
        </li>



    </ul>

  </div> <!-- end sidebox -->

  

  <div class="sidebox">

  	<h2>Admin Center</h2>

    <ul>

     	<li id="dashboard-1">

           <ul>

              <li><a href="/careers/job-board/wp-admin/">Editor's Dashboard</a></li>

            </ul>

         </li>

    </ul>

    <a href="http://wordpress.org/"><img src="/img/powered-by.png" alt="Powered by WordPress" border="0" /></a>

  </div>

</div> <!-- end sidebar-content -->



</div>

<!-- end sidebar -->

