<?php

/**

 * @package WordPress

 * @subpackage Classic_Theme

 */

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>



<head profile="http://gmpg.org/xfn/11">

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />



	<title>News &amp; Updates | Human Connectome Project</title>



	<link href="/about/blog/themes/hcp/style.css" rel="stylesheet" type="text/css" />



	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php wp_get_archives('type=monthly&format=link'); ?>

	<?php //comments_popup_script(); // off by default ?>

	<?php wp_head(); ?>

</head>



<body <?php body_class(); ?>>

<div id="rap">



	<div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />

      <map name="Header" id="Header">

        <area shape="rect" coords="14,7,404,123" href="/" alt="Human Connectome Project Logo" />

      </map>

    </div> 

    <!-- end Header -->

    

    <!-- top level navigation -->

    <div id="topnav">

    <ul id="nav">

        <li><a href="/">Home</a></li>

        <li><a href="/about/">About the Project</a>

            <ul>

              <li><a href="/about/project/">Project Overview</a></li>

                <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>

                <li><a href="/about/news-press.html">News &amp; Press</a></li>

                <li><a href="/about/blog/">Connectome Blog</a></li>

            </ul>

         </li>

         <li><a href="/documentation/">Documentation</a>

             <ul>

               <li><a href="/documentation/publications.html">Publications</a></li>

                <li><a href="/documentation/data.html">Data</a></li>

                <li><a href="/documentation/methodology/">Methodology</a></li>

                <li><a href="/documentation/resources.html">Resources</a></li>

                <li><a href="/documentation/tutorials.html">Tutorials</a></li>

            </ul>

         </li>

         <li><a href="/connectome/">Using the Connectome</a>

             <ul class="subnav">

               <li><a href="/connectome/connectomeDB.html">ConnectomeDB</a></li>

                <li><a href="/connectome/connectome-workbench.html">Connectome Workbench</a></li>

                <li><a href="/connectome/network-analysis.html">Network Analysis (BCT)</a></li>

                <li><a href="/connectome/MEG-fieldtrip.html">MEG / Fieldtrip</a></li>

            </ul>

         </li>

         <li><a href="/contact/">Contact Us</a></li>

         <li><a href="/collaborate/">Collaboration Extranet</a>

            <ul class="subnav">

               <li><a href="/collaborate/">Login</a></li>

            </ul>

        </li>

      </ul>

    </div> 

    <!-- End Nav -->



<div id="content">

<!-- end header -->



<?php

/**

 * @package WordPress

 * @subpackage Classic_Theme

 */

get_header();

?>



<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



<?php the_date('','<h2>','</h2>'); ?>



<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

	 <h3 class="storytitle"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>

	<div class="meta"><?php _e("Filed under:"); ?> <?php the_category(',') ?> &#8212; <?php the_tags(__('Tags: '), ', ', ' &#8212; '); ?> <?php the_author() ?> @ <?php the_time() ?> <?php edit_post_link(__('Edit This')); ?></div>



	<div class="storycontent">

		<?php the_content(__('(more...)')); ?>

	</div>



	<div class="feedback">

		<?php wp_link_pages(); ?>

		<?php comments_popup_link(__('Comments (0)'), __('Comments (1)'), __('Comments (%)')); ?>

	</div>



</div>



<?php comments_template(); // Get wp-comments.php template ?>



<?php endwhile; else: ?>

<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>

<?php endif; ?>



<?php posts_nav_link(' &#8212; ', __('&laquo; Newer Posts'), __('Older Posts &raquo;')); ?>



<?php

/**

 * @package WordPress

 * @subpackage Classic_Theme

 */

?>

<!-- begin footer -->

</div>



<?php

/**

 * @package WordPress

 * @subpackage Classic_Theme

 */

?>

<!-- begin sidebar -->

<div id="menu">



<ul>

<?php 	/* Widgetized sidebar, if you have the plugin installed. */

		if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>

	<?php wp_list_pages('title_li=' . __('Pages:')); ?>

	<?php wp_list_bookmarks('title_after=&title_before='); ?>

	<?php wp_list_categories('title_li=' . __('Categories:')); ?>

 <li id="search">

   <label for="s"><?php _e('Search:'); ?></label>

   <form id="searchform" method="get" action="<?php bloginfo('home'); ?>">

	<div>

		<input type="text" name="s" id="s" size="15" /><br />

		<input type="submit" value="<?php esc_attr_e('Search'); ?>" />

	</div>

	</form>

 </li>

 <li id="archives"><?php _e('Archives:'); ?>

	<ul>

	 <?php wp_get_archives('type=monthly'); ?>

	</ul>

 </li>

 <li id="meta"><?php _e('Meta:'); ?>

	<ul>

		<?php wp_register(); ?>

		<li><?php wp_loginout(); ?></li>

		<li><a href="<?php bloginfo('rss2_url'); ?>" title="<?php _e('Syndicate this site using RSS'); ?>"><?php _e('<abbr title="Really Simple Syndication">RSS</abbr>'); ?></a></li>

		<li><a href="<?php bloginfo('comments_rss2_url'); ?>" title="<?php _e('The latest comments to all posts in RSS'); ?>"><?php _e('Comments <abbr title="Really Simple Syndication">RSS</abbr>'); ?></a></li>

		<li><a href="http://validator.w3.org/check/referer" title="<?php _e('This page validates as XHTML 1.0 Transitional'); ?>"><?php _e('Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr>'); ?></a></li>

		<li><a href="http://gmpg.org/xfn/"><abbr title="XHTML Friends Network">XFN</abbr></a></li>

		<li><a href="http://wordpress.org/" title="<?php _e('Powered by WordPress, state-of-the-art semantic personal publishing platform.'); ?>"><abbr title="WordPress">WP</abbr></a></li>

		<?php wp_meta(); ?>

	</ul>

 </li>

<?php endif; ?>



</ul>



</div>

<!-- end sidebar -->





	<div id="footer">

    <p><a href="/privacy/">Privacy</a> | <a href="/terms/">Terms and Conditions</a> | <a href="/sitemap/">Site Map</a></p>

    <p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/">National Institutes of Health</a>, and all information in this site is available to the public domain. <br />

    No Protected Health Information has been published on this site. Last updated April 15, 2010. </p>

  </div>



</div>



<?php wp_footer(); ?>

</body>

</html>



