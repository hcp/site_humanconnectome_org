// JavaScript Document


function showRegDiv() {
	showModal('reg-div');
	validateRegForm();
}
function closeConfirmation() {
	if ($('#reg-confirm-msg').text().match(/Thank/)) {
		modalClose();
	} else {
		modalClose('reg-confirm-div');
		modalClose('reg-processing-div');
	}
}

/* 
 * --- 
 * Data Use Terms controls 
 * ---
 */
 
function agreeChange(checkbox,id) {
	// if user checks box, don't let them uncheck. Turn on the form submit and guide them to it. 
	$(checkbox).prop('disabled','disabled');
	$('#'+id).prop('disabled',false).focus();
	var version = $('#dut-version').html();
	$('.modal').append('<input type="hidden" name="dut" id="accept-dut" value="'+version+'" />');
}



function scrollFunc(ele,check,button) {
	if (ele.clientHeight + ele.scrollTop >= ele.scrollHeight) {
		$("#" + check).prop('disabled',false);
		$('.dut-reminder').hide();
		$('.dut-accept-message').show();
	}
}


function forgotLoginRequest() {
	params='account=' + $('#forgotPassword').val() + '&email=' + $('#forgotUsername').val() + '&XNAT_CSRF=' + window.csrfToken;
	//$('#forgot-pass-div').addClass("hidden").hide();
	$('#reg-processing-div').removeClass("hidden").show();
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/forgotlogin?'+params,{ success:forgotGetSuccess,failure:forgotGetFailure });
}

$(document).ready(function(){
	$('.term-check').on('click', function(){
		// check this element and disable it
		$(this).prop('checked',true).prop('disabled','disabled');
		// check to see if each element has been checked ("accepted"), and if so, allow user to accept all terms and continue with order.
		var acceptance; 
		$('.term-check').each(function(){
			acceptance = true; 
			if ( $(this).prop('checked') === false ) { acceptance=false; return false; }
		});
		if (acceptance) { 
			// display an acceptance message in the footer of the DUT modal
			$('.dut-accept-message').find('input').prop('checked',true);
			$('.dut-accept-message').find('label').html('You have accepted each of the terms of use.');
			// enable the form submit button
			$('.form-submit').show();
		}
	});
});

