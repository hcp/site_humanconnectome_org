<?php

/**

 * @package WordPress

 * @subpackage Classic_Theme

 */

get_header();

?>



<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



<p class="date"><?php the_category(',') ?> | <?php the_date('','',''); ?></p>



<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

	 <h3 class="storytitle"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>

	



	<div class="storycontent">

		<?php the_content(__('[Read More]')); ?>

	</div>



	<div class="meta"> <?php the_tags(__('Tags: '), ', ', '<br clear="all" />'); ?> Posted by <?php the_author() ?> @ <?php the_time() ?> <?php edit_post_link(__(' -- Edit This')); ?></div>



</div>





<?php endwhile; else: ?>

<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>

<?php endif; ?>



<?php posts_nav_link(' &#8212; ', __('&laquo; Newer Posts'), __('Older Posts &raquo;')); ?>



<?php get_footer(); ?>

