<?php

/**

 * @package WordPress

 * @subpackage Classic_Theme

 */

?>

<!-- begin sidebar -->

<div id="menu">

<div id="sidebar-content">

  <div class="sidebox">

    <h2>News Archive</h2>

	<ul>



<?php 	/* Widgetized sidebar, if you have the plugin installed. */

		if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>

			<?php wp_list_bookmarks('title_after=&title_before='); ?>

            <?php wp_list_categories('title_li=' . __('Categories:')); ?>

         

                <li id="archives"><?php _e('Archives:'); ?>

                <ul>

                 <?php wp_get_archives(array('type' => 'monthly')); ?>

                </ul>

             	</li>

             </ul>

        <?php endif; ?>



    </ul>

  </div> <!-- end sidebox -->

  

  <div class="sidebox">

  	<h2>Admin Center</h2>

    <ul>

     	<li id="dashboard-1">

           <ul>

              <li><a href="/about/pressroom/wp-admin/">Editor's Dashboard</a></li>

            </ul>

         </li>

    </ul>

    <a href="http://wordpress.org/"><img src="/img/powered-by.png" alt="Powered by WordPress" border="0" /></a>

  </div>

</div> <!-- end sidebar-content -->



</div>

<!-- end sidebar -->

