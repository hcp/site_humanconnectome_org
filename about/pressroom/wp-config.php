<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

if ($_SERVER['HTTP_HOST']=='dev.humanconnectome.org') : 
	define ('DB_NAME', 'devadmin_hcp_dev');  	// testing server setting. 
else : 
	define('DB_NAME', 'devadmin_hcp');  		// live server setting
endif; 

/** MySQL database username */
define('DB_USER', 'devadmin_wp1');  				

/** MySQL database password */
define('DB_PASSWORD', 'Hsg0^LLpPZA#');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Yo79iEK.D%^OX-@(w}vg=qUy8K`$.,t&8>[>q6;/SQ|hpO4G)g|gpbj?uFMFs(Ot');
define('SECURE_AUTH_KEY',  'Lz+_^D8-JMTn+&r8MQAC H*nU%-J-0+-OkD>u/;98l,#3,-f5}(.T,]Q+_[tO7_N');
define('LOGGED_IN_KEY',    'H;y,--8^bOER[31I>r_}44fJ|Y{{isMC2$mg-df)7%MU!6AqgxX`#Tv5x/>1|e*Z');
define('NONCE_KEY',        '8M-vq+ <<VWD|-_FI{^{6(F49fFnno}!%FG2Img Z+j8*$Oq);#qu~b}c,H{okay');
define('AUTH_SALT',        'w-K|%(z4nR|=osH@l*+bM?1)MsE[ws,w:|]+!m:(Qqv`c*uYL$L/0%#l72kL|LA}');
define('SECURE_AUTH_SALT', '/57,`e&-WEW@tDA!_6E1q1AwG$|K|%&g]<] }Pw/zU-FuM>4bIC{+dPlP==e.uVd');
define('LOGGED_IN_SALT',   'P_,u-ngF9$k$]6Sveu*CLo](aN|)`:Ffb;ZaqFasxKFMRnp~`n@lx^jcr13aA/(T');
define('NONCE_SALT',       '?JpS-`7$k9rLq5o-!6|=Kl`R9{wBm`~pY>4z+5O-#Vp{s&.;OIugd{!|`M&2k|C_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'hcp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
