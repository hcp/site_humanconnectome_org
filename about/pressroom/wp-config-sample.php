<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
if ($_SERVER['HTTP_HOST']=='dev.humanconnectome.org') : 
	define ('DB_NAME', 'devadmin_hcp_dev');  	// testing server setting. 
else : 
	define('DB_NAME', 'devadmin_hcp');  		// live server setting
endif; 

/** MySQL database username */
define('DB_USER', 'devadmin_wp1');  				

/** MySQL database password */
define('DB_PASSWORD', 'Hsg0^LLpPZA#');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1X5)V{_1Sh;j 5K?2#55#Y^2%G&i=O<mN2+YDEWG*S2Gi([bb9<9uln0N3H)f[6&');
define('SECURE_AUTH_KEY',  '0zZ-8vG2%W?kCtdn=)d:*>*j)>0SY`ocbAKCV,@mMtxh-BH*!ppb`A|)Xlh%}1&o');
define('LOGGED_IN_KEY',    'mjq@D3Hm1Wlu%)fu?;w,i5wx1kx[K1tfSOl~zD~Q%s=7JK]|v@y[Mu_%L0MV>1w]');
define('NONCE_KEY',        '<+[Z|Rjl@~[OC|2kCG]^T=,_g[Paptf}C~ >U>~wXmOJ8[pPYVVx:qJ$in`@.HO4');
define('AUTH_SALT',        '-Cd7/c4{Za x#1k#b=d<|t2#R$+8DVm|^v;ZHQ6%u6:0h<xW|PS6G[[c!Ng7~0Xj');
define('SECURE_AUTH_SALT', 'a)+Q~%=xX]I$N$l&e68xS+$5t}Tn7c:!I-)k(nHc)%TpGv+F5R[[++Ffl-D55G1L');
define('LOGGED_IN_SALT',   '3|aW!5<g(s*Al3g:*kk7bP>6+}g++2b9ra8K]X_O`-,*tD[wozAWTJ0J5U.m[OW8');
define('NONCE_SALT',       '3A0U%];We_2j]+,`bQRH_jH-3FZCwRxiG:;3#@Yn||yW1Au=Fz`BV7[/{sh*CDm8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'hcp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
