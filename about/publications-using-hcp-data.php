<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/hcp-temp-main.dwt" codeOutsideHTMLIsLocked="false" --><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>Publications | Human Connectome Project</title>
    <!-- InstanceEndEditable -->
    <!-- InstanceBeginEditable name="head" -->
    <meta http-equiv="Description" content="Articles written by our investigators, including HCP-support papers and other relevant connectomics writing." />
    <meta http-equiv="Keywords" content="Human Connectome Project, Publications, Brain Networks, Parcellation, Functional Connectivity" />
    <!-- InstanceEndEditable -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans+Mono' rel='stylesheet' type='text/css'>
    <link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/js/global.js"></script>
    <script type="text/javascript" src="/js/jquery.easing.1.2.js"></script>
</head>

<body>
<!-- site container -->
<div id="site_container">

<div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div>

<!-- Header -->
<div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
    <map name="Header" id="Header">
        <area shape="rect" coords="14,7,404,123" href="/" alt="Human Connectome Project Logo" />
    </map>
</div>
<!-- end Header -->

<!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" -->
<div id="topnav">
  <ul id="nav">
    <li><a href="/">Home</a></li>
    <li><a href="/about/">About the Project</a>
      <ul>
        <li><a href="/about/project/">Project Overview &amp; Components</a></li>
        <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
        <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
        <li><a href="/about/teams.html">Operational Teams</a></li>
        <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
        <li><a href="/about/publications.html">Publications</a></li>
        <li><a href="/courses">HCP Course Materials</a></li>
        <li><a href="/about/pressroom/">News &amp; Updates</a></li>
      </ul>
    </li>
    <li><a href="/data/">Data</a>
      <ul>
        <li><a href="/data/">Get Access to Public Data Releases</a></li>
        <li><a href="/data/connectome-in-a-box.html">Order Connectome in a Box</a></li>
        <li><a href="/data/data-use-terms/">HCP Data Use Terms</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Home" target="_blank">HCP Wiki: Data Resources</a></li>
      </ul>
    </li>
    <li><a href="/software/">Software</a>
      <ul>
        <li><a href="/software/connectome-workbench.html">What is Connectome Workbench</a></li>
        <li><a href="/software/get-connectome-workbench.html">Get Connectome Workbench</a></li>
        <li><a href="/software/workbench-command.php">Using Workbench Command</a></li>
        <li><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></li>
        <li><a href="/documentation/HCP-pipelines/index.html">HCP MR Pipelines</a></li>
        <li><a href="/documentation/HCP-pipelines/meg-pipeline.html">HCP MEG Pipelines</a></li>
      </ul>
    </li>
    <li><a href="/documentation/">Documentation</a>
      <ul>
      	<li><a href="/documentation/">All Documentation</a></li>
        <li><a href="/documentation/citations.html">How to Cite HCP</a></li>
        <li><a href="/documentation/tutorials/">Software Tutorials</a></li>
        <li><a href="/documentation/S900/">900 Subjects Data Release</a></li>
        <li><a href="/documentation/HCP-pipelines/">HCP Pipelines</a></li>
          <li><a href="/documentation/subject-key">How to Cite HCP Subjects with a Subject Key</a></li>
      </ul>
    </li>
    <li><a href="/contact/">Contact</a>
      <ul class="subnav">
      	<li><a href="/contact/">General Project Inquiries</a></li>
        <li><a href="/contact/#subscribe">Subscribe to Mailing Lists</a></li>
        <li><a href="/careers/">Career Opportunities</a></li>
        <li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
        <li> <a href="/contact/feature-request.php">Feature Request</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Media+Images+from+the+Human+Connectome+Project+WU-Minn+Consortium" target="_blank">HCP Wiki: Media Images</a></li>
      </ul>
    </li>
    <li><a href="/resources/">Other Resources</a>
      <ul class="subnav">
        <li><a href="http://wiki.humanconnectome.org/" target="_blank">HCP Wiki</a></li>
        <li><a href="http://lifespan.humanconnectome.org/" target="_blank">HCP Lifespan Pilot Project</a></li>
        <li><a href="/documentation/MGH-diffusion/">MGH Adult Diffusion Project</a></li>        
        <li><a href="/resources/faq-disease-related-connectome-research.html">FAQ: Disease-Related Connectome Research</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="search">
    <!--   If Javascript is not enabled, nothing will display here -->
    <script>
      (function() {
        var cx = '000073750279216385221:vnupk6whx-c';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      })();
    </script>
    <gcse:search></gcse:search>	
</div> <!-- Search box -->
<link rel="stylesheet" href="/css/google-cse-customizations.css" />
<!-- #EndLibraryItem --><!-- End Nav -->

<!-- Breadcrumbs -->
<div id="breadcrumbs">
    <p>&raquo; <!-- InstanceBeginEditable name="Breadcrumbs" --><a href="./">about</a> > publications using HCP data<!-- InstanceEndEditable --></p></div>
<!-- end breadcrumbs -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">
<div id="content" class="editable">
    <!-- InstanceBeginEditable name="Main Content" -->
    <!--    <div id="cse" style="width:100%; display:none;"></div>  -->
    <h1>Publications Using HCP Data</h1>
    <p>This page documents publications appearing on PubMed that cite the use of  publicly released WU-Minn HCP data or methods for part of their study analysis. This list includes  both within HCP consortium and outside HCP consortium studies published after  the March 2013 Q1 HCP Data Release. This list does not include publications  using MGH-USC HCP data or  methods, if WU-Minn HCP data or methods were not also included. (See: <a href="http://www.neuroscienceblueprint.nih.gov/connectome/" target="_blank">NIH  HCP Consortia</a>)</p>
    <p>To compile this list, we search PubMed for &ldquo;<strong>Human Connectome Project</strong>&rdquo;  OR WU-Minn HCP Grant number: &ldquo;<strong>MH091657</strong>&rdquo;, and manually take out  publications that do not specifically use the WU-Minn data or methods. We also  manually add any publications that are directly sent to us and meet the  &quot;use WU-Minn HCP data or  methods&quot; criteria.</p>
    <p>For  publications produced by the HCP consortium and collaborators, since the  inception of HCP in 2010, please see <a href="/about/consortium-publications.php">HCP  Consortium Publications</a>.
 </p>
    <div id="listFilter">
        <p>
            <strong>Find publications by author:</strong>
            <input type="text" name="authorNameSearch" />
            <button id="searchToggle">Find Pubs</button>
            <button id="resetList">Reset</button>
        </p>
    </div>
    <p><span style="float:right; font-size: 11px"><a href="javascript:" onclick="$('.summary').toggle();">Toggle Summaries</a></span></p>

    <script type="text/javascript" src="/js/pubSearch.js"></script>
    <div id="Investigators" class="noThumbs">
        <?php
        /* db login */
        if ($_SERVER['HTTP_HOST']=='dev.humanconnectome.org') :
            $dbname = 'db101945_hcp_dev';
        else :
            $dbname = 'db101945_hcpp';
        endif;
        $dbuser = 'db101945';
        $dbpass = 'N0seF@c3';
        $dbhost = $_ENV{DATABASE_SERVER};
        $db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

        if (!$db) {
            die('Connect Error (' . mysqli_connect_errno() . ') '
                . mysqli_connect_error());
        }

        // query pubs
        $q = "SELECT * FROM pubs WHERE Type='external' OR Type='both' ORDER BY PMID DESC;";
        $r = mysqli_query($db,$q);

        while ($pub = mysqli_fetch_array($r)) :
            ?>
            <dl class="Inv" id="<?php echo $pub['UID'] ?>">
                <dt><strong><?php echo $pub['Title'] ?></strong></dt>
                <dd class="dimage">
                    <?php if ($pub['ThumbnailPath']) : ?>
                        <img src='<?php echo $pub['ThumbnailPath'] ?>' width='80' height='80' />
                    <?php endif; ?>
                </dd>
                <dd class="authors team">
                    <p style="margin-bottom: 3px;"><?php echo $pub['Authors'] ?></p>
                    <?php if ($pub['doi']) : ?>
                        <a href="http://dx.doi.org/<?php echo $pub['doi'] ?>"><?php echo $pub['PubTitle'] ?></a>,
                    <?php else : ?>
                        <?php echo $pub['PubTitle'] ?>
                    <?php endif; ?>
                    <?php echo $pub['PubDate'] ?>
                    <?php if ($pub['PMID']) : ?>
                        | PMID: <a href='http://www.ncbi.nlm.nih.gov/pubmed/<?php echo $pub['PMID'] ?>' target='_blank'><?php echo $pub['PMID'] ?></a>
                    <?php endif; ?>
                </dd>
                <?php if ($pub['PubSummary']) : ?>
                    <dd class="summary" style="display: none">
                        <p><?php echo $pub['PubSummary'] ?></p>
                    </dd>
                <?php endif; ?>
            </dl>
        <?php
        endwhile;

        /* db connection close */
        mysqli_close($db);
        ?>

    </div>
    <p><img src="/img/article-end.png" /></p>
    <!-- InstanceEndEditable -->
</div>

<!-- Primary page Content -->

<div id="sidebar-rt">

    <div id="sidebar-content" class="editable">
        <!-- InstanceBeginEditable name="Sidebar Content" -->
        <div class="sidebox">
            <h2>HCP Citations</h2>
            <p><strong><a href="/documentation/citations.html">How to cite HCP publications if you are publishing work based on HCP data</a></strong>.</p>
        </div>
        <div class="sidebox-bottom"></div>

        <div class="sidebox readingList">
            <h2>Related Publications</h2>
            <p><strong>On This Site:</strong></p>
            <ul><li><a href="/about/consortium-publications.php">List of HCP Consortium Publications</a></li></ul>
            <hr size="1" />
            <p><strong>Other Publications</strong></p>
            <ul>
                <li><a href="http://www.sciencedirect.com/science/article/pii/S0896627311010038" target="_blank">Open Neuroscience Solutions for the Connectome-wide Association Era</a> - Michael Peter Milham, Neuron. January 26, 2012.</li>
                <li><a href="http://www.jneurosci.org/content/31/44/15775.long" target="_blank">Rich-club organization of the Human Connectome</a> - Martijn P. van den Heuvel and Olaf Sporns, Journal of Neuroscience. November, 2011.</li>
                <li><a href="http://www.sciencedirect.com/science/article/pii/S1053811911012687" target="_blank">Hierarchical topological network analysis of anatomical human brain connectivity and differences related to sex and kinship</a> - Julio M. Duarte-Carvajalino et al, Neuroimage. November, 2011.</li>
                <li><a href="http://www.pnas.org/content/early/2011/09/14/1112685108.abstract" target="_blank">Investigating the electrophysiological basis of resting state networks using magnetoencephalography</a> - Matthew J. Brookes et al, PNAS. September, 2011.</li>
                <li><a href="http://www.sciencemag.org/content/329/5997/1358.full" target="_blank">Prediction of Individual Brain Maturity Using fMRI</a> - Nico U.F. Dosenbach et al, Science. September, 2010.</li>
                <li><a href="http://www.cell.com/neuron/abstract/S0896-6273%2810%2900421-6" target="_blank">A Parcellation Scheme for Human Left Lateral Parietal Cortex</a> - Steven M. Nelson et al, Neuron. July, 2010.</li>
            </ul>
        </div>
        <div class="sidebox-bottom"></div>

        <div class="sidebox-bottom"></div>

        <div class="sidebox readingList">
            <h2>History of Connectomics</h2>
            <ul>
                <li><a href="http://www.ploscompbiol.org/article/info:doi/10.1371/journal.pcbi.0010042">The Human Connectome: A Structural Description of the Human Brain</a> - Olaf Sporns, et al. PLoS, September 2005. </li>
                <li><a href="http://www.ploscompbiol.org/article/info:doi/10.1371/journal.pcbi.0010042">From Diffusion MRI to Brain Connectomics</a> - Patric Hagmann. Thesis Presentation, &Eacute;cole Polytechnique F&eacute;d&eacute;rale De Lausanne,  2005.</li>
                <li><a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC140943/">Functional Connectivity in the Resting Brain: A Network Analysis of the Default Mode Hypothesis</a> - Michael D. Greicius, et al. PNAS, December 2002</li>
                <li><a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC14647/?tool=pmcentrez">A Default Mode of Brain Function</a> - Marcus E. Raichle, et al. PNAS, January 2001.</li>

            </ul>
        </div>
        <div class="sidebox-bottom"></div>

        <!-- InstanceEndEditable -->
        <p>&nbsp;</p>
    </div>

    <!-- editable sidebar content region -->
</div> <!-- sidebar -->

</div> <!-- middle -->

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a> - <a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a></span><br />
<a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a> - <a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a><br />
<a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a> - <a href="http://duke.edu/" title="Duke University" target="_new">Duke University</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>. 
</p>

<p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p> 

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>

<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
<!-- InstanceEnd --></html>
