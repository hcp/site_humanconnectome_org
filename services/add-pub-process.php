<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/hcp-temp-wide.dwt" codeOutsideHTMLIsLocked="false" --><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Human Connectome Project</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<meta http-equiv="Description" content="Welcome to the website for the Human Connectome Project, whose purpose is to map the neural pathways that make up the human brain. This project is funded by a grant from the National Institutes for Health." />
<meta http-equiv="Keywords" content="Human Connectome Project, Connectome, HCP, neural pathways, brain mapping, brain connectivity, brain circuitry, alzheimer's research, psychiatric disorders, neuroimaging, functional MRI, PETT scan, NIH, National Institutes for Health" />
<!-- InstanceEndEditable -->
<link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/global.js"></script>

</head>

<body>
<!-- site container -->
<div id="site_container">

<div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div>

<!-- Header -->
<div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
  <map name="Header" id="Header">
    <area shape="rect" coords="14,7,404,123" href="/" alt="Human Connectome Project Logo" />
  </map>
</div> 
<!-- end Header -->

<!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" -->
<div id="topnav">
  <ul id="nav">
    <li><a href="/">Home</a></li>
    <li><a href="/about/">About the Project</a>
      <ul>
        <li><a href="/about/project/">Project Overview &amp; Components</a></li>
        <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
        <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
        <li><a href="/about/teams.html">Operational Teams</a></li>
        <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
        <li><a href="/about/publications.html">Publications</a></li>
        <li><a href="/courses">HCP Course Materials</a></li>
        <li><a href="/about/pressroom/">News &amp; Updates</a></li>
      </ul>
    </li>
    <li><a href="/data/">Data</a>
      <ul>
        <li><a href="/data/">Get Access to Public Data Releases</a></li>
        <li><a href="/data/connectome-in-a-box.html">Order Connectome in a Box</a></li>
        <li><a href="/data/data-use-terms/">HCP Data Use Terms</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Home" target="_blank">HCP Wiki: Data Resources</a></li>
      </ul>
    </li>
    <li><a href="/software/">Software</a>
      <ul>
        <li><a href="/software/connectome-workbench.html">What is Connectome Workbench</a></li>
        <li><a href="/software/get-connectome-workbench.html">Get Connectome Workbench</a></li>
        <li><a href="/software/workbench-command.php">Using Workbench Command</a></li>
        <li><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></li>
        <li><a href="/documentation/HCP-pipelines/index.html">HCP MR Pipelines</a></li>
        <li><a href="/documentation/HCP-pipelines/meg-pipeline.html">HCP MEG Pipelines</a></li>
      </ul>
    </li>
    <li><a href="/documentation/">Documentation</a>
      <ul>
      	<li><a href="/documentation/">All Documentation</a></li>
        <li><a href="/documentation/citations.html">How to Cite HCP</a></li>
        <li><a href="/documentation/tutorials/">Software Tutorials</a></li>
        <li><a href="/documentation/S900/">900 Subjects Data Release</a></li>
        <li><a href="/documentation/HCP-pipelines/">HCP Pipelines</a></li>
          <li><a href="/documentation/subject-key">How to Cite HCP Subjects with a Subject Key</a></li>
      </ul>
    </li>
    <li><a href="/contact/">Contact</a>
      <ul class="subnav">
      	<li><a href="/contact/">General Project Inquiries</a></li>
        <li><a href="/contact/#subscribe">Subscribe to Mailing Lists</a></li>
        <li><a href="/careers/">Career Opportunities</a></li>
        <li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
        <li> <a href="/contact/feature-request.php">Feature Request</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Media+Images+from+the+Human+Connectome+Project+WU-Minn+Consortium" target="_blank">HCP Wiki: Media Images</a></li>
      </ul>
    </li>
    <li><a href="/resources/">Other Resources</a>
      <ul class="subnav">
        <li><a href="http://wiki.humanconnectome.org/" target="_blank">HCP Wiki</a></li>
        <li><a href="http://lifespan.humanconnectome.org/" target="_blank">HCP Lifespan Pilot Project</a></li>
        <li><a href="/documentation/MGH-diffusion/">MGH Adult Diffusion Project</a></li>        
        <li><a href="/resources/faq-disease-related-connectome-research.html">FAQ: Disease-Related Connectome Research</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="search">
    <!--   If Javascript is not enabled, nothing will display here -->
    <script>
      (function() {
        var cx = '000073750279216385221:vnupk6whx-c';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      })();
    </script>
    <gcse:search></gcse:search>	
</div> <!-- Search box -->
<link rel="stylesheet" href="/css/google-cse-customizations.css" />
<!-- #EndLibraryItem --><!-- End Nav -->

<!-- Breadcrumbs -->
<div id="breadcrumbs">
  <p>&raquo; <!-- InstanceBeginEditable name="Breadcrumbs" -->services &gt; add publication<!-- InstanceEndEditable --></p></div>
<!-- end breadcrumbs -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">
  <div id="content-wide" class="editable"> 
  <!-- InstanceBeginEditable name="Main Content" -->
  	<link type="text/css" rel="stylesheet" href="/css/data.css" />
    <h1>Add Publication (Form Handler)</h1>
    <div id="errors"></div>
    <?php 
		/* db login */
		if ($_SERVER['HTTP_HOST']=='dev.humanconnectome.org') : 
			$dbname = 'db101945_hcp_dev';
		else : 
			$dbname = 'db101945_hcpp';
		endif; 
		$dbuser = 'db101945';
		$dbpass = 'N0seF@c3';
		$dbhost = $_ENV{DATABASE_SERVER};
		$db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
	
		if (!$db) {
			die('Connect Error (' . mysqli_connect_errno() . ') '
					. mysqli_connect_error());
		}
		
		// set Nullable values
        $pmid = ($_POST['pmid']) ? "'".$_POST['pmid']."'" : "NULL";
        $uid = ($_POST['pmid']) ? $_POST['pmid'] : time(); // creates a unique identifier if none exists.
        $doi = ($_POST['doi']) ? "'".$_POST['doi']."'" : "NULL";
        $pubTimestamp = ($_POST['timestamp']) ? "'".$_POST['timestamp']."'" : "NULL";
        $pubSummary = ($_POST['PubSummary']) ? "'".nl2br($_POST['PubSummary'])."'" : "NULL";
        $thumbnailPath = ($_POST['ThumbnailPath']) ? "'".$_POST['ThumbnailPath']."'" : "NULL";

        /* check for existence of article */

        echo "<p>PMID: ".$pmid."</p>";

        if (!$_POST['pmid']) :
            $q = "INSERT INTO pubs(PMID, UID, Authors, Title, PubTitle, PubDate, doi, PubTimestamp, Type, ThumbnailPath, PubSummary) VALUES (".$pmid.",'".$uid."','".$_POST['authors']."','".$_POST['title']."','".$_POST['pubTitle']."','".$_POST['pubDate']."',".$doi.",".$pubTimestamp.",'".$_POST['Type']."',".$thumbnailPath.",".$pubSummary.")";
            $r = mysqli_query($db,$q) or die("<p>".$q."</p><p>".$db->error."</p>");
            echo "<p>Added new article to database</p>";
            echo "<p>".$q."</p>";

        else :
            $q = "SELECT * FROM pubs WHERE uid='".$uid."';";
            $r = mysqli_query($db,$q) or die("<p>".$q."</p><p>".$db->error."</p>");

            if (mysqli_num_rows($r) > 0) :
                $q = "UPDATE pubs SET
                    Authors='".$_POST['authors']."',
                    Title='".$_POST['title']."',
                    PubTitle='".$_POST['pubTitle']."',
                    PubDate='".$_POST['pubDate']."',
                    PubSummary=".$pubSummary.",
                    doi=".$doi.",
                    PubTimestamp=".$pubTimestamp.",
                    Type='".$_POST['Type']."',
                    ThumbnailPath=".$thumbnailPath."
                    WHERE uid = '".$uid."';";
                $r = mysqli_query($db,$q) or die("<p>".$q."</p><p>".$db->error."</p>");

                echo "<p>Updated this article record.</p>";
                echo "<p>".$q."</p>";
            else :

                $q = "INSERT INTO pubs(PMID, UID, Authors, Title, PubTitle, PubDate, doi, PubTimestamp, Type, ThumbnailPath, PubSummary) VALUES (".$pmid.",'".$uid."','".$_POST['authors']."','".$_POST['title']."','".$_POST['pubTitle']."','".$_POST['pubDate']."',".$doi.",".$pubTimestamp.",'".$_POST['Type']."',".$thumbnailPath.",".$pubSummary.")";
                $r = mysqli_query($db,$q) or die("<p>".$q."</p><p>".$db->error."</p>");
                echo "<p>Added new article to database</p>";
                echo "<p>".$q."</p>";

            endif;
		endif;
		
		
		/* db connection close */
		mysqli_close($db);
	?>
    <p><a href="/services/add-publication.php">Add another publication?</a></p>
<!-- InstanceEndEditable -->  
</div>

  <!-- Primary page Content -->

    
</div> <!-- middle -->

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a> - <a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a></span><br />
<a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a> - <a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a><br />
<a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a> - <a href="http://duke.edu/" title="Duke University" target="_new">Duke University</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>. 
</p>

<p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p> 

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="http://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
/*
*  How to restrict a search to a Custom Search Engine
*  http://www.google.com/cse/
*/

google.load('search', '1');

function OnLoad() {
  // Create a search control
  var searchControl = new google.search.SearchControl();

  // Add in a WebSearch
  var webSearch = new google.search.WebSearch();

  // Restrict our search to pages from our CSE
  webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

  // Add the searcher to the SearchControl
  searchControl.addSearcher(webSearch);

  // tell the searcher to draw itself and tell it where to attach
  searchControl.draw(document.getElementById("search"));

  // execute an inital search
  // searchControl.execute('');
}

google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18630139-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
<!-- InstanceEnd --></html>
