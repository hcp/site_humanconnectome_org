<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Confirm HCP Data Order | Human Connectome Project</title>
    <meta http-equiv="Description" content="Order confirmation page." />
    <link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
    <link href="/css/sortable.css" rel="stylesheet" type="text/css" />
    <link href="/css/data.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<!--    <script src="//d79i1fxsrar4t.cloudfront.net/jquery.liveaddress/2.4/jquery.liveaddress.min.js"></script>
    <script>jQuery.LiveAddress("4149195827605618025");</script> -->
    <script type="text/javascript" src="/js/jquery.easing.1.2.js"></script>
    <script type="text/javascript" src="/js/global.js"></script>
    <Script type="text/javascript" src="/js/data-access.js"></script>

</head>

<body>


<!-- site container -->
<div id="site_container">

<div id="NIH_header2">
    <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div>

<!-- Header -->
<div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
    <map name="Header" id="Header">
        <area shape="rect" coords="14,7,400,123" href="/" alt="Human Connectome Project Logo" />
    </map>
</div>
<!-- end Header -->

<!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" -->
<div id="topnav">
  <ul id="nav">
    <li><a href="/">Home</a></li>
    <li><a href="/about/">About the Project</a>
      <ul>
        <li><a href="/about/project/">Project Overview &amp; Components</a></li>
        <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
        <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
        <li><a href="/about/teams.html">Operational Teams</a></li>
        <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
        <li><a href="/about/publications.html">Publications</a></li>
        <li><a href="/courses">HCP Course Materials</a></li>
        <li><a href="/about/pressroom/">News &amp; Updates</a></li>
      </ul>
    </li>
    <li><a href="/data/">Data</a>
      <ul>
        <li><a href="/data/">Get Access to Public Data Releases</a></li>
        <li><a href="/data/connectome-in-a-box.html">Order Connectome in a Box</a></li>
        <li><a href="/data/data-use-terms/">HCP Data Use Terms</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Home" target="_blank">HCP Wiki: Data Resources</a></li>
      </ul>
    </li>
    <li><a href="/software/">Software</a>
      <ul>
        <li><a href="/software/connectome-workbench.html">What is Connectome Workbench</a></li>
        <li><a href="/software/get-connectome-workbench.html">Get Connectome Workbench</a></li>
        <li><a href="/software/workbench-command.php">Using Workbench Command</a></li>
        <li><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></li>
        <li><a href="/documentation/HCP-pipelines/index.html">HCP MR Pipelines</a></li>
        <li><a href="/documentation/HCP-pipelines/meg-pipeline.html">HCP MEG Pipelines</a></li>
      </ul>
    </li>
    <li><a href="/documentation/">Documentation</a>
      <ul>
      	<li><a href="/documentation/">All Documentation</a></li>
        <li><a href="/documentation/citations.html">How to Cite HCP</a></li>
        <li><a href="/documentation/tutorials/">Software Tutorials</a></li>
        <li><a href="/documentation/S900/">900 Subjects Data Release</a></li>
        <li><a href="/documentation/HCP-pipelines/">HCP Pipelines</a></li>
          <li><a href="/documentation/subject-key">How to Cite HCP Subjects with a Subject Key</a></li>
      </ul>
    </li>
    <li><a href="/contact/">Contact</a>
      <ul class="subnav">
      	<li><a href="/contact/">General Project Inquiries</a></li>
        <li><a href="/contact/#subscribe">Subscribe to Mailing Lists</a></li>
        <li><a href="/careers/">Career Opportunities</a></li>
        <li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
        <li> <a href="/contact/feature-request.php">Feature Request</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Media+Images+from+the+Human+Connectome+Project+WU-Minn+Consortium" target="_blank">HCP Wiki: Media Images</a></li>
      </ul>
    </li>
    <li><a href="/resources/">Other Resources</a>
      <ul class="subnav">
        <li><a href="http://wiki.humanconnectome.org/" target="_blank">HCP Wiki</a></li>
        <li><a href="http://lifespan.humanconnectome.org/" target="_blank">HCP Lifespan Pilot Project</a></li>
        <li><a href="/documentation/MGH-diffusion/">MGH Adult Diffusion Project</a></li>        
        <li><a href="/resources/faq-disease-related-connectome-research.html">FAQ: Disease-Related Connectome Research</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="search">
    <!--   If Javascript is not enabled, nothing will display here -->
    <script>
      (function() {
        var cx = '000073750279216385221:vnupk6whx-c';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      })();
    </script>
    <gcse:search></gcse:search>	
</div> <!-- Search box -->
<link rel="stylesheet" href="/css/google-cse-customizations.css" />
<!-- #EndLibraryItem --><!-- End Nav -->

<script type="text/javascript">
    /* confirm acceptance of DUT on page open. */
    $(document).ready(function(){
        showModal('#dut-Phase2OpenAccess');
    });


</script>

<!-- Breadcrumbs -->
<div id="breadcrumbs">
    <p>&raquo; <a href="/courses/">HCP Courses</a> &gt; confirm registration details</p>
</div>
<!-- end breadcrumbs -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">
<div id="content">
<noscript>
    <p style="color:#c00; clear:both;"><strong>Alert:</strong> You appear to have JavaScript turned off. Registration and access for ConnectomeDB requires JavaScript. Please enable it in your browser before proceeding.</p>
</noscript>

<h2>Connectome In A Box: Confirm Order</h2>
<p>Please review the following order information. If any of this is incorrect, please <a href="javascript:window.history.back();">go back to the order form</a> to correct it.</p>
<?php
/* mysql connection */
$dbname = ($_SERVER['HTTP_HOST']=='dev.humanconnectome.org') ? 'devadmin_orders_dev' : 'devadmin_orders';
$dbuser = 'devadmin_drives';
$dbpass = '1IXQe6NUdnVB';
$dbhost = $_ENV{DATABASE_SERVER};
$db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

if (!$db) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
}
mysqli_set_charset($db,"utf8");
?>

<?php
/* create entry in 'orders' database and capture its just-created ID field */
$neworder = mysqli_query($db,'INSERT INTO orders (timestamp,status) VALUES (NOW(),\'incomplete\')');
$orderIds = mysqli_fetch_array(mysqli_query($db,'SELECT id FROM orders ORDER BY id DESC LIMIT 1'));
$orderId = $orderIds[0];
$courses_ordered = '';
$cnum = 0;
foreach ($_POST['itemcode'] as $key => $value) :
    if ($key > 0) $courses_ordered .= ', '; // add a separator if user ordered more than one course.
    $courses_ordered .= $value;
endforeach;
?>

<?php
/* look up customer by email address and either match, or create customer ID */
$q = "SELECT COUNT(*) as rows,customer_id as id,shipping_country FROM orders WHERE customer_email = '".$_POST['customer_email']."';";
$customer_record = mysqli_fetch_array(mysqli_query($db,$q));
if (!$customer_record['id']) :
    // generate country code
    $q = "SELECT * FROM country_codes WHERE name = '".$_POST['HCP-COUNTRY']."';";
    $r = mysqli_query($db,$q) or die($q);
    $countryinfo = mysqli_fetch_array($r);
    ?><p><?php implode(",",$countryinfo); ?></p><?php
    $str1 = $countryinfo['dial_code'];
    while (strlen($str1) < 4) :
        $str1 = "0".$str1;
    endwhile;
    $str2 = $orderId;
    while (strlen($str2) < 4) :
        $str2 = "0".$str2;
    endwhile;
    $customerID = "H".$str1.$str2;
else :
    $customerID = $customer_record['id'];
endif;
?>

<?php
/* enter the order info into the order DB */
$q = "UPDATE orders SET order_type='course',
                        customer_name='".$_POST['customer_name']."',
						customer_id='".$customerID."',
						customer_email='".$_POST['customer_email']."', 
						customer_institution='".$_POST['customer_institution']."',
						shipping_country='".$_POST['HCP-COUNTRY']."', 
						shipping_phone='".$_POST['shipping_phone']."',
						course_registered='".implode(", ",$_POST['courseid'])."',
						registration_type='".$_POST['regtype'][0]."'
						WHERE id='".$orderId."';";
$r = mysqli_query($db,$q);

if ($_POST['customer_pi']) mysqli_query($db,"UPDATE orders SET customer_pi='".$_POST['customer_pi']."' WHERE id='".$orderId."';");
?>

<?php
/* set array variables for product orders */
$items = $_POST['courseid'];
$amounts = $_POST['item-price'];
$itemcount = $_POST['itemcnt'];
$subtotal = array_sum($amounts); // + $discount

/* Calculate shipping cost. Include a 2.25% Cashnet fee that gets added on top. */
// shipping costs determined by Mellie Euler. Last updated Jun 2, 2013.
$country = $_POST['HCP-COUNTRY'];

// calculate CashNet fee as 2.25% of every dollar processed as payment, and add it to the S&H.
$cashNet = $subtotal*(0.0225);
// echo "<p>You were charged $".number_format($cashNet,2)." in CashNet fees</p>";

?>

<!-- raw post contents -->
<div style="background-color:#f0f0f0; padding:6px;" class="hidden">
    <?php
    printArray($_POST);

    function printArray($array){
        echo "<ul>";
        foreach ($array as $key => $value){
            echo "<li>$key => $value</li>";
            if(is_array($value)){ //If $value is an array, print it as well!
                printArray($value);
            }
        }
        echo "</ul>";
    }

    ?>
</div>

<!-- report to customer -->
<p><strong>Order ID: <?php echo $orderId; ?></strong></p>
<table cellspacing="1" cellpadding="3" width="100%" class="sortable" style="margin-bottom: 17px;">
    <thead>
    <tr><th style="background-color:#06c;">Customer ID: <?php echo $customerID ?></th><th>Registration Type</th><th>Cost</th></tr>
    </thead>
    <tbody>

    <?php
    if ($items) :
        foreach ($items as $key => $value) {
            $q = "SELECT * FROM courses WHERE course_id='".$value."'";
            $course = mysqli_fetch_array(mysqli_query($db,$q));
            ?>
            <tr><td><?php echo $course['course_title']; ?></td><td><?php echo $_POST['regtype'][$key] ?></td><td align="right">$<?php echo number_format(floatval($amounts[$key]),2) ?></td></tr>
        <?php
        } // end item listing
    endif;
    ?>

        <tr><td colspan="2">Transaction Fee (bundled into final "Shipping &amp; Handling" charge)</td><td align="right">$<?php echo number_format($cashNet,2); ?></td></tr>
        <tr><td colspan="2"><strong>Total cost:</strong></td><td align="right"><strong>$<?php echo number_format($subtotal + $cashNet,2) ; ?></strong></td></tr>
    </tbody>
</table>



<!-- BEGIN ORDER FORM -->

<?php if ($_SERVER['HTTP_HOST'] == 'dev.humanconnectome.org') : ?>
<!-- test storefront - no live transactions -->
<form class="hcpForm" action="https://commerce.cashnet.com/404handler/pageredirpost.aspx?Virtual=HCPtest" method="post" style="position:relative">
<p style="position: absolute; color: red; bottom: 0px; left: 0px;">Test Storefront - no transactions will be processed.</p>
<?php else : ?>
    <!-- LIVE storefront! -->
<form class="hcpForm" action="https://commerce.cashnet.com/404handler/pageredirpost.aspx?Virtual=HCP" method="post">
<?php endif; ?>

<h3>Registrant Information</h3>
<p><?php echo $_POST["customer_name"]?><br />
    <?php echo ($_POST["customer_institution"]) ? $_POST["customer_institution"]."<br />" : "" ?>
    <?php echo $_POST["HCP-COUNTRY"]?><br />
    <?php echo $_POST["shipping_phone"]?><br />
    <?php echo $_POST["customer_email"]?></p>

    <h3>Billing Information</h3>

    <p><?php echo $_POST["HCP-NAME_G"]?><br />
        <?php echo ($_POST["HCP-INSTITUTION"]) ? $_POST["HCP-INSTITUTION"]."<br />" : "" ?>
        <?php echo $_POST["HCP-ADDR_G"]?><br />
        <?php echo ($_POST["billing_office"]) ? $_POST["billing_office"]."<br />" : "" ?>
        <?php echo $_POST["HCP-CITY_G"]?>, <?php echo $_POST["billing_state"]?><br />
        <?php echo $_POST["HCP-ZIP_G"]?><br />
        <?php echo $_POST["HCP-COUNTRY"]?><br />
        <?php echo $_POST["HCP-PHONE"]?><br />
        <?php echo $_POST["HCP-EMAIL_G"]?></p>
    <input type="hidden" name="HCP-NAME_G" value="<?php echo $_POST['HCP-NAME_G'] ?>" />
    <input type="hidden" name="HCP-EMAIL_G" value="<?php echo $_POST['HCP-EMAIL_G'] ?>" />
    <input type="hidden" name="HCP-INSTITUTION" value="<?php echo ($b ? $_POST['HCP-INSTITUTION'] : '') ?>" />
    <input type="hidden" name="HCP-ADDR_G" value="<?php echo $_POST['HCP-ADDR_G']." ".$_POST['billing_office'] ?>" />
    <input type="hidden" name="HCP-CITY_G" value="<?php echo $_POST['HCP-CITY_G'] ?>" />
    <input type="hidden" name="HCP-STATE_G" value="<?php echo $_POST['billing_state'] ?>" />
    <input type="hidden" name="HCP-ZIP_G" value="<?php echo $_POST['HCP-ZIP_G'] ?>" />
    <input type="hidden" name="HCP-COUNTRY" value="<?php echo $_POST['HCP-COUNTRY'] ?>" />
    <?php
    $q = "UPDATE orders SET billing_contact='".$_POST['HCP-NAME_G']."',
                            billing_address='".$_POST['HCP-ADDR_G']." ".$_POST['billing_office']."',
                            billing_city='".$_POST['HCP-CITY_G']."',
                            billing_state='".$_POST['billing_state']."',
                            billing_postal_code='".$_POST['HCP-ZIP_G']."',
                            billing_country='".$_POST['HCP-COUNTRY']."' WHERE id='".$orderId."';";
    $r = mysqli_query($db,$q);
    ?>



    <?php
/*
 * CONVERT INHERITED FORM VALUES FROM 'POST' TO CASHNET FORMAT
 */
?>

<input type="hidden" name="itemcnt" value="<?php echo $_POST['itemcnt']; ?>" />
<input type="hidden" name="orderId" value="<?php echo $orderId ?>" />
<input type="hidden" name="HCP-ORDERID" value="<?php echo $orderId ?>" />



<?php
$item=0;
if ($items) :
    foreach($items as $key => $value) {
        $item = $key+1;

        ?>
        <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="<?php echo $_POST['itemcode'][$key] ?>" />
        <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo number_format($amounts[$key],2) ?>" />
        <input type="hidden" name="<?php echo 'qty'.$item ?>" value="1" />
        <input type="hidden" name="<?php echo 'ref1type'.$item ?>" value="HCP-REGISTRANT" />
        <input type="hidden" name="<?php echo 'ref1val'.$item ?>" value="<?php echo $_POST['customer_name'] ?>" />
        <input type="hidden" name="<?php echo 'ref2type'.$item ?>" value="HCP-REGTYPE" />
        <input type="hidden" name="<?php echo 'ref2val'.$item ?>" value="<?php echo $_POST['regtype'][$key] ?>" />
        <input type="hidden" name="<?php echo 'ref3type'.$item ?>" value="HCP-COURSEID" />
        <input type="hidden" name="<?php echo 'ref3val'.$item ?>" value="<?php echo $_POST['courseid'][$key] ?>" />
        <input type="hidden" name="<?php echo 'ref4type'.$item ?>" value="HCP-COURSENAME" />
        <input type="hidden" name="<?php echo 'ref4val'.$item ?>" value="<?php echo $_POST['coursename'][$key] ?>" />

    <?php
    } // end item (drive) entry
endif;
$item ++;
?>
        <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="HCP-FEE" />
        <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo number_format($cashNet,2) ?>" />
<?
$q = "INSERT INTO receipt (order_id,registration_cost,cashnet_fee) VALUES ('".$orderId."','".array_sum($amounts)."','".number_format($cashNet,2)."');";
$r = mysqli_query($db,$q) or die($q);

if ($_POST['justification']) :
    // store justification for price change in receipt notes.
    $q = "UPDATE receipt SET receipt_notes = '".stripslashes($_POST['justification'])."' WHERE order_id='".$orderId."';";
    $r = mysqli_query($db,$q) or die($q);
endif;

?>
<div class="alertBox">
    <p><strong>Updated Cancellation Policy</strong><br />
        We can no longer offer a full refund on any cancellations of registrations for the "Exploring the Human Connectome" course. (<a href="2015/exploring-the-human-connectome.php#cancellation" target="_blank">Cancellation Policy</a>). Please order with care.
    </p>
</div>
<p align="right" style="margin-bottom:0;"><input type="submit" value="Proceed to Checkout" /></p>

</form>
<p></p>
<p>Credit card information will be gathered at checkout. Forms of payment we accept:</p>
<p><img src="/img/icons/payment-American-Express.png" alt="AMEX" /> <img src="/img/icons/payment-Discover.png" alt="Discover" /> <img src="/img/icons/payment-Mastercard.png" alt="Mastercard" /> <img src="/img/icons/payment-Visa.png" alt="Visa" /> <img src="/img/icons/payment-JCB.png" alt="JCB" /></p>


<?php
mysqli_close($db);
?>



</div>
<!-- /#Content -->

<div id="sidebar-rt">

    <div id="sidebar-content">
        <div class="sidebox databox">
            <h2>HCP Course Details</h2>
            <p><img src="/img/art/hcp-course-2015-honolulu.jpg" alt="Exploring the Human Connectome" /></p>
            <p>The 2015 HCP Course is designed for investigators who are interested in:</p>
            <ul>
                <li>using data being collected and distributed by HCP</li>
                <li>acquiring and analyzing HCP-style imaging and behavioral data at your own institution</li>
                <li>processing your own non-HCP data using HCP pipelines and methods</li>
                <li>learning to use Connectome Workbench tools and the CIFTI connectivity data format</li>
                <li>learning HCP multi-modal neuroimaging analysis methods, including those that combine MEG and MRI data</li>
            </ul>
            <p>For more details, see the HCP Course Website</p>
        </div>
        <!-- /sidebox -->
    </div>
</div>
<!-- /sidebar -->


</div> <!-- middle -->

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a> - <a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a></span><br />
<a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a> - <a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a><br />
<a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a> - <a href="http://duke.edu/" title="Duke University" target="_new">Duke University</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>. 
</p>

<p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p> 

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="http://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
