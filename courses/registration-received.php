<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Data | Human Connectome Project</title>
    <meta http-equiv="Description" content="Public access to HCP data. Currently, Phase 1 Pilot Data is available." />
    <meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
    <link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
    <link href="/css/sortable.css" rel="stylesheet" type="text/css" />
    <link href="/css/data.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/js/jquery.easing.1.2.js"></script>
    <script type="text/javascript" src="/js/global.js"></script>


</head>

<body>
<!-- site container -->
<div id="site_container">

<div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div>

<!-- Header -->
<div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
    <map name="Header" id="Header">
        <area shape="rect" coords="14,7,400,123" href="/" alt="Human Connectome Project Logo" />
    </map>
</div>
<!-- end Header -->

<!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" -->
<div id="topnav">
  <ul id="nav">
    <li><a href="/">Home</a></li>
    <li><a href="/about/">About the Project</a>
      <ul>
        <li><a href="/about/project/">Project Overview &amp; Components</a></li>
        <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
        <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
        <li><a href="/about/teams.html">Operational Teams</a></li>
        <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
        <li><a href="/about/publications.html">Publications</a></li>
        <li><a href="/courses">HCP Course Materials</a></li>
        <li><a href="/about/pressroom/">News &amp; Updates</a></li>
      </ul>
    </li>
    <li><a href="/data/">Data</a>
      <ul>
        <li><a href="/data/">Get Access to Public Data Releases</a></li>
        <li><a href="/data/connectome-in-a-box.html">Order Connectome in a Box</a></li>
        <li><a href="/data/data-use-terms/">HCP Data Use Terms</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Home" target="_blank">HCP Wiki: Data Resources</a></li>
      </ul>
    </li>
    <li><a href="/software/">Software</a>
      <ul>
        <li><a href="/software/connectome-workbench.html">What is Connectome Workbench</a></li>
        <li><a href="/software/get-connectome-workbench.html">Get Connectome Workbench</a></li>
        <li><a href="/software/workbench-command.php">Using Workbench Command</a></li>
        <li><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></li>
        <li><a href="/documentation/HCP-pipelines/index.html">HCP MR Pipelines</a></li>
        <li><a href="/documentation/HCP-pipelines/meg-pipeline.html">HCP MEG Pipelines</a></li>
      </ul>
    </li>
    <li><a href="/documentation/">Documentation</a>
      <ul>
      	<li><a href="/documentation/">All Documentation</a></li>
        <li><a href="/documentation/citations.html">How to Cite HCP</a></li>
        <li><a href="/documentation/tutorials/">Software Tutorials</a></li>
        <li><a href="/documentation/S900/">900 Subjects Data Release</a></li>
        <li><a href="/documentation/HCP-pipelines/">HCP Pipelines</a></li>
          <li><a href="/documentation/subject-key">How to Cite HCP Subjects with a Subject Key</a></li>
      </ul>
    </li>
    <li><a href="/contact/">Contact</a>
      <ul class="subnav">
      	<li><a href="/contact/">General Project Inquiries</a></li>
        <li><a href="/contact/#subscribe">Subscribe to Mailing Lists</a></li>
        <li><a href="/careers/">Career Opportunities</a></li>
        <li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
        <li> <a href="/contact/feature-request.php">Feature Request</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Media+Images+from+the+Human+Connectome+Project+WU-Minn+Consortium" target="_blank">HCP Wiki: Media Images</a></li>
      </ul>
    </li>
    <li><a href="/resources/">Other Resources</a>
      <ul class="subnav">
        <li><a href="http://wiki.humanconnectome.org/" target="_blank">HCP Wiki</a></li>
        <li><a href="http://lifespan.humanconnectome.org/" target="_blank">HCP Lifespan Pilot Project</a></li>
        <li><a href="/documentation/MGH-diffusion/">MGH Adult Diffusion Project</a></li>        
        <li><a href="/resources/faq-disease-related-connectome-research.html">FAQ: Disease-Related Connectome Research</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="search">
    <!--   If Javascript is not enabled, nothing will display here -->
    <script>
      (function() {
        var cx = '000073750279216385221:vnupk6whx-c';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      })();
    </script>
    <gcse:search></gcse:search>	
</div> <!-- Search box -->
<link rel="stylesheet" href="/css/google-cse-customizations.css" />
<!-- #EndLibraryItem --><!-- End Nav -->

<!-- banner -->
<div id="bannerCollapsed" class="bannerContainer hidden">
  <span>
    <a style="background:url(/img/open.png) no-repeat 2px center #000;" onClick="openBanner();" title="Show banner">Open</a>
    </span>
    <iframe height="36" width="100%" frameborder="0" scrolling="no" src="/data/banner/collapsed/"></iframe>
</div>
<!-- end banner -->

<!-- Breadcrumbs -->
<div id="breadcrumbs">
    <p>&raquo; <a href="/courses">HCP Courses</a> &gt; registration confirmation</p></div>
<!-- end breadcrumbs -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">
<div id="content">
<noscript>
    <p style="color:#c00; clear:both;"><strong>Alert:</strong> You appear to have JavaScript turned off. Registration and access for ConnectomeDB requires JavaScript. Please enable it in your browser before proceeding.</p>
</noscript>

<?php
$orderId = $_POST['HCP-ORDERID'];
if ($orderId) {
    ?>

    <?php
    /* mysql connection */
    $dbname = ($_SERVER['HTTP_HOST']=='dev.humanconnectome.org') ? 'devadmin_orders_dev' : 'devadmin_orders';
    $dbuser = 'devadmin_drives';
    $dbpass = '1IXQe6NUdnVB';
    $dbhost = $_ENV{DATABASE_SERVER};
    $db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

    if (!$db) {
        die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
    }
    mysqli_set_charset($db,"utf8");
    ?>

    <?php

    /* check for successful transaction */
    $success = $_POST['respmessage'];
    if ($success == 'SUCCESS') {

        ?>

        <h2>HCP Course Registration: Success</h2>
        <p>Thank you for your course registration. Your registration ID number is #<?php echo $orderId; ?> and your payment transaction ID is #<?php echo $_POST['tx'] ?>. Please save these for your records.</p>
        <p>You will receive an email from us soon when this order is shipped with tracking information. If any of this is incorrect, please contact us at <a href="mailto:orders@humanconnectome.org"><strong>orders@humanconnectome.org</strong></a> to correct it.</p>
        <h3>Registration Summary</h3>
        <?php
        // set variables
        $items = $_POST['itemcode'];


        $q = "SELECT * FROM receipt,orders WHERE receipt.order_id='".$orderId."' AND orders.id='".$orderId."';";
        $r = mysqli_query ($db,$q) or die($q);
        $order = mysqli_fetch_array($r);

        /* update order status in DB -- set status according to shipping country */
        $orderStatus = "open";

        $q = "UPDATE orders SET status='".$orderStatus."',transaction_id='".$_POST['tx']."' WHERE id='".$orderId."';";
        $r = mysqli_query($db,$q) or die($q);

        $q = "SELECT * FROM receipt,orders WHERE receipt.order_id='".$orderId."' AND orders.id='".$orderId."';";
        $r = mysqli_query ($db,$q) or die($q);
        $order = mysqli_fetch_array($r);

        // build array of releases to query against
        $q = "SELECT course_id FROM courses";
        $r = mysqli_query ($db,$q) or die($q);
        $courses = array();
        while ($course = mysqli_fetch_array($r)) :
            $courses[] = $course[0];
        endwhile;

        $items=array();

        for ($i=1; $i<=$_POST['itemcnt']; $i++) :
            // check for item code, then check for quantity of each drive ordered.
            // stop before you get to the last itemcode, which is for shipping
            $courseId = $_POST['ref3val'.$i];
            $qty = $_POST['qty'.$i];

            if (in_array($courseId,$courses)) :

                $q = "SELECT * FROM courses WHERE course_id='".$courseId."';";
                $r = mysqli_fetch_array(mysqli_query($db,$q));
                //	echo "<p>".$itemcode." found in [".implode(", ",$releases)."]";
                // add to items array
                $items[$i]=array('course_id' => $courseId, 'course_title' => $r['course_title'], 'cost' => $_POST['amount'.$i], 'regtype' => $_POST['ref2val'.$i]);
            else :
                echo "<p class='hidden'>".$courseId." not found in [".implode(", ",$courses)."]";
            endif;
        endfor;

        ?>
        <!-- report to customer -->
        <table cellspacing="1" cellpadding="3" width="100%" class="sortable">
            <thead>
            <tr><th style="background-color:#06c;">Order ID: <?php echo $orderId ?></th>
                <th>Registration Type</th></tr>
            </thead>
            <tbody>

            <?php
            foreach ($items as $key => $item) {
                if (in_array($item['course_id'],$courses)) :
                    ?>
                    <tr><td><?php echo $item['course_title'] ?></td><td><?php echo $item['regtype'] ?></td></tr>
                <?php
                else :
                    echo "<p>$item not found in [".implode(", ",$courses)."]";
                endif;
            } // end item listing

            ?>
            <tr><th colspan = 2>Item costs</th></tr>
            <tr><td>Total Registration Cost:</td><td align="right">$<?php echo number_format($order['registration_cost'],2) ?></td></tr>
            <?php
            if ($order['sales_tax']) {
                ?>
                <tr><td>Sales Tax: </td><td align="right">$<?php echo number_format($order['sales_tax'],2); ?></td></tr>
            <?php
            }
            ?>
            <tr><td>Transaction Fee: </td><td align="right">$<?php echo number_format($order['cashnet_fee'],2); ?></td></tr>
            <tr><td colspan="2" align="right" style="padding-top:10px;"><p><strong>Total cost: $<?php echo number_format($order['registration_cost'] + $order['cashnet_fee'],2); ?></strong></p></td></tr>


            </tbody>
        </table>


        <?php
        /* if order was unsuccessful */

    } else {

        /* update order DB */
        $q = "UPDATE orders SET status='failed',transaction_id='".$_POST['tx']."' WHERE id='".$orderId."';";
        $r = mysqli_query($db,$q) or die($q);
        ?>
        <h2>Connectome In A Box: Problem With Your Order</h2>
        <p>There was a problem with your order. Please contact <a href="mailto:orders@humanconnectome.org"><strong>orders@humanconnectome.org</strong></a> and inquire about order ID #<?php echo $_POST['HCP-ORDERID'] ?> to rectify it.</p>
        <p><strong>Error Code <?php echo $_POST['result'] ?>:</strong> <span class="error"><?php echo $_POST['respmessage'] ?> </span></p>
    <?php
    }
    ?>

    <div style="background-color:#f0f0f0; padding:6px; display:none;">
        <?php

        function printArray($array,$title){
            echo "<h3>".$title."</h3>";
            echo "<ul>";
            foreach ($array as $key => $value){
                echo "<li>$key => $value</li>";
                if(is_array($value)){ //If $value is an array, print it as well!
                    printArray($value);
                }
            }
            echo "</ul>";
        }
        printArray($order,"Data in Order table");
        echo '<hr />';
        printArray($_POST,"POST data received through form");

        ?>
    </div>

    <p style="padding-bottom:6px; border-bottom: 1px #ccc solid">&nbsp;</p>
    <?php
    mysqli_close($db);
    ?>

<?php
} else {
    ?>
    <h2>Error</h2>
    <p>No order information. If you think you got to this page in error, please contact orders@humanconnectome.org</p>

    <div style="background-color:#f0f0f0; padding:6px; display:none;">
        <?php

        function printArray($array){
            echo "<ul>";
            foreach ($array as $key => $value){
                echo "<li>$key => $value</li>";
                if(is_array($value)){ //If $value is an array, print it as well!
                    printArray($value);
                }
            }
        }
        printArray($order);
        echo '<hr />';
        printArray($_POST);

        ?>
    </div>
<?php
}
?>
</div>
<!-- /#Content -->

<div id="sidebar-rt">

    <div id="sidebar-content"><!-- #BeginLibraryItem "/Library/course-hcp2015-accommodations.lbi" --><div class="sidebox">
            <h2 align="center">Location and Accommodations</h2>
            <p><img src="/img/blog features/mariott-waikiki-beach.jpg" style="width:100%" /></p>
            <p><strong>Course Location: </strong></p>
            <p><a href="http://www.marriott.com/hotels/travel/hnlmc/?mktcmp=w_regionsite_hnlmc_x" target="_blank">Waikiki Beach Marriott Resort &amp; Spa</a><br />
            </p>
2552 Kalakaua Avenue, Oahu
            <br />
            Honolulu, Hawaii 96815
            
            <p>&nbsp;</p>
            <p><strong>Accommodations:</strong></p>
            <p> Accommodations are not included with course registration. However, a limited number of discounted rooms are available at the Marriott at <a href="https://resweb.passkey.com/Resweb.do?mode=welcome_ei_new&eventID=13777135" title="Register at the Waikiki Marriott via resweb.passkey.com">this website</a>.</p>
            <p>&nbsp;</p>
            <p><strong>Other Options:</strong></p>
            <p><a href="http://www.tripadvisor.com/Hotels-g60982-Honolulu_Oahu_Hawaii-Hotels.html" target="_blank">Tripadvisor.com: local hotels in Waikiki Beach</a></p>
            <p>&nbsp;</p>
            <p>If you would like to share accommodation with another attendee (of either the HCP or FSL Courses), you may enter your contact information on our <a href="http://documents.fmrib.ox.ac.uk/documentsWiki/Public/FSLCourseRoomSharing" target="_blank">publicly-viewable wiki page</a>.</p>
          </div><!-- #EndLibraryItem --><!-- /sidebox -->
    </div>
</div>
<!-- /sidebar -->


</div> <!-- middle -->

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a> - <a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a></span><br />
<a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a> - <a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a><br />
<a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a> - <a href="http://duke.edu/" title="Duke University" target="_new">Duke University</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>. 
</p>

<p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p> 

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="http://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
