<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Order Connectome in a Box | Human Connectome Project</title>
    <meta http-equiv="Description" content="Public access to HCP data. Currently, Phase 1 Pilot Data is available." />
    <meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
    <link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
    <link href="/css/data.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script src="//d79i1fxsrar4t.cloudfront.net/jquery.liveaddress/2.4/jquery.liveaddress.min.js"></script>
    <script>jQuery.LiveAddress("4149195827605618025");</script>
    <script type="text/javascript" src="/js/jquery.easing.1.2.js"></script>
    <script type="text/javascript" src="/js/global.js"></script>
    <script type="text/javascript" src="/js/dut-controls.js"></script>

</head>

<body>

<!-- site container -->
<div id="site_container">

<div id="NIH_header2">
    <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div>

<!-- Header -->
<div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
    <map name="Header" id="Header">
        <area shape="rect" coords="14,7,400,123" href="/" alt="Human Connectome Project Logo" />
    </map>
</div>
<!-- end Header -->

<!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" -->
<div id="topnav">
  <ul id="nav">
    <li><a href="/">Home</a></li>
    <li><a href="/about/">About the Project</a>
      <ul>
        <li><a href="/about/project/">Project Overview &amp; Components</a></li>
        <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
        <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
        <li><a href="/about/teams.html">Operational Teams</a></li>
        <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
        <li><a href="/about/publications.html">Publications</a></li>
        <li><a href="/courses">HCP Course Materials</a></li>
        <li><a href="/about/pressroom/">News &amp; Updates</a></li>
      </ul>
    </li>
    <li><a href="/data/">Data</a>
      <ul>
        <li><a href="/data/">Get Access to Public Data Releases</a></li>
        <li><a href="/data/connectome-in-a-box.html">Order Connectome in a Box</a></li>
        <li><a href="/data/data-use-terms/">HCP Data Use Terms</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Home" target="_blank">HCP Wiki: Data Resources</a></li>
      </ul>
    </li>
    <li><a href="/software/">Software</a>
      <ul>
        <li><a href="/software/connectome-workbench.html">What is Connectome Workbench</a></li>
        <li><a href="/software/get-connectome-workbench.html">Get Connectome Workbench</a></li>
        <li><a href="/software/workbench-command.php">Using Workbench Command</a></li>
        <li><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></li>
        <li><a href="/documentation/HCP-pipelines/index.html">HCP MR Pipelines</a></li>
        <li><a href="/documentation/HCP-pipelines/meg-pipeline.html">HCP MEG Pipelines</a></li>
      </ul>
    </li>
    <li><a href="/documentation/">Documentation</a>
      <ul>
      	<li><a href="/documentation/">All Documentation</a></li>
        <li><a href="/documentation/citations.html">How to Cite HCP</a></li>
        <li><a href="/documentation/tutorials/">Software Tutorials</a></li>
        <li><a href="/documentation/S900/">900 Subjects Data Release</a></li>
        <li><a href="/documentation/HCP-pipelines/">HCP Pipelines</a></li>
          <li><a href="/documentation/subject-key">How to Cite HCP Subjects with a Subject Key</a></li>
      </ul>
    </li>
    <li><a href="/contact/">Contact</a>
      <ul class="subnav">
      	<li><a href="/contact/">General Project Inquiries</a></li>
        <li><a href="/contact/#subscribe">Subscribe to Mailing Lists</a></li>
        <li><a href="/careers/">Career Opportunities</a></li>
        <li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
        <li> <a href="/contact/feature-request.php">Feature Request</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Media+Images+from+the+Human+Connectome+Project+WU-Minn+Consortium" target="_blank">HCP Wiki: Media Images</a></li>
      </ul>
    </li>
    <li><a href="/resources/">Other Resources</a>
      <ul class="subnav">
        <li><a href="http://wiki.humanconnectome.org/" target="_blank">HCP Wiki</a></li>
        <li><a href="http://lifespan.humanconnectome.org/" target="_blank">HCP Lifespan Pilot Project</a></li>
        <li><a href="/documentation/MGH-diffusion/">MGH Adult Diffusion Project</a></li>
        <li><a href="/resources/faq-disease-related-connectome-research.html">FAQ: Disease-Related Connectome Research</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="search">
    <!--   If Javascript is not enabled, nothing will display here -->
    <script>
      (function() {
        var cx = '000073750279216385221:vnupk6whx-c';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      })();
    </script>
    <gcse:search></gcse:search>
</div> <!-- Search box -->
<link rel="stylesheet" href="/css/google-cse-customizations.css" />
<!-- #EndLibraryItem --><!-- End Nav -->

<!-- Breadcrumbs -->
<div id="breadcrumbs">
    <p>&raquo; <a href="/courses/">HCP Courses</a> &gt; course registration</p></div>
<!-- end breadcrumbs -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">
<div id="content">
<noscript>
    <p style="color:#c00; clear:both;"><strong>Alert:</strong> You appear to have JavaScript turned off. Registration and access for ConnectomeDB requires JavaScript. Please enable it in your browser before proceeding.</p>
</noscript>

<?php
/* mysql connection */

$dbname = ($_SERVER['HTTP_HOST']=='dev.humanconnectome.org') ? 'devadmin_orders_dev' : 'devadmin_orders';
$dbuser = 'devadmin_drives';
$dbpass = '1IXQe6NUdnVB';
$dbhost = $_ENV{DATABASE_SERVER};
$db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

if (!$db) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
}
?>

<h2 id="storeHead">HCP Course Registration Form</h2>
<div style="background: url('/img/icon-alert-48.png') 10px center no-repeat #ffd; border:1px solid #ccc; border-radius:5px; padding:10px 10px 10px 70px; margin-bottom: 1em;" class="hidden">
    <p style="margin-bottom: 6px;"><strong>NOTICE: </strong><br />
        The rate of incoming orders has outpaced our rate of acquiring hard drives for our inventory. Normal delivery would take place within 30 days of your order. Current delivery times may take longer, until we have a fresh stock of hard drives in house.</p>
</div>

<script type="text/javascript">
    // temporary "coming soon" form disabler

     $(document).ready(function(){
     $('select').prop('disabled',true);
     $('input').prop('disabled','disabled');
     $('textarea').prop('disabled','disabled');
     $('.add-drive').hide();
     $('#cinab-order').removeProp('action');
     $('#storeHead').append(' - <a href="javascript:showModal(\'coming-soon\')">Coming Soon</a>');
     showModal('coming-soon');
     });


</script>

<form id="hcp-course-reg" class="hcpForm" method="post" action="/course-registration/confirm-registration.php">

<script type="text/javascript" src="/js/registration-validation.js"></script>

<h3>1: Registration Selection</h3>
<input type="hidden" name="itemcnt" id="itemcnt" value="0" />

<div class="hcpForm-wrapper" id="product-info" style="overflow:auto;">
    <div class="hidden error-messages" id="error-message-product">
        <p><strong>Errors Found:</strong></p>
        <ul class="error-list">
            <li>You're a jerk.</li>
        </ul>
    </div>

    <?php
    /* DEFINE FUNCTION TO HANDLE PRODUCT DISPLAY.
     * Requires an array that has single product information
     */
    function displayProducts($course) {
        ?>
        <div class="product-box <?php echo $itemStatus ?>" id="<?php echo $course['course_id']; ?>">
            <input type="hidden" name="<?php echo $course['cashnet_id'].'qty'; ?>" value="0" class="qty item-info" disabled="disabled" />
            <input type="hidden" name="itemcode[]" value="<?php echo $course['cashnet_id'] ?>" class="itemcode item-info" disabled="disabled" />
            <input type="hidden" name="regtype[]" value="" class="regtype item-info" disabled="disabled" />
            <input type="hidden" name="courseid[]" value="<?php echo $course['course_id'] ?>" class="item-info" disabled="disabled" />
            <input type="hidden" name="coursename[]" value="<?php echo $course['course_title'] ?>" class="item-info" disabled="disabled" />
            <div class="product-description">
                <h3><?php echo $course['course_title']; ?> </h3>
                <p><?php echo $course['course_description']; ?></p>

                <p>Select Registration Type (Required):
                    <?php
                    // parse JSON listing of registration fees into a PHP array
                    $jsonString = $course['registration_fee'];
                    // $jsonString = '{ "data": [{"role":"student","cost":"900.00"},{"role":"postdoc/academic","cost":"1500.00"},{"role":"commercial","cost":"2700"}]}';
                    $feeScale = json_decode($jsonString,true);
                    // var_dump($feeScale); echo $jsonString;
                    ?>
                    <select name="item-price[]" class="item-price-select">
                        <option value="">Not Registered</option>
                        <?php foreach ($feeScale as $k=>$array) : ?>
                            <?php foreach ($array as $key=>$fees) : ?>
                                <option class="<?php echo stripslashes($fees['role']) ?>" value="<?php echo $fees['cost'] ?>" data-regtype="<?php echo $fees['role'] ?>"><?php echo $fees['role'].": $".$fees['cost'] ?></option>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </select>
                </p>
                <p><em>Note: An additional 2.25% Cashnet fee will be assessed to each registration.</em></p>
                <div class="order-panel">
                    <img src="/img/art/icon-course.png" />
                </div>
            </div> <!-- /product description -->

        </div>

    <?
    }
    /* end PRODUCT DISPLAY function
    */
    ?>

    <?php
    // get all CURRENT courses from DB and create a product picker for each
    $courses = mysqli_query($db,'SELECT * FROM courses WHERE status="current" ORDER BY course_id ASC;');
    $orderLimit = 1; // could be set in DB if desired
    while ($course = mysqli_fetch_array($courses)) {
        displayProducts($course);
    } // end product loop for current releases
    mysqli_free_result($courses);
    ?>

</div> <!-- /wrapper -->

<h3>2: Course Registrant Information</h3>

<div class="hcpForm-wrapper" id="shipping-info">
    <div class="hidden error-messages" id="error-message-address">
        <p><strong>Errors Found:</strong></p>
        <ul class="error-list">
            <li>You're crazy.</li>
        </ul>
    </div>

    <div>
        <p>You will be prompted to fill out your billing address on the next page.</p>
    </div>
    <div>
        <label for="customer_name">Name</label>
        <input type="text" name="customer_name" style="width:485px" class="required">
    </div>
    <div>
        <label for="customer_institution">Institution</label>
        <input type="text" name="customer_institution" style="width:485px" >
    </div>
    <div>
        <label for="customer_pi">Name of PI / Faculty Mentor (Optional)</label>
        <input type="text" name="customer_pi" style="width: 485px;" >
    </div>
    <div>
        <label for="customer_email">Email Address</label>
        <input type="text" name="customer_email" style="width:485px" class="required email" />
    </div>

    <div>
        <label for="shipping_phone">Contact Phone Number (optional)</label>
        <input type="text" name="shipping_phone" style="width:485px" class="" />
    </div>

</div>

<h3>3: Billing Information</h3>

<div class="hcpForm-wrapper" style="overflow:auto;">
    <div class="hidden error-messages" id="error-message-payment">
        <p><strong>Errors Found:</strong></p>
        <ul class="error-list">
            <li>You're crazy.</li>
        </ul>
    </div>
    <h3>Billing Information</h3>
    <div>
        <label for="HCP-COUNTRY">Billing Country</label>
        <select name="HCP-COUNTRY" id="country-select">
                <option value=""></option>
                <option value="United States">United States</option>
                <option value="Afghanistan">Afghanistan</option>
                <option value="Albania">Albania</option>
                <option value="Algeria">Algeria</option>
                <option value="American Samoa">American Samoa</option>
                <option value="Andorra">Andorra</option>
                <option value="Anguilla">Anguilla</option>
                <option value="Antarctica">Antarctica</option>
                <option value="Antigua And Barbuda">Antigua And Barbuda</option>
                <option value="Argentina">Argentina</option>
                <option value="Armenia">Armenia</option>
                <option value="Aruba">Aruba</option>
                <option value="Australia">Australia</option>
                <option value="Austria">Austria</option>
                <option value="Azerbaijan">Azerbaijan</option>
                <option value="Bahamas">Bahamas</option>
                <option value="Bahrain">Bahrain</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Barbados">Barbados</option>
                <option value="Belarus">Belarus</option>
                <option value="Belgium">Belgium</option>
                <option value="Belize">Belize</option>
                <option value="Benin">Benin</option>
                <option value="Bermuda">Bermuda</option>
                <option value="Bhutan">Bhutan</option>
                <option value="Bolivia">Bolivia</option>
                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                <option value="Botswana">Botswana</option>
                <option value="Bouvet Island">Bouvet Island</option>
                <option value="Brazil">Brazil</option>
                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                <option value="Brunei Darussalam">Brunei Darussalam</option>
                <option value="Bulgaria">Bulgaria</option>
                <option value="Burkina Faso">Burkina Faso</option>
                <option value="Burundi">Burundi</option>
                <option value="Cambodia">Cambodia</option>
                <option value="Cameroon">Cameroon</option>
                <option value="Canada">Canada</option>
                <option value="Cape Verde">Cape Verde</option>
                <option value="Cayman Islands">Cayman Islands</option>
                <option value="Central African Republic">Central African Republic</option>
                <option value="Chad">Chad</option>
                <option value="Chile">Chile</option>
                <option value="China">China</option>
                <option value="Christmas Island">Christmas Island</option>
                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                <option value="Colombia">Colombia</option>
                <option value="Comoros">Comoros</option>
                <option value="Congo">Congo</option>
                <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                <option value="Cook Islands">Cook Islands</option>
                <option value="Costa Rica">Costa Rica</option>
                <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                <option value="Croatia">Croatia</option>
                <option value="Cyprus">Cyprus</option>
                <option value="Czech Republic">Czech Republic</option>
                <option value="Denmark">Denmark</option>
                <option value="Djibouti">Djibouti</option>
                <option value="Dominica">Dominica</option>
                <option value="Dominican Republic">Dominican Republic</option>
                <option value="East Timor">East Timor</option>
                <option value="Ecuador">Ecuador</option>
                <option value="Egypt">Egypt</option>
                <option value="El Salvador">El Salvador</option>
                <option value="England">England</option>
                <option value="Equatorial Guinea">Equatorial Guinea</option>
                <option value="Eritrea">Eritrea</option>
                <option value="Espana">Espana</option>
                <option value="Estonia">Estonia</option>
                <option value="Ethiopia">Ethiopia</option>
                <option value="Falkland Islands">Falkland Islands</option>
                <option value="Faroe Islands">Faroe Islands</option>
                <option value="Fiji">Fiji</option>
                <option value="Finland">Finland</option>
                <option value="France">France</option>
                <option value="French Guiana">French Guiana</option>
                <option value="French Polynesia">French Polynesia</option>
                <option value="French Southern Territories">French Southern Territories</option>
                <option value="Gabon">Gabon</option>
                <option value="Gambia">Gambia</option>
                <option value="Georgia">Georgia</option>
                <option value="Germany">Germany</option>
                <option value="Ghana">Ghana</option>
                <option value="Gibraltar">Gibraltar</option>
                <option value="Great Britain">Great Britain</option>
                <option value="Greece">Greece</option>
                <option value="Greenland">Greenland</option>
                <option value="Grenada">Grenada</option>
                <option value="Guadeloupe">Guadeloupe</option>
                <option value="Guam">Guam</option>
                <option value="Guatemala">Guatemala</option>
                <option value="Guinea">Guinea</option>
                <option value="Guinea-Bissau">Guinea-Bissau</option>
                <option value="Guyana">Guyana</option>
                <option value="Haiti">Haiti</option>
                <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                <option value="Honduras">Honduras</option>
                <option value="Hong Kong">Hong Kong</option>
                <option value="Hungary">Hungary</option>
                <option value="Iceland">Iceland</option>
                <option value="India">India</option>
                <option value="Indonesia">Indonesia</option>
                <option value="IRAN">IRAN</option>
                <option value="Iraq">Iraq</option>
                <option value="Ireland">Ireland</option>
                <option value="Israel">Israel</option>
                <option value="Italy">Italy</option>
                <option value="Jamaica">Jamaica</option>
                <option value="Japan">Japan</option>
                <option value="Jordan">Jordan</option>
                <option value="Kazakhstan">Kazakhstan</option>
                <option value="Kenya">Kenya</option>
                <option value="Kiribati">Kiribati</option>
                <option value="KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF">KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF</option>
                <option value="Korea, Republic of">Korea, Republic of</option>
                <option value="Kuwait">Kuwait</option>
                <option value="Kyrgyzstan">Kyrgyzstan</option>
                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                <option value="Latvia">Latvia</option>
                <option value="Lebanon">Lebanon</option>
                <option value="Lesotho">Lesotho</option>
                <option value="Liberia">Liberia</option>
                <option value="Libya">Libya</option>
                <option value="Liechtenstein">Liechtenstein</option>
                <option value="Lithuania">Lithuania</option>
                <option value="Luxembourg">Luxembourg</option>
                <option value="MACAO">MACAO</option>
                <option value="Macedonia">Macedonia</option>
                <option value="Madagascar">Madagascar</option>
                <option value="Malawi">Malawi</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Maldives">Maldives</option>
                <option value="Mali">Mali</option>
                <option value="Malta">Malta</option>
                <option value="Marshall Islands">Marshall Islands</option>
                <option value="Martinique">Martinique</option>
                <option value="Mauritania">Mauritania</option>
                <option value="Mauritius">Mauritius</option>
                <option value="Mayotte">Mayotte</option>
                <option value="Mexico">Mexico</option>
                <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                <option value="Moldova, Republic of">Moldova, Republic of</option>
                <option value="Monaco">Monaco</option>
                <option value="Mongolia">Mongolia</option>
                <option value="Montserrat">Montserrat</option>
                <option value="Morocco">Morocco</option>
                <option value="Mozambique">Mozambique</option>
                <option value="Myanmar">Myanmar</option>
                <option value="Namibia">Namibia</option>
                <option value="Nauru">Nauru</option>
                <option value="Nepal">Nepal</option>
                <option value="Netherlands">Netherlands</option>
                <option value="Netherlands Antilles">Netherlands Antilles</option>
                <option value="New Caledonia">New Caledonia</option>
                <option value="New Zealand">New Zealand</option>
                <option value="Nicaragua">Nicaragua</option>
                <option value="Niger">Niger</option>
                <option value="Nigeria">Nigeria</option>
                <option value="Niue">Niue</option>
                <option value="Norfolk Island">Norfolk Island</option>
                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                <option value="Norway">Norway</option>
                <option value="Oman">Oman</option>
                <option value="Pakistan">Pakistan</option>
                <option value="Palau">Palau</option>
                <option value="Panama">Panama</option>
                <option value="Papua New Guinea">Papua New Guinea</option>
                <option value="Paraguay">Paraguay</option>
                <option value="Peru">Peru</option>
                <option value="Philippines">Philippines</option>
                <option value="Pitcairn">Pitcairn</option>
                <option value="Poland">Poland</option>
                <option value="Portugal">Portugal</option>
                <option value="Puerto Rico">Puerto Rico</option>
                <option value="Qatar">Qatar</option>
                <option value="Reunion">Reunion</option>
                <option value="Romania">Romania</option>
                <option value="Russia">Russia</option>
                <option value="Russian Federation">Russian Federation</option>
                <option value="Rwanda">Rwanda</option>
                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                <option value="Saint Lucia">Saint Lucia</option>
                <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                <option value="Samoa (Independent)">Samoa (Independent)</option>
                <option value="San Marino">San Marino</option>
                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                <option value="Saudi Arabia">Saudi Arabia</option>
                <option value="Scotland">Scotland</option>
                <option value="Senegal">Senegal</option>
                <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                <option value="Seychelles">Seychelles</option>
                <option value="Sierra Leone">Sierra Leone</option>
                <option value="Singapore">Singapore</option>
                <option value="Slovakia">Slovakia</option>
                <option value="Slovenia">Slovenia</option>
                <option value="Solomon Islands">Solomon Islands</option>
                <option value="Somalia">Somalia</option>
                <option value="South Africa">South Africa</option>
                <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                <option value="Spain">Spain</option>
                <option value="Sri Lanka">Sri Lanka</option>
                <option value="St. Helena">St. Helena</option>
                <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                <option value="Suriname">Suriname</option>
                <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                <option value="Swaziland">Swaziland</option>
                <option value="Sweden">Sweden</option>
                <option value="Switzerland">Switzerland</option>
                <option value="Taiwan">Taiwan</option>
                <option value="Tajikistan">Tajikistan</option>
                <option value="Tanzania">Tanzania</option>
                <option value="Thailand">Thailand</option>
                <option value="Togo">Togo</option>
                <option value="Tokelau">Tokelau</option>
                <option value="Tonga">Tonga</option>
                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option value="Tunisia">Tunisia</option>
                <option value="Turkey">Turkey</option>
                <option value="Turkmenistan">Turkmenistan</option>
                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                <option value="Tuvalu">Tuvalu</option>
                <option value="Uganda">Uganda</option>
                <option value="Ukraine">Ukraine</option>
                <option value="United Arab Emirates">United Arab Emirates</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                <option value="Uruguay">Uruguay</option>
                <option value="Uzbekistan">Uzbekistan</option>
                <option value="Vanuatu">Vanuatu</option>
                <option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                <option value="Venezuela">Venezuela</option>
                <option value="Viet Nam">Viet Nam</option>
                <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                <option value="Wales">Wales</option>
                <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                <option value="Western Sahara">Western Sahara</option>
                <option value="Yemen">Yemen</option>
                <option value="Zambia">Zambia</option>
                <option value="Zimbabwe">Zimbabwe</option>
            </select>
    </div>
    <div>
        <label for="HCP-NAME_G">Name of Billing Contact</label>
        <input type="text" name="HCP-NAME_G" style="width:485px" class="required" />
    </div>
    <div>
        <label for="HCP-INSTITUTION">Institution</label>
        <input type="text" name="HCP-INSTITUTION" style="width:485px" />
    </div>
    <div>
        <label for="HCP-EMAIL_G">Email Address</label>
        <input type="text" name="HCP-EMAIL_G" style="width:485px" class="required email" />
    </div>
    <div>
        <label for="HCP-ADDR_G">Street Address</label>
        <input type="text" name="HCP-ADDR_G" style="width:485px" class="required" />
    </div>
    <div>
        <label for="billing_office">Office / Suite / Etc</label>
        <textarea name="billing_office" style="width:485px" rows="3"></textarea>
    </div>
    <div>
        <label for="HCP-PHONE">Phone Number</label>
        <input type="text" name="HCP-PHONE" style="width:485px" />
    </div>
    <div>
        <label for="HCP-CITY_G">City</label>
        <input type="text" name="HCP-CITY_G" style="width:290px" class="required" />
    </div>
    <div class="state-US">
        <label for="shipping_state">State</label>
        <select name="billing_state" class="required">
            <option selected="selected"></option>
            <option value="AL">Alabama</option>
            <option value="AK">Alaska</option>
            <option value="AZ">Arizona</option>
            <option value="AR">Arkansas</option>
            <option value="CA">California</option>
            <option value="CO">Colorado</option>
            <option value="CT">Connecticut</option>
            <option value="DC">District Of Columbia</option>
            <option value="DE">Delaware</option>
            <option value="FL">Florida</option>
            <option value="GA">Georgia</option>
            <option value="HI">Hawaii</option>
            <option value="ID">Idaho</option>
            <option value="IL">Illinois</option>
            <option value="IN">Indiana</option>
            <option value="IA">Iowa</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
            <option value="LA">Louisiana</option>
            <option value="ME">Maine</option>
            <option value="MD">Maryland</option>
            <option value="MA">Massachusetts</option>
            <option value="MI">Michigan</option>
            <option value="MN">Minnesota</option>
            <option value="MS">Mississippi</option>
            <option value="MO">Missouri</option>
            <option value="MT">Montana</option>
            <option value="NE">Nebraska</option>
            <option value="NV">Nevada</option>
            <option value="NH">New Hampshire</option>
            <option value="NJ">New Jersey</option>
            <option value="NM">New Mexico</option>
            <option value="NY">New York</option>
            <option value="NC">North Carolina</option>
            <option value="ND">North Dakota</option>
            <option value="OH">Ohio</option>
            <option value="OK">Oklahoma</option>
            <option value="OR">Oregon</option>
            <option value="PA">Pennsylvania</option>
            <option value="RI">Rhode Island</option>
            <option value="SC">South Carolina</option>
            <option value="SD">South Dakota</option>
            <option value="TN">Tennessee</option>
            <option value="TX">Texas</option>
            <option value="UT">Utah</option>
            <option value="VT">Vermont</option>
            <option value="VA">Virginia</option>
            <option value="WA">Washington</option>
            <option value="WV">West Virginia</option>
            <option value="WI">Wisconsin</option>
            <option value="WY">Wyoming</option>
        </select>

    </div>
    <div class="state-int hidden">
        <label for="shipping_state">State / Province / Region</label>
        <input type="text" name="billing_state" style="width:150px" disabled="disabled" />
    </div>
    <div>
        <label for="HCP-ZIP_G">ZIP/Postal Code</label>
        <input type="text" name="HCP-ZIP_G" style="width:90px" class="required" />
    </div>

</div>

<ul>
    <li>Only credit card payments can be accepted at this time. Payments are securely processed by an eTransact storefront set up by Washington University in St Louis. The Human Connectome Project website does not store or track credit card information in its database. </li>
</ul>
<div class="hcpForm-wrapper" id="order-cost">
    <?php if (!$_GET['noValidation']) : ?>
        <!-- validates order -->
        <p align="right" style="margin-bottom:0;"><input type="button" class="highlight" value="Proceed to Checkout" onclick="validateMe()" /></p>
    <?php else : ?>
        <p align="right" style="margin-bottom:0;"><input type="submit" class="highlight" value="Proceed to Checkout" /></p>
        <input type="hidden" name="dut" value="true" />
    <?php endif; ?>
</div>

</form>

<!-- modal overlays -->
<div id="page-mask" class="pm" onclick="javascript:modalClose();"></div>

<div class="modal" id="coming-soon" style="min-height:0px !important;">
    <div class="modal-title">HCP Course Registration Form Closed <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
    <div class="modal_content">
        <h1><img src="/img/art/icon-HCP-data-drive-large.png" align="right" style="margin:0 0 0 30px" />HCP Course Registration Offline</h1>
        <p>Thank you for your interest in registering for HCP Courses. There are no course offerings currently available for registration. </p>
        <p>If you would like to receive an update when the storefront is ready for orders, please sign up to our <a href="/contact/#subscribe">HCP Announce mailing list</a>.</p>
    </div>
</div> <!-- end "coming soon" modal -->


<?php
mysqli_close($db);
?>
</div>
<!-- /#Content -->

<div id="sidebar-rt">

    <div id="sidebar-content">
        <div class="sidebox databox">
            <h2>HCP Course Details</h2>
            <p><img src="/img/art/hcp-course-2015-honolulu.jpg" alt="Exploring the Human Connectome" /></p>
            <p>The 2015 HCP Course is designed for investigators who are interested in:</p>
            <ul>
                <li>using data being collected and distributed by HCP</li>
                <li>acquiring and analyzing HCP-style imaging and behavioral data at your own institution</li>
                <li>processing your own non-HCP data using HCP pipelines and methods</li>
                <li>learning to use Connectome Workbench tools and the CIFTI connectivity data format</li>
                <li>learning HCP multi-modal neuroimaging analysis methods, including those that combine MEG and MRI data</li>
            </ul>
            <p>&nbsp;</p>
            <p>For more details, see the <a href="/course-registration/2015/exploring-the-human-connectome.php">HCP Course Website</a></p>
        </div>
        <!-- /sidebox -->
    </div>
</div>
<!-- /sidebar -->


</div> <!-- middle -->

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a> - <a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a></span><br />
<a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a> - <a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a><br />
<a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a> - <a href="http://duke.edu/" title="Duke University" target="_new">Duke University</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>.
</p>

<p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p>

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="http://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
