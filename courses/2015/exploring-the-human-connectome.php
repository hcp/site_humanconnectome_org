<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HCP Course: Exploring the Human Connectome (Summer 2015) | Human Connectome Project</title>
<meta http-equiv="Description" content="The Human Connectome Project released its first MEG dataset in 2014. Download the dataset and full documentation." />
<meta http-equiv="Keywords" content="User Guide, Documentation, MEG, Public Data Release, Human Connectome Project" />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans+Mono' rel='stylesheet' type='text/css'>
<link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
    <link href="/css/data.css" rel="stylesheet" type="text/css" />
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/js/global.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.2.js"></script>
</head>

<body>
<!-- site container -->
<div id="site_container">

<div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div>

<!-- Header -->
<div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
  <map name="Header" id="Header">
    <area shape="rect" coords="14,7,404,123" href="/" alt="Human Connectome Project Logo" />
  </map>
</div> 
<!-- end Header -->

<!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" -->
<div id="topnav">
  <ul id="nav">
    <li><a href="/">Home</a></li>
    <li><a href="/about/">About the Project</a>
      <ul>
        <li><a href="/about/project/">Project Overview &amp; Components</a></li>
        <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
        <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
        <li><a href="/about/teams.html">Operational Teams</a></li>
        <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
        <li><a href="/about/publications.html">Publications</a></li>
        <li><a href="/courses">HCP Course Materials</a></li>
        <li><a href="/about/pressroom/">News &amp; Updates</a></li>
      </ul>
    </li>
    <li><a href="/data/">Data</a>
      <ul>
        <li><a href="/data/">Get Access to Public Data Releases</a></li>
        <li><a href="/data/connectome-in-a-box.html">Order Connectome in a Box</a></li>
        <li><a href="/data/data-use-terms/">HCP Data Use Terms</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Home" target="_blank">HCP Wiki: Data Resources</a></li>
      </ul>
    </li>
    <li><a href="/software/">Software</a>
      <ul>
        <li><a href="/software/connectome-workbench.html">What is Connectome Workbench</a></li>
        <li><a href="/software/get-connectome-workbench.html">Get Connectome Workbench</a></li>
        <li><a href="/software/workbench-command.php">Using Workbench Command</a></li>
        <li><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></li>
        <li><a href="/documentation/HCP-pipelines/index.html">HCP MR Pipelines</a></li>
        <li><a href="/documentation/HCP-pipelines/meg-pipeline.html">HCP MEG Pipelines</a></li>
      </ul>
    </li>
    <li><a href="/documentation/">Documentation</a>
      <ul>
      	<li><a href="/documentation/">All Documentation</a></li>
        <li><a href="/documentation/citations.html">How to Cite HCP</a></li>
        <li><a href="/documentation/tutorials/">Software Tutorials</a></li>
        <li><a href="/documentation/S900/">900 Subjects Data Release</a></li>
        <li><a href="/documentation/HCP-pipelines/">HCP Pipelines</a></li>
          <li><a href="/documentation/subject-key">How to Cite HCP Subjects with a Subject Key</a></li>
      </ul>
    </li>
    <li><a href="/contact/">Contact</a>
      <ul class="subnav">
      	<li><a href="/contact/">General Project Inquiries</a></li>
        <li><a href="/contact/#subscribe">Subscribe to Mailing Lists</a></li>
        <li><a href="/careers/">Career Opportunities</a></li>
        <li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
        <li> <a href="/contact/feature-request.php">Feature Request</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Media+Images+from+the+Human+Connectome+Project+WU-Minn+Consortium" target="_blank">HCP Wiki: Media Images</a></li>
      </ul>
    </li>
    <li><a href="/resources/">Other Resources</a>
      <ul class="subnav">
        <li><a href="http://wiki.humanconnectome.org/" target="_blank">HCP Wiki</a></li>
        <li><a href="http://lifespan.humanconnectome.org/" target="_blank">HCP Lifespan Pilot Project</a></li>
        <li><a href="/documentation/MGH-diffusion/">MGH Adult Diffusion Project</a></li>        
        <li><a href="/resources/faq-disease-related-connectome-research.html">FAQ: Disease-Related Connectome Research</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="search">
    <!--   If Javascript is not enabled, nothing will display here -->
    <script>
      (function() {
        var cx = '000073750279216385221:vnupk6whx-c';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      })();
    </script>
    <gcse:search></gcse:search>	
</div> <!-- Search box -->
<link rel="stylesheet" href="/css/google-cse-customizations.css" />
<!-- #EndLibraryItem --><!-- End Nav -->

<!-- banner -->
<div id="bannerOpen" class="data-banner" style="background: url('/img/art/course-banner-HCP2015.jpg') left top no-repeat #fff !important; height: 260px">
</div>
<!-- Breadcrumbs -->
<div id="breadcrumbs">
  <p>&raquo; <a href="/courses/">hcp course materials</a> &gt; Exploring the Human Connectome (2015)</p></div>
<!-- end breadcrumbs -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">

    <div id="content-wide" class="editable">
        <style>
            h2 { border-top: 1px solid #ccc !important; margin-top: 1em; padding-top: 1em; }
            #content li a { color: #444; border-bottom: 1px dashed #ccc; }
            #content li a:hover { color: #0168db; text-decoration: none; }
            .download-button { background-color: #0030a0; border: 1px solid #111; box-shadow: 1px 2px #aaa; color: #fff; font-family: Arial, sans-serif; margin-bottom: 1em; padding: 6px 10px; text-align: center; }
            .download-button span { font-size: 11px; }
            .download-button:hover { background-color: #0168db; box-shadow: none; cursor: pointer; }
        </style>
        <h1>Exploring the Human Connectome  </h1>
        <h2>HCP Course Practical Data and Software</h2>
        <div style="display: inline-block; vertical-align: top; width: 600px; ">
            <p>The 2015 HCP Course Practical Data and Software are available in 2 formats: as a preconfigured Virtual Machine (VM) that includes all necessary software and as a data only option (that requires separate software configuration on your system).</p>
            <p>Both formats require user verification through <a href="https://db.humanconnectome.org" title="Open ConnectomeDB">ConnectomeDB</a> as they contain HCP data covered under the <a href="/data/data-use-terms">HCP Open Access Data Use Terms</a>. The download also requires the <a href="/documentation/connectomeDB/downloading/installing-aspera.html">Aspera Connect plugin</a>. Practical instructions have been written to work with the preconfigured VM.  </p>
            <p class="alertBox">
                <strong>You will need at least 1TB of free disk space</strong> for downloading/extracting the data. The Practical Data are very large (~275 GB) and are downloaded as a compressed archive. <br /><br />
                <strong>Expect downloads to take many hours to complete</strong>, even using the Aspera plugin.
            </p>
            <h3>Option 1: Download Course VM</h3>
            <p>The VM is a copy of the computer configuration used by attendees at the HCP Course and the instructions for each Practical can be used as written in this Linux VM environment. </p>
            <p><a href="javascript:showModal('download-course-vm')" style="font-weight: bold">Download (275 GB archive)</a> | <a href="https://wiki.humanconnectome.org/display/PublicData/How+To+Install+the+HCP+Course+VM+for+Windows%2C+Linux+or+Mac" target="_blank" style="font-weight: bold;">Course VM Installation Instructions for Windows / Mac / Linux</a></p>
            <h3>Option 2: Download Course Practical Data Only</h3>
            <p>For advanced users that want to use the practical data on their own systems. Using this package will require one to install and configure the necessary software for running the practicals. Some parts of the software with the course data have only been tested for Linux, therefore a Linux OS is recommended for users that choose this option.</p>
            <p><a href="javascript:showModal('download-practicals')" style="font-weight: bold">Download (215 GB archive)</a> | <a href="javascript:showModal('prerequisite-software')" style="font-weight: bold;">Prerequisite software for running the 2015 HCP Course Practicals</a></p>
        </div>
        <div style="display: inline-block; margin-left: 20px; border-left: 1px solid #ccc; padding-left: 20px; vertical-align: top; width: 300px;">
            <div class="download-button" onclick="javascript:showModal('download-course-vm');"><strong>Download Course VM</strong><br /><span>275 GB compressed archive</span></div>
            <p>Recommended for most users. <a href="https://wiki.humanconnectome.org/display/PublicData/How+To+Install+the+HCP+Course+VM+for+Windows%2C+Linux+or+Mac" target="_blank">Installation Instructions</a></p>
            <div class="download-button" onclick="javascript:showModal('download-practicals');"><strong>Download Practical Data Only</strong><br /><span>215 GB compressed archive</span></div>
            <p>For advanced users that want to use the practical data on their own systems. <a href="javascript:showModal('prerequisite-software')">Prerequisite Software</a></p>
        </div>

        <a name="timetable"></a>        <a name="schedule"></a>
        <div>
            <h2>Course Schedule and Resources</h2>
            <p><strong>Note:</strong> You can download all HCP Course materials en masse from <strong><a href="https://wustl.box.com/s/g7qnpjml4uy32ttqa4bzzikedmqlu5k4" target="_blank" title="Download HCP course materials from wustl.box.com">wustl.box.com</a></strong></p>

            <p><strong>Day 1<span class="time"> (Monday, 8 June)</span>: Structural imaging; preprocessing; intersubject registration; parcellation</strong></p>
            <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                <thead>
                <tr>
                    <th width="80">Type <span class="time">(Time)</span></th><th>Course</th><th width="420">Resources</th>
                </tr>
                </thead>
                <tbody>
                <tr class="hcp-schedule-lecture">
                    <td rowspan="2" valign="top">Lecture 1 <span class="time">(8:00 - 9:30)</span></td><td>Intro & overview (Van Essen)</td>
                    <td>PDF: <a href="https://wustl.box.com/shared/static/6r1fgryyd6tiqpiwyssy47i1pjt75brp.pdf" title="Download Lecture Notes">Day1_Lecture1a_HCP_Course_Intro_VanEssen.pdf</a></td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>HCP Acquisition Basics  (Harms/Xu)</td>
                    <td>PDF: <a href="https://wustl.box.com/shared/static/zz6p8esjus49n0ej8w9ave452jaeolj7.pdf" title="Download Lecture Notes">Day1_Lecture1b_HCP_Acquisition_Basics_Harms.pdf</a></td>
                </tr>
                <tr class="hcp-schedule-practical">
                    <td>Practical 1 <span class="time">(9:30 - 10:30)</span></td><td>Intro to wb_view &amp; Imaging File Types</td>
                    <td>PDF: <a href="https://wustl.box.com/shared/static/om7upe6pe7nrtyvynjdpoza45i3cgc8d.pdf" title="Download Practical Materials">Day1_Practical1_wb_view&FileTypes.pdf</a></td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 2  <span class="time">(11:00 - 12:00)</span></td><td>Structural MRI: Precise Neuroanatomical Localization through Careful Processing (Glasser)</td>
                    <td>PDF: <a href="https://wustl.box.com/shared/static/c09seazn7ywkgfrhlmx74ym4122f4bpk.pdf" title="Download PDF">Day1_Lecture2_Structural_MRI_Glasser.pdf</a> <br />
                        PPT: <a href="https://wustl.box.com/shared/static/mihkeyog2xja09a95qlb63oa0l88ht7j.pptx" title="Download PPT">Day1_Lecture2_Structural_MRI_Glasser.pptx</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-practical">
                    <td>Practical 2 <span class="time">(1:30 - 3:30)</span></td><td>wb_command &amp; HCP pipelines I</td><td>
                        PDF: <a href="https://wustl.box.com/shared/static/sq05vq2xtg36oa1gap5iqjq476vkxftg.pdf" title="Download PDF">Day1_Practical2_wb_command&PipelinesI.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 3 <span class="time">(3:30 - 4:30)</span></td><td>Brain Parcellation (Glasser)</td><td>
                        PDF: <a href="https://wustl.box.com/shared/static/edzbzghkbltq2m901smfisaaw8xua9az.pdf" title="Download PDF">Day1_Lecture3_Brain_Parcellation_Glasser.pdf</a> <br />
                        PPT: <a href="https://wustl.box.com/shared/static/sq3wrp5gri9xmcwxgcrh6awiuz8ffiui.pptx" title="Download PPT">Day1_Lecture3_Brain_Parcellation_Glasser.pptx</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 4 <span class="time">(4:30 - 5:30)</span></td><td>rfMRI background, preprocessing, denoising (Smith)</td><td>
                        PDF: <a href="https://wustl.box.com/shared/static/ecw0e6f428s8x0p7ba5do6vhe6y3n5xz.pdf" title="Download PDF">Day1_Lecture4_rfMRI_Preprocessing_Denoising_Smith</a>
                    </td>
                </tr>
                </tbody>
            </table>
            <p><strong>Day 2<span class="time"> (Tuesday, 9 June)</span>: Resting-state fMRI; network analysis</strong></p>
            <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                <thead>
                <tr>
                    <th width="80">Type <span class="time">(Time)</span></th><th>Course</th><th width="420">Resources</th>
                </tr>
                </thead>
                <tbody>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 5 <span class="time">(8:00 - 9:00)</span></td><td>Resting State Analysis II (Burgess)</td><td>
                        PDF: <a href="https://wustl.box.com/shared/static/wre2i9w8l18uu4hi9nohtm3gz8u3atnt.pdf" title="Download PDF">Day2_Lecture5_rfMRI_Analysis_II_Burgess.pdf</a><br />
                        PPT: <a href="https://wustl.box.com/shared/static/ln6315dzoqyiqencmgpv5v3ligmw36pb.pptx" title="Download PPT">Day2_Lecture5_rfMRI_Analysis_II_Burgess.pptx</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-practical">
                    <td>Practical 3 <span class="time">(9:00 - 10:30)</span></td><td>rfMRI Preprocessing, Denoising; ICA-based parcellations</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/grbg2lp50kdz6e04aev7hzf7ep29u6aa.pdf" title="Download PDF">Day2_Practical3_rfMRI_processing&parcellation.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 6 <span class="time">(11:00 - 12:00)</span></td><td>rfMRI Network Analysis Strategies (Smith)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/1xrihyhphdzasfxlivzp9abg3cavwck3.pdf" title="Download PDF">Day2_Lecture6_rfMRI_Network_Analysis_Strategies_Smith.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-practical">
                    <td>Practical 4 <span class="time">(1:30 - 3:30)</span></td><td>rfMRI Netmats & Dual Regression </td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/vvlcvepwh60k7ymy4g6r2sabjk1vz9gv.pdf" title="Download PDF">Day2_Practical4_rfMRI_Netmats_Dual_Regression.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 7 <span class="time">(3:30 - 4:30)</span></td><td>Multimodal Classification of Areas in Individuals and Parcellation Validation (Glasser)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/cfat83qm1o92qz3dthk0dbmyyyf90dwi.pdf" title="Download PDF">Day2_Lecture7_Multi-modal_Classification_Glasser.pdf</a><br />
                        PPT: <a href="https://wustl.box.com/shared/static/lrt03qof9bdodabnyc4745cz5tdxhwb1.pptx" title="Download PPT">Day2_Lecture7_Multi-modal_Classification_Glasser.pptx</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 8 <span class="time">(4:30 - 5:30)</span></td><td>Monitoring and Evaluating HCP Data Quality (Harms)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/7beoj4ibtqcdubnfoiutm76m969gklfo.pdf" title="Download PDF">Day2_Lecture8_HCP_QC_Harms.pdf</a>
                    </td>
                </tr>
                </tbody>
            </table>
            <p><strong>Day 3<span class="time"> (Wednesday, 10 June)</span>: Diffusion imaging and tractography; 7T acquisition and analysis</strong></p>

            <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                <thead>
                <tr>
                    <th width="80">Type <span class="time">(Time)</span></th><th>Course</th><th width="420">Resources</th>
                </tr>
                </thead>
                <tbody>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 9 <span class="time">(8:00 - 9:00)</span></td><td>Diffusion MRI, Distortion Correction & DTI (Andersson)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/2trqokm9mjozq2b41zqxwtbij1fuvro0.pdf" title="Download PDF">Day3_Lecture9_Diffusion_Andersson.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-practical">
                    <td>Practical 5 <span class="time">(9:00 - 10:30)</span> </td><td>Diffusion MRI, Distortion Correction & DTI</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/st19xluzmd4jat5ho4cf1oadiekehmnw.pdf" title="Download PDF">Day3_Practical5_dMRI&DTI.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 10 <span class="time">(11:00 - 12:00)</span></td><td>Fibre Orientation Models & Tractography Analysis (Bastiani)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/cpseug4thxhfiy1bq3pk7pkljb9x007v.pdf" title="Download PDF">Day3_Lecture_10_Tractography_Bastiani.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-practical">
                    <td>Practical 6 <span class="time">(1:30 - 3:30)</span></td><td>Fibre Orientation Models & Tractography</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/n8pw64dba2tbiridhuzefxrhdmsabdr8.pdf" title="Download PDF">Day3_Practical6_Tractography.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 11 <span class="time">(3:30 - 4:30)</span></td><td>Regression, Prediction & Permutation Analyses (Nichols)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/9q3dccp4qy082zpzppg7ko748gdd7lpl.pdf" title="Download PDF">Day3_Lecture_11_Modelling_Inference_Nichols.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 12 <span class="time">(4:30 - 5:30)</span></td><td>Family structure & Heritability (Nichols)</td>
                    <td>PDF: <a href="https://wustl.box.com/shared/static/i9mldg10ofkdz32hj7kz2fdrv76ecggp.pdf" title="Download PDF">Day3_Lecture_12_Family_Structure_Heritability_Nichols.pdf</a> </td>
                </tr>
                </tbody>
            </table>

            <p><strong>Day 4<span class="time"> (Thursday, 11 June)</span>: Task-fMRI; data mining; disease connectomics</strong> </p>

            <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                <thead>
                <tr>
                    <th width="80">Type <span class="time">(Time)</span></th><th>Course</th><th width="420">Resources</th>
                </tr>
                </thead>
                <tbody>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 13 <span class="time">(8:00 - 9:00)</span></td><td>Task fMRI and Behavior Measures in HCP (Burgess/Harms)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/rog9y06kjyd1orlaqnemkxk2wds6nweh.pdf" title="Download PDF">Day4_Lecture_13_tfMRI_Burgess.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-practical">
                    <td>Practical 7 <span class="time">(9:00 - 10:30)</span></td><td>Task fMRI Analyses</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/0vcsqc0yg3ncqmirryyxpgsiqhxww659.pdf" title="Download PDF">Day4_Practical7_Task_fMRI.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 14 <span class="time">(11:00 - 12:00)</span></td><td>HCP Informatics: ConnectomeDB and Cloud-based Processing (Marcus)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/ggpmysqxjmvp6le17hoz3fobn7kcblwc.pdf" title="Download PDF">Day4_Lecture_14_HCP_Informatics_Marcus.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-practical">
                    <td>Practical 8 <span class="time">(1:30 - 3:30)</span></td><td>Cloud-based Processing using HCP Pipelines & Amazon Web Services</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/ejctwnmeepjkf52kcx32jut9tu5h7o6t.pdf" title="Download PDF">Day4_Practical8_Cloud-Based_Processing.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 15 <span class="time">(3:30 - 4:30)</span></td><td>Understanding the Clinical Connectome: Application of HCP Methods to Neuropsychiatric Illness   (Anticevic)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/p7jl7fzy4aeclvbq9ria1raynr5e2d1l.pdf" title="Download PDF">Day4_Lecture_15_Clinical_Connectomics_Anticevic.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 16 <span class="time">(4:30 - 5:30)</span></td><td>Using  7T Multimodal Data (Xu)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/8kwykejyfwfos2lsrwuz8gsodjpwt1sv.pdf" title="Download PDF">Day4_Lecture_16_Using_7T_Xu.pdf</a>
                    </td>
                </tr>
                </tbody>
            </table>

            <p><strong>Day 5 (Friday, 12 June): Magnetoencephalography; multimodal integration</strong> </p>

            <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                <thead>
                <tr>
                    <th width="80">Type <span class="time">(Time)</span></th><th>Course</th><th width="420">Resources</th>
                </tr>
                </thead>
                <tbody>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 17 <span class="time">(8:00 - 9:00)</span></td><td>Introduction to HCP-MEG Connectome (Larson-Prior)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/mr43lm4ebnw6exl8spn9mwn9o5qpzdeb.pdf" title="Download PDF">Day5_Lecture_17_Intro_HCP_MEG_Larson-Prior.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-practical">
                    <td>Practical 9 <span class="time">(9:00 - 10:30)</span></td><td>MEG preprocessing, channel & source analysis of tMEG and rMEG </td>
                    <td>PDF: <a href="https://wustl.box.com/shared/static/q1e1stg3aruu4aasuc6fcv489l65nwbu.pdf" title="Download PDF">Day5_Practical9_MEG_I</a></td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 18 <span class="time">(11:00 - 12:00)</span></td><td>Assessment of connectivity with MEG (Schoffelen)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/bp312dde3aadcqmpnutkqqj0aqt6an0s.pdf" title="Download PDF">Day5_Lecture_18_MEG_Connectivity_Schoffelen</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-practical">
                    <td>Practical 10 <span class="time">(1:30 - 3:30)</span></td><td>rMEG and tMEG source-level connectivity; Multimodal Integration </td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/dhz3scmb4xv4utlve88fmf1wz5dp4q2i.pdf" title="Download PDF">Day5_Practical_10_MEG_II.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 19 <span class="time">(3:30 - 4:30)</span></td><td>Follow-up analyses for HCP MEG data and other software tools (Oostenveld)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/nnmfsq41n93oyacfvso86ngva259n64o.pdf" title="Download PDF">Day5_Lecture_19_Followup_MEG_Analyses_Oostenveld.pdf</a>
                    </td>
                </tr>
                <tr class="hcp-schedule-lecture">
                    <td>Lecture 20 <span class="time">(4:30 - 5:30)</span></td><td>HCP: Looking back—and forward (Van Essen)</td>
                    <td>
                        PDF: <a href="https://wustl.box.com/shared/static/4rhn1658g85txorrflwbyids6wqgvq4k.pdf" title="Download PDF">Day5_Lecture_20_HCP_Course_Wrapup_VanEssen.pdf</a>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <hr size="1" color="#ccc" style="margin: 1em 0" />
        <div style="float:right; width: 160px; margin-left: 30px; margin-bottom: 30px; padding: 15px; border-left: 1px dotted #ccc; font-size:10px;">
            <p><strong>On This Page:</strong></p>
            <ul>
                <li><a href="#registration">Registration</a></li>
                <li><a href="#invitation">Invitation Letter</a></li>
                <li><a href="#accommodations">Accommodations and Location</a></li>
                <li><a href="#faculty">Faculty</a></li>
                <!-- <li><a href="#timetable">Timetable</a></li> -->
                <!-- <li><a href="#schedule">Schedule (Preliminary)</a></li> -->
                <li><a href="#cancellation">Cancellation Policy (Updated May 18, 2015)</a></li>
                <li><a href="#contact">Contact Information</a></li>
            </ul>
        </div>
        <div>
            <p><span style="font-size:16px; line-height:1.2;"><strong>Learn about multi-modal neuroimaging data, analysis, and visualization tools of the Human Connectome Project</strong></span> </p>
            <p>We are pleased to announce the <strong>2015 HCP Course: &quot;Exploring the Human Connectome&quot;</strong>,  to be held <strong>June 8-12</strong> at the <a href="http://www.marriott.com/hotels/travel/hnlmc/?mktcmp=w_regionsite_hnlmc_x" target="_blank"><strong>Marriott Resort Waikiki Beach</strong></a>, in  Honolulu, Hawaii, USA.</p>
            <p>This course is designed for investigators who are interested  in:</p>
            <ul>
                <li>using data  being collected and distributed by HCP</li>
                <li>acquiring  and analyzing HCP-style imaging and behavioral data at your own institution</li>
                <li>processing your  own non-HCP data using HCP pipelines and methods</li>
                <li>learning to  use Connectome Workbench tools and the CIFTI connectivity data format </li>
                <li>learning HCP  multi-modal neuroimaging analysis methods, including those that combine MEG and  MRI data </li>
                <li>positioning yourself to capitalize on HCP-style data from forthcoming large-scale projects (e.g., Lifespan HCP and Connectomes Related to Human Disease)</li>
            </ul>
            <p>This 5-day intensive course will provide training in the  acquisition, analysis and visualization of data from the Human Connectome  Project using methods and informatics tools developed by the WU-Minn HCP  consortium plus data made freely available to the neuroscience community.  Participants will learn how to acquire, analyze, visualize, and interpret data  from four major MR modalities (structural MR, resting-state fMRI, diffusion  imaging, task-evoked fMRI) plus magnetoencephalography (MEG) and extensive  behavioral data.  Lectures and labs will  provide grounding in neurobiological as well as methodological issues involved  in interpreting multimodal data, and will span the range from  single-voxel/vertex to brain network analysis approaches.  </p>
            <p>The course is open to graduate students, postdocs, faculty,  and industry participants.  The course is  aimed at both new and current users of HCP data, methods, and tools, and will  cover both basic and advanced topics. Prior experience in human neuroimaging or  in computational analysis of brain networks is desirable, preferably including  familiarity with FSL and Freesurfer software.  </p>
            <p>      All lectures and printed material will be in English.</p>
            <p>      The <a href="http://fsl.fmrib.ox.ac.uk/fslcourse/hawaii2015.html" target="_blank">2015 FSL Course</a> will  also be run in the same venue on the same days. Because the two courses are  totally separate and self-contained, it will not be logistically possible for  attendees to attend sessions from both courses. </p>
            <p>The HCP and FSL courses run the week before <a href="http://ohbm.loni.usc.edu/" target="_blank">OHBM 2015</a> (June 14-18 at the Hawaii  Convention Center in Honolulu).
            </p>
        </div>
        <a name="registration"></a>
        <div>
            <h2>Registration</h2>
            <p> Registration (in US dollars) is $800 for PhD/MSc students,  $1300 for postdocs and other non-student attendees, and $2300 for commercial  attendees. Course registration includes lectures, computer practical sessions,  coffee/tea breaks, an evening reception on Monday June 8, and a flash drive containing  the lecture slides. All course materials (lecture slide PDFs, practical  instructions and practical datasets) will be available online after the course.</p>
            <p>  The registration fee does not include accommodation (see below),  or meals; there are plenty of local shops, cafés and restaurants nearby.</p>
            <p>  To register and pay, visit the <a href="/course-registration/">on-line registration page</a>. We  are only able to take payment by credit card. Please note that your  registration is not complete until you have finalized the payment (entering  form information and then proceed to checkout and payment).</p>
        </div>
        <a name="invitation"></a>
        <div>
            <p>If you require an invitation letter as part of a travel visa application, please download <a href="HCP_Course_2015_Invitation_Letter_v2.pdf" title="Invitation Letter (PDF)">this letter</a> and fill in your contact information.</p>
        </div>
        <div>
            <a name="accommodations"></a>
            <h2>Accommodation &amp; Location  Information</h2>
            <p>  The course will take place at the <a href="http://www.marriott.com/hotels/travel/hnlmc/?mktcmp=w_regionsite_hnlmc_x">Waikiki  Beach Marriott Resort and Spa</a>, Hawaii, USA.<br />
                You will need to organize your own accommodation for the  course. A limited number of discounted rooms are available at the Marriott at <a href="https://resweb.passkey.com/Resweb.do?mode=welcome_ei_new&eventID=13777135" title="Register at the Waikiki Marriott via resweb.passkey.com">this website</a>. There are also many <a href="http://www.tripadvisor.com/Hotels-g60982-Honolulu_Oahu_Hawaii-Hotels.html">local  hotels in Waikiki Beach</a>.</p>
            <p>  If you would like to share accommodation with another  attendee (of either the HCP or FSL Courses), you may enter your contact  information on <a href="http://documents.fmrib.ox.ac.uk/documentsWiki/Public/FSLCourseRoomSharing">our  publicly-viewable wiki page</a>.  </p>
        </div>
        <a name="faculty"></a>
        <div>
            <h2>Faculty</h2>
            <p><strong>Speakers:</strong> David Van Essen, Steve Smith, Jesper Andersson, Dan Marcus, Matt Glasser, Michael Harms, Greg Burgess, Tom Nichols, Matteo Bastiani, Alan Anticevic, Gordon Xu, Robert Oostenveld, Jan-Mathijs Schoffelen, Linda Larson-Prior</p>
            <p><strong>Tutors:</strong> (in addition to speakers): Jenn Elam, Tim Brown, Tim Coalson, Giorgos Michalareas, Francesco Di Pompeo</p>
        </div>

        <a name="cancellation"></a>
            <h2>Cancellation Policy (Updated May 18, 2015)</h2>
            <p>Please book with care. We have made commitments on costs that cannot be changed this close to the course date, and can no longer offer full refunds on registration cancellations. We cannot offer refunds of any kind for any cancellation within two weeks of the event (from May 25th on). We can offer 50% refund on cancellations between now and then.</p>
        <div>

        </div>
        <a name="contact"></a>
        <div>
            <h2>Contact Information</h2>
            <p>If you have any questions, please contact us at: <a href="mailto:hcpcourse@humanconnectome.org">hcpcourse@humanconnectome.org</a></p>
            <p>We look forward to seeing you in <em>Hawai'i</em>! </p>
        </div>

    </div>

  <!-- Primary page Content -->

<div id="sidebar-rt" class="hidden">
    
        <div id="sidebar-content" class="editable">
          <div class="sidebox">
          	<h2 align="center">How to Register</h2>

                	<p><strong>Registration Closed</strong></p>
                    <p>Apologies, but course registration is no longer available. <a href="mailto:hcpcourse@humanconnectome.org">Contact us</a> to learn about future course offerings. </p>
            
          </div><!-- #BeginLibraryItem "/Library/course-hcp2015-accommodations.lbi" --><div class="sidebox">
            <h2 align="center">Location and Accommodations</h2>
            <p><img src="/img/blog features/mariott-waikiki-beach.jpg" style="width:100%" /></p>
            <p><strong>Course Location: </strong></p>
            <p><a href="http://www.marriott.com/hotels/travel/hnlmc/?mktcmp=w_regionsite_hnlmc_x" target="_blank">Waikiki Beach Marriott Resort &amp; Spa</a><br />
            </p>
2552 Kalakaua Avenue, Oahu
            <br />
            Honolulu, Hawaii 96815
            
            <p>&nbsp;</p>
            <p><strong>Accommodations:</strong></p>
            <p>Accommodations are not included with course registration. However, a limited number of discounted rooms are available at the Marriott via <a href="https://resweb.passkey.com/Resweb.do?mode=welcome_ei_new&eventID=13777135" title="Register at the Waikiki Marriott via resweb.passkey.com">this website</a>.</p>
            <p>&nbsp;</p>
            <p><strong>Other Options:</strong></p>
            <p><a href="http://www.tripadvisor.com/Hotels-g60982-Honolulu_Oahu_Hawaii-Hotels.html" target="_blank">Tripadvisor.com: local hotels in Waikiki Beach</a></p>
            <p>&nbsp;</p>
            <p>If you would like to share accommodation with another attendee (of either the HCP or FSL Courses), you may enter your contact information on our <a href="http://documents.fmrib.ox.ac.uk/documentsWiki/Public/FSLCourseRoomSharing" target="_blank">publicly-viewable wiki page</a>.</p>
            <p>&nbsp;</p>
                <p><strong><a href="2015%20HCP%20Course%20Flyer%2011x17.pdf">Download Course Flyer (PDF)</a></strong></p>
            </div>

            <div class="sidebox">
                <h2 align="center">Invitation Letter</h2>
                <p>Do you need an invitation letter to complete a visa application? <a href="HCP_Course_2015_Invitation_Letter_v2.pdf"><strong>Download this PDF</strong></a> and fill in your contact information.</p>
            </div>
            <!-- #EndLibraryItem --></div>

        <!-- editable sidebar content region -->
    </div> <!-- sidebar -->

    <!-- modals -->
    <div id="page-mask" class="pm" onclick="javascript:modalClose();"></div>
    <div class="modal" id="download-course-vm" style="min-height:0px !important;">
        <div class="modal-title">Download Course VM <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
        <div class="modal_content">
            <h1>Before you download the Course VM...</h1>
            <p><strong>Make sure you have at least 1TB of free disk space.</strong> The downloaded archive must be opened within VM virtualizer software software (VMware Player, VMware Fusion, or VirtualBox) where you will run the VM.  If your internal storage has more than 1TB of space free, this will generally be acceptably fast for the purpose. </p>
            <p><strong>If you are planning to buy a new external drive for this purpose</strong>, we recommend that you check the computer(s) you intend to use it with for USB 3, eSATA, or Thunderbolt ports, and get an external drive with whichever of those interfaces your computer(s) have. USB 2 will work, but at significantly reduced performance speed.</p>
            <p><strong>This download requires <a href="/documentation/connectomeDB/downloading/installing-aspera.html" title="Aspera installation instructions">the Aspera plugin</a></strong> and a free ConnectomeDB account. We recommend using Firefox to download as Aspera is not well supported in Chrome.</p>
            <p><strong>Remember to change the settings in the Aspera Connect plugin</strong> (via the “gear” icon in the Transfers window or through Aspera Connect Menu>Preferences>Transfers) to download to your desired location. The default in Aspera Connect is to download to your desktop (which you probably don’t want). </p>
            <p><button onclick="javascript:modalClose()">Cancel</button> <button title="begin download" onclick="javascript:window.location.assign('https://db.humanconnectome.org/app/action/ChooseDownloadResources?project=HCP_Resources&resource=CourseData&filePath=HCP_2015_Course_VM.zip')">Begin My Download</button></p>
        </div>
    </div> <!-- end "download course VM" modal -->

    <div class="modal" id="download-practicals" style="min-height:0px !important;">
        <div class="modal-title">Download Practical Data Only <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
        <div class="modal_content">
            <h1>Before you download the Practical Data&hellip;</h1>
            <p><strong>Make sure you have at least 1TB of free disk space to download and extract the data.</strong> </p>
            <p><strong>If you are planning to buy a new external drive for this purpose</strong>, we recommend that use one compatible with USB 3, eSATA, or Thunderbolt connections. USB 2 will work, but at significantly reduced performance speed.</p>
            <p><strong>This download requires <a href="/documentation/connectomeDB/downloading/installing-aspera.html" title="Aspera installation instructions">the Aspera plugin</a></strong> and a free ConnectomeDB account. </p>
            <p><strong>Remember to change the settings in the Aspera Connect plugin</strong> (via the “gear” icon in the Transfers window or through Aspera Connect Menu>Preferences>Transfers) to download to your desired location. </p>
            <p class="alertBox">The paths provided throughout the HCP Course practical instructions, both for locating the software to run and for locating the data to process, will very likely not be correct for your particular configuration of software and data. You will be responsible for keeping track of these paths (where you installed the software and where you placed the data) and altering the commands provided in the course materials to reflect your chosen configuration. </p>
            <p>Additionally, the MEG practicals for Day 5 use precompiled MATLAB scripts that will only work on Linux systems. If you don’t have a Linux machine and want to run the MEG practicals, we recommend downloading the Course VM as in Option 1.</p>
            <p><button onclick="javascript:modalClose()">Cancel</button> <button title="begin download" onclick="javascript:window.location.assign('https://db.humanconnectome.org/app/action/ChooseDownloadResources?project=HCP_Resources&resource=CourseData&filePath=HCP_2015_Course_Data.zip')">Begin My Download</button></p>
        </div>
    </div> <!-- end "download course VM" modal -->

    <div class="modal" id="prerequisite-software" style="min-height:0px !important;">
        <div class="modal-title">Software Prerequisites for Practical Data <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
        <div class="modal_content">
            <h1>Prerequisite software for running the 2015 HCP Course Practicals</h1>
            <ul>
                <li><a href="http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation" target="_blank" title="Download FSL">FSL version 5.0.6</a></li>
                <li><a href="/software/get-connectome-workbench.html" target="_blank" title="Get Connectome Workbench">Connectome Workbench v1.1.1</a></li>
                <li><a href="/documentation/HCP-pipelines/meg-pipeline.html" target="_blank" title="Get MEG Connectome Pipelines">megconnectome v2.2 and fieldtrip-r9924</a></li>
                <li><a href="http://www.mathworks.com/products/matlab/" target="_blank" title="Download MATLAB">MATLAB</a> with the official MATLAB toolboxes: Statistics, Bioinformatics, and Signal Processing OR <a href="http://www.gnu.org/software/octave/download.html" target="_blank" title="Download Octave">Octave v.3.8.0</a> or greater with toolboxes: control, general, signal, and statistics</li>
                <li><a href="https://github.com/Washington-University/Pipelines/wiki/v3.4.0-Release-Notes,-Installation,-and-Usage#installation" target="_blank" title="Download HCP Pipeline Scripts">HCP Pipeline Scripts v3.4.0</a></li>
            </ul>
            <p>Requisite for running the HCP Pipelines: <a href="ftp://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/5.3.0-HCP" target="_blank" title="Download Freesurfer">FreeSurfer version 5.3.0-HCP</a>.</p>
            <p>See this <a href="https://github.com/Washington-University/Pipelines/wiki/v3.4.0-Release-Notes,-Installation,-and-Usage#prerequisites" target="_blank" title="Installation Instructions">installation guidance</a> for using the prerequisite software in the HCP pipelines.</p>
            <p><button onclick="javascript:modalClose()">Close</button></p>
        </div>
    </div> <!-- end "download course VM" modal -->


</div> <!-- middle -->

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a> - <a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a></span><br />
<a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a> - <a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a><br />
<a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a> - <a href="http://duke.edu/" title="Duke University" target="_new">Duke University</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>. 
</p>

<p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p> 

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script> 

<!-- google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18630139-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
