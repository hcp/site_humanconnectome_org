<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Confirm HCP Data Order | Human Connectome Project</title>
    <meta http-equiv="Description" content="Order confirmation page." />
    <link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
    <link href="/css/sortable.css" rel="stylesheet" type="text/css" />
    <link href="/css/data.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script src="//d79i1fxsrar4t.cloudfront.net/jquery.liveaddress/2.4/jquery.liveaddress.min.js"></script>
    <script>jQuery.LiveAddress("4149195827605618025");</script>
    <script type="text/javascript" src="/js/jquery.easing.1.2.js"></script>
    <script type="text/javascript" src="/js/global.js"></script>
    <Script type="text/javascript" src="/js/data-access.js"></script>

</head>

<body>


<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <!-- Header -->
    <div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
        <map name="Header" id="Header">
            <area shape="rect" coords="14,7,400,123" href="/" alt="Human Connectome Project Logo" />
        </map>
    </div>
    <!-- end Header -->

    <!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" -->
    <div id="topnav">
        <ul id="nav">
            <li><a href="/">Home</a></li>
            <li><a href="/about/">About the Project</a>
                <ul>
                    <li><a href="/about/project/">Project Overview &amp; Components</a></li>
                    <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
                    <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
                    <li><a href="/about/teams.html">Operational Teams</a></li>
                    <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
                    <li><a href="/about/publications.html">Publications</a></li>
                    <li><a href="/courses">HCP Course Materials</a></li>
                    <li><a href="/about/pressroom/">News &amp; Updates</a></li>
                </ul>
            </li>
            <li><a href="/data/">Data</a>
                <ul>
                    <li><a href="/data/">Get Access to Public Data Releases</a></li>
                    <li><a href="/data/connectome-in-a-box.html">Order Connectome in a Box</a></li>
                    <li><a href="/data/data-use-terms/">HCP Data Use Terms</a></li>
                    <li><a href="https://wiki.humanconnectome.org/display/PublicData/Home" target="_blank">HCP Wiki: Data Resources</a></li>
                </ul>
            </li>
            <li><a href="/software/">Software</a>
                <ul>
                    <li><a href="/software/connectome-workbench.html">What is Connectome Workbench</a></li>
                    <li><a href="/software/get-connectome-workbench.html">Get Connectome Workbench</a></li>
                    <li><a href="/software/workbench-command.php">Using Workbench Command</a></li>
                    <li><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></li>
                    <li><a href="/documentation/HCP-pipelines/index.html">HCP MR Pipelines</a></li>
                    <li><a href="/documentation/HCP-pipelines/meg-pipeline.html">HCP MEG Pipelines</a></li>
                </ul>
            </li>
            <li><a href="/documentation/">Documentation</a>
                <ul>
                    <li><a href="/documentation/">All Documentation</a></li>
                    <li><a href="/documentation/citations.html">How to Cite HCP</a></li>
                    <li><a href="/documentation/tutorials/">Software Tutorials</a></li>
                    <li><a href="/documentation/S900/">900 Subjects Data Release</a></li>
                    <li><a href="/documentation/HCP-pipelines/">HCP Pipelines</a></li>
                    <li><a href="/documentation/subject-key">How to Cite HCP Subjects with a Subject Key</a></li>
                </ul>
            </li>
            <li><a href="/contact/">Contact</a>
                <ul class="subnav">
                    <li><a href="/contact/">General Project Inquiries</a></li>
                    <li><a href="/contact/#subscribe">Subscribe to Mailing Lists</a></li>
                    <li><a href="/careers/">Career Opportunities</a></li>
                    <li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
                    <li> <a href="/contact/feature-request.php">Feature Request</a></li>
                    <li><a href="https://wiki.humanconnectome.org/display/PublicData/Media+Images+from+the+Human+Connectome+Project+WU-Minn+Consortium" target="_blank">HCP Wiki: Media Images</a></li>
                </ul>
            </li>
            <li><a href="/resources/">Other Resources</a>
                <ul class="subnav">
                    <li><a href="http://wiki.humanconnectome.org/" target="_blank">HCP Wiki</a></li>
                    <li><a href="http://lifespan.humanconnectome.org/" target="_blank">HCP Lifespan Pilot Project</a></li>
                    <li><a href="/documentation/MGH-diffusion/">MGH Adult Diffusion Project</a></li>
                    <li><a href="/resources/faq-disease-related-connectome-research.html">FAQ: Disease-Related Connectome Research</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div id="search">
        <!--   If Javascript is not enabled, nothing will display here -->
        <script>
            (function() {
                var cx = '000073750279216385221:vnupk6whx-c';
                var gcse = document.createElement('script');
                gcse.type = 'text/javascript';
                gcse.async = true;
                gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                '//www.google.com/cse/cse.js?cx=' + cx;
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(gcse, s);
            })();
        </script>
        <gcse:search></gcse:search>
    </div> <!-- Search box -->
    <link rel="stylesheet" href="/css/google-cse-customizations.css" />
    <!-- #EndLibraryItem --><!-- End Nav -->

    <script type="text/javascript">
        /* confirm acceptance of DUT on page open. */
        $(document).ready(function(){
            showModal('#dut-Phase2OpenAccess');
        });


    </script>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; data</p>
    </div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">
            <noscript>
                <p style="color:#c00; clear:both;"><strong>Alert:</strong> You appear to have JavaScript turned off. Registration and access for ConnectomeDB requires JavaScript. Please enable it in your browser before proceeding.</p>
            </noscript>

            <!-- check to make sure Data Use Terms were accepted -->
            <?php if ($_POST['dut']) : ?>

            <h2>Connectome In A Box: Confirm Order</h2>
            <p>Please review the following order information. If any of this is incorrect, please <a href="javascript:window.history.back();">go back to the order form</a> to correct it.</p>
            <?php
            /* base 36 converter - used to create unique preorder IDs */
            function base36converter() {
                $input = time();
                $base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $output = '';

                while ($input > 0) {
                    $output = $base[$input%36] . $output;
                    $input = floor($input / 36);
                }

                return $output;
            }

            /* mysql connection */
            $dbname = ($_SERVER['HTTP_HOST']=='dev.humanconnectome.org') ? 'devadmin_orders_dev' : 'devadmin_orders';
            $dbuser = 'devadmin_drives';
            $dbpass = '1IXQe6NUdnVB';
            $dbhost = $_ENV{DATABASE_SERVER};
            $db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

            if (!$db) {
                die('Connect Error (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error());
            }
            mysqli_set_charset($db,"utf8")
            ?>

            <?php
            /* create entry in 'orders' database and capture its just-created ID field */
            $neworder = mysqli_query($db,'INSERT INTO orders (timestamp,status) VALUES (NOW(),\'incomplete\')');
            $orderIds = mysqli_fetch_array(mysqli_query($db,'SELECT id FROM orders ORDER BY id DESC LIMIT 1'));
            $orderId = $orderIds[0];
            $data_ordered = '';
            $cnum = 0;

            if ($_POST['itemcode']) :
                foreach ($_POST['itemcode'] as $key => $value) :
                    if ($value == 'HCP-CUSTOM') :
                        $data_ordered .= $_POST['custom-drive-id'][$cnum];
                        $customDriveId[$key] = $_POST['custom-drive-id'][$cnum];
                        $cnum++;
                    else :
                        $data_ordered .= $value;
                    endif;
                    $data_ordered .= ',';
                endforeach;
            endif;
            ?>

            <?php
            /* look up customer by email address and either match, or create customer ID */
            $q = "SELECT COUNT(*) as rows,customer_id as id,shipping_country FROM orders WHERE customer_email = '".$_POST['customer_email']."';";
            $customer_record = mysqli_fetch_array(mysqli_query($db,$q));
            if (!$customer_record['id']) :
                // generate country code
                $q = "SELECT * FROM country_codes WHERE name = '".$_POST['HCP-COUNTRY']."';";
                $r = mysqli_query($db,$q) or die($q);
                $countryinfo = mysqli_fetch_array($r);
                ?><p><?php implode(",",$countryinfo); ?></p><?php
                $str1 = $countryinfo['dial_code'];
                while (strlen($str1) < 4) :
                    $str1 = "0".$str1;
                endwhile;
                $str2 = $orderId;
                while (strlen($str2) < 4) :
                    $str2 = "0".$str2;
                endwhile;
                $customerID = "H".$str1.$str2;
            else :
                $customerID = $customer_record['id'];
            endif;
            ?>

            <?php
            /* enter the order info into the order DB */
            $orderType = ($_POST['order-type']);
            $q = "UPDATE orders SET customer_name='".$_POST['customer_name']."',
                    customer_id='".$customerID."',
                    customer_email='".$_POST['customer_email']."',
                    customer_institution='".$_POST['customer_institution']."',
                    dut_accepted='".$_POST['dut']."',
                    shipping_address='".$_POST['shipping_address']."',
                    shipping_address_2='".$_POST['shipping_office']."',
                    shipping_city='".$_POST['shipping_city']."',
                    shipping_state='".$_POST['shipping_state']."',
                    shipping_postal_code='".$_POST['shipping_postal_code']."',
                    shipping_country='".$_POST['HCP-COUNTRY']."',
                    shipping_phone='".$_POST['shipping_phone']."',
                    data_ordered='".$data_ordered."',
                    drives_ordered='".$_POST['drivecnt']."',
                    enclosures_ordered='".$_POST['enclosureQty']."',
                    order_type='".$orderType."' WHERE id='".$orderId."';";
            $r = mysqli_query($db,$q);

            if ($orderType == 'preorder') :
                $preorderId = base36converter();
                $preorderAmount = 100;
                $q = "INSERT INTO preorders (id,customer_name,customer_email,order_id,release_id,status,amount) VALUES ('".$preorderId."','".$_POST['customer_name']."','".$_POST['customer_email']."','".$orderId."','".$_POST['itemcode'][0]."','incomplete','".$preorderAmount."')";
                $r = mysqli_query($db,$q) or die ($q);

                $q = "UPDATE orders SET preorder_id='".$preorderId."' WHERE id='".$orderId."';";
                $r = mysqli_query($db,$q) or die ($q);
            endif;
            ?>

            <?php
            /* set array variables for product orders */
            $items = $_POST['itemcode'];
            $amounts = ($_POST['amount']) ? $_POST['amount'] : array();
            $driveFormat = $_POST['drive-format'];
            $itemcount = $_POST['itemcnt'];
            $drivecount = $_POST['drivecnt'];
            $discount = 0-$_POST['HCP-RECYCLE'];
            $enclosures = $_POST['enclosureQty'];
            $subtotal = array_sum($amounts) +(39 * $enclosures); // + $discount
            $salesTax = 0; // will be set in a moment
            $shipping = array();
            $state = $_POST['shipping_state'];

            if ($state == 'HI') :
                echo "<p>Hawaii residents are charged international shipping rates by Fedex, when shipping from St Louis, MO.</p>";
            endif;

            /* Calculate sales tax, if necessary. Only tax drive cost. */
            $taxRates = array(
                "MO" => 0.08679,
                "IN" => 0.07
            );
            if (!isset($_POST['taxexempt'])) {
                $salesTax = $taxRates[$state] * $subtotal;
            }

            /* Get shipping cost from release info. Include a 2.25% Cashnet fee that gets added on top. */
            $country = $_POST['HCP-COUNTRY'];
            $customShipping = $_POST['customShipping'];

            /* define shipping costs. (Now drive quantity is the only dependent) */

            function calculateShipping($shippingJson) {
                global $country, $customShipping, $state;

                // convert JSON string of shipping costs to PHP array
                $shippingCosts = json_decode($shippingJson,true);

                if ($customShipping) :
                    $s = $customShipping;
                else :
                    switch($country) {
                        case "United States":
                            if ($state == 'HI') {
                                $s = $shippingCosts["Canada"];
                            } else {
                                $s = $shippingCosts["United States"];
                            }
                            break;
                        case "Canada":
                            $s = $shippingCosts["Canada"];
                            break;
                        case "Mexico":
                            $s = $shippingCosts["Mexico"];
                            break;
                        default:
                            $s = $shippingCosts["International"];
                    }
                endif;

                return $s;
            }

            ?>

            <!-- report to customer -->
            <p><strong>Order ID: <?php echo $orderId; ?></strong></p>
            <table cellspacing="1" cellpadding="3" width="100%" class="sortable" style="margin-bottom: 17px;">
                <thead>
                <tr><th style="background-color:#06c;">Customer ID: <?php echo $customerID ?></th><th>Format </th><th>Drives Shipped</th><th>Cost</th></tr>
                </thead>
                <tbody>

                <?php
                if ($items) :
                    foreach ($items as $key => $value) {
                        $q = "SELECT * FROM releases WHERE release_id='".$value."'";
                        $release = mysqli_fetch_array(mysqli_query($db,$q));
                        if ($value == "HCP-CUSTOM") :
                            $release['title'] =  $_POST['custom-drive-id'][$key];
                        endif;
                        $driveQty = $release['drive_qty'] * $_POST[$value.'qty']; // multiply quantity ordered by drives per release

                        $orderToQuery = $value;
                        $driveToQuery = explode(',',$release['contains_drives']); // get first drive from list of drives
                        $driveToQuery = $driveToQuery[0];

                        $shipping[$key] = calculateShipping($release['shipping_rate']);
                        ?>

                        <tr><td><?php echo $release['title']; ?></td><td>Linux</td><td><?php echo $driveQty.' '.$release['drive_type'].' drive(s)' ?></td><td align="right">$<?php echo number_format($amounts[$key],2) ?></td></tr>

                    <?php
                    } // end item listing
                endif;

                if ($_POST['enclosureQty'] > 0) {
                    ?>
                    <tr><td colspan="3">Drive Enclosures: QTY (<?php echo $_POST['enclosureQty'] ?>)</td><td align="right">$<?php echo number_format(39*$_POST['enclosureQty'],2) ?></td></tr>
                <?php
                }

                if ($salesTax > 0) {
                    ?>
                    <tr><td colspan="3">Sales Tax</td><td align="right">$<?php echo number_format($salesTax,2); ?></td></tr>
                <?php
                }

                $totalShipping = array_sum($shipping);
                // calculate CashNet fee as 2.25% of every dollar processed as payment, and add it to the S&H.
                $cashNet = ($subtotal + $salestax + $totalShipping)*(0.0225);
                ?>

                <tr>
                    <td colspan="3">Shipping Cost <?php if (($country != "United States") && ($country != "Canada") && ($country != "Mexico")) : echo " (includes $40 international duty)"; endif; ?></td>
                    <td align="right">$<?php echo number_format($totalShipping,2); ?></td>
                </tr>
                <tr>
                    <td colspan="3">Transaction Fee (bundled into final "Shipping &amp; Handling" charge)</td><td align="right">$<?php echo number_format($cashNet,2); ?></td>
                </tr>
                <?php if ($_POST['order-type'] == "preorder") : ?>

                    <tr>
                        <td colspan="3">
                            <strong>Total Expected Cost:</strong></td><td align="right"><strong>$<?php echo number_format($subtotal + $salesTax + $totalShipping + $cashNet,2) ; ?></strong></td>
                    </tr>
                    <tr style="background-color: #fffcc9">
                        <td colspan="3"><strong>Preorder Deposit Amount: </strong></td><td align="right"><strong>$<?php echo number_format($preorderAmount,2); ?></strong></td>
                    </tr>
                <?php else : ?>
                    <tr>
                        <td colspan="3"><strong>Total Cost:</strong></td><td align="right"><strong>$<?php echo number_format($subtotal + $salesTax + $totalShipping + $cashNet,2) ; ?></strong></td>
                    </tr>
                <?php endif; ?>



                </tbody>
            </table>



            <?php
            if ($_POST['HCP-RECYCLE']) :
                ?>
                <table cellspacing="1" cellpadding="3" width="100%" class="sortable" style="margin-top: 3em;">
                    <tr><th>Drive Recycling Program</th></tr>
                    <tr>
                        <td>
                            <?php // check to see if referenced order number has a drive associated.
                            $q = "SELECT orders.customer_email as email, drives_ordered.serial, orders.id, orders.transaction_id, orders.status
								FROM orders INNER JOIN drives_ordered 
								ON orders.id=drives_ordered.order_id 
								WHERE orders.status != 'incomplete' 
								AND (orders.transaction_id='".$_POST['recycle-order']."' OR orders.id='".$_POST['recycle-order']."');";
                            $r = mysqli_query($db,$q) or die("ERROR: Query for your original order returned no results. Please return to the order form and try again.");
                            $recycle_order = mysqli_fetch_array($r);
                            ?>

                            <p style="float:left; margin-right: 20px;"><img src="/img/art/icon-SATA-drive-recycle.png" /></p>
                            <?php $driveRecycle = FALSE; ?>
                            <?php if ($recycle_order['serial']) : ?>
                                <?php if ($recycle_order['email'] == $_POST['customer_email']) : ?>
                                    <p>You have signed up for a Drive Recycling Program Rebate. Rebate depends on receipt and verification of hard drive sent to you from order #<?php echo $_POST['recycle-order'] ?>. A refund will in this amount will be issued to your credit card after that verification. Details will be confirmed after you complete your order.</p>
                                    <div><label for="recycle_serial">Drive Serial # to return:</label>
                                        <input name="recycle_serial" type="text" value="<?php echo ($recycle_order['serial']) ? $recycle_order['serial'] : "No serial number found." ?>" /></div>
                                    <div><label for="recycle_order_id">Transaction ID from Original Order</label>
                                        <input name="recycle_order_id" type="text" value="<?php echo ($recycle_order['transaction_id']) ? $recycle_order['transaction_id'] : "No transaction id found." ?>" /></div>
                                    <?php
                                    $q = "UPDATE orders SET drive_recycle='Yes', drive_recycle_transaction_id='".$_POST['recycle-order']."' where id='".$orderId."';";
                                    $r = mysqli_query($db,$q) or die($q);
                                    $driveRecycle = TRUE;
                                    ?>
                                <?php else : ?>
                                    <p>Error: You attempted to sign up for the Drive Recycling program, but your email address does not match the one associated with the original transaction ID that you provided: <strong><?php echo $_POST['recycle-order'] ?></strong>. Please try again or contact <a href="mailto:orders@humanconnectome.org"><strong>orders@humanconnectome.org</strong></a> if you wish to enroll in the Drive Recycling Program.</p>
                                    <p class="hidden"><?php echo $q ?></p>
                                <?php endif; // customer email match ?>
                            <?php else : ?>
                                <p>Error: We could not find a drive serial number attached to the order ID that you provided: <strong><?php echo $_POST['recycle-order'] ?></strong>. Please try again or contact <a href="mailto:orders@humanconnectome.org"><strong>orders@humanconnectome.org</strong></a> if you wish to enroll in the Drive Recycling Program.</p>
                                <p class="hidden"><?php echo $q ?></p>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            <?php
            endif; // Drive Recycle option
            ?>

            <!-- BEGIN ORDER FORM -->

            <?php if ($_SERVER['HTTP_HOST'] == 'dev.humanconnectome.org') : ?>
            <!-- test storefront - no live transactions -->
            <form class="hcpForm" action="https://commerce.cashnet.com/404handler/pageredirpost.aspx?Virtual=HCPtest" method="post" style="position:relative">
                <p style="position: absolute; color: red; bottom: 0px; left: 0px;">Test Storefront - no transactions will be processed.</p>
                <?php else : ?>
                <!-- LIVE storefront! -->
                <form class="hcpForm" action="https://commerce.cashnet.com/404handler/pageredirpost.aspx?Virtual=HCP" method="post">
                    <?php endif; ?>

                    <h3>Shipping Information</h3>
                    <p><?php echo $_POST["customer_name"]?><br />
                        <?php echo ($_POST["customer_institution"]) ? $_POST["customer_institution"]."<br />" : "" ?>
                        <?php echo $_POST["shipping_address"]?><br />
                        <?php echo ($_POST["shipping_office"]) ? $_POST["shipping_office"]."<br />" : "" ?>
                        <?php echo $_POST["shipping_city"]?>, <?php echo $_POST["shipping_state"]?><br />
                        <?php echo $_POST["shipping_postal_code"]?><br />
                        <?php echo $_POST["HCP-COUNTRY"]?><br />
                        <?php echo $_POST["shipping_phone"]?><br />
                        <?php echo $_POST["customer_email"]?></p>

                    <h3>Billing Information</h3>
                    <? if ($_POST['billing']=='yes') :  // if user specified using the same shipping and billing addresses ?>

                        <p><?php echo $_POST["customer_name"]?><br />
                            <?php echo ($_POST["customer_institution"]) ? $_POST["customer_institution"]."<br />" : "" ?>
                            <?php echo $_POST["shipping_address"]?><br />
                            <?php echo ($_POST["shipping_office"]) ? $_POST["shipping_office"]."<br />" : "" ?>
                            <?php echo $_POST["shipping_city"]?>, <?php echo $_POST["shipping_state"]?><br />
                            <?php echo $_POST["shipping_postal_code"]?><br />
                            <?php echo $_POST["HCP-COUNTRY"]?><br />
                            <?php echo $_POST["shipping_phone"]?><br />
                            <?php echo $_POST["customer_email"]?></p>
                        <input type="hidden" name="HCP-NAME_G" value="<?php echo $_POST['customer_name'] ?>" />
                        <input type="hidden" name="HCP-EMAIL_G" value="<?php echo $_POST['customer_email'] ?>" />
                        <input type="hidden" name="HCP-INSTITUTION" value="<?php echo ($b ? $_POST['customer_institution'] : '') ?>" />
                        <input type="hidden" name="HCP-ADDR_G" value="<?php echo $_POST['shipping_address']." ".$_POST['shipping_office'] ?>" />
                        <input type="hidden" name="HCP-CITY_G" value="<?php echo $_POST['shipping_city'] ?>" />
                        <input type="hidden" name="HCP-STATE_G" value="<?php echo $_POST['shipping_state'] ?>" />
                        <input type="hidden" name="HCP-ZIP_G" value="<?php echo $_POST['shipping_postal_code'] ?>" />
                        <input type="hidden" name="HCP-COUNTRY" value="<?php echo $_POST['HCP-COUNTRY'] ?>" />
                        <?php
                        $q = "UPDATE orders SET billing_address='".$_POST['shipping_address']." ".$_POST['shipping_office']."',
										billing_city='".$_POST['shipping_city']."', 
										billing_state='".$_POST['shipping_state']."', 
										billing_postal_code='".$_POST['shipping_postal_code']."', 
										billing_country='".$_POST['HCP-COUNTRY']."' WHERE id='".$orderId."';";
                        $r = mysqli_query($db,$q);
                        ?>

                    <? else : // if not, gather and validate that information here ?>
                        <div class="hcpForm-wrapper" name="billing">
                            <div>
                                <label for="HCP-NAME_G">Name</label>
                                <input type="text" name="HCP-NAME_G" style="width:485px" class="required" />
                            </div>
                            <div>
                                <label for="HCP-INSTITUTION">Institution</label>
                                <input type="text" name="HCP-INSTITUTION" style="width:485px" />
                            </div>
                            <div>
                                <label for="HCP-EMAIL_G">Email Address</label>
                                <input type="text" name="HCP-EMAIL_G" style="width:485px" class="required email" />
                            </div>
                            <div>
                                <label for="HCP-ADDR_G">Street Address</label>
                                <input type="text" name="HCP-ADDR_G" style="width:485px" class="required" />
                            </div>
                            <div>
                                <label for="address-extra">Street Address 2</label>
                                <textarea name="address-extra" style="width:485px" rows="3"></textarea>
                            </div>
                            <div>
                                <label for="HCP-PHONE">Phone Number (required for FedEx shipping)</label>
                                <input type="text" name="HCP-PHONE" style="width:485px" class="required" />
                            </div>
                            <div>
                                <label for="HCP-CITY_G">City</label>
                                <input type="text" name="HCP-CITY_G" style="width:290px" class="required" />
                            </div>
                            <?php if ($_POST['HCP-COUNTRY'] == "United States") : ?>
                                <div class="state-US">
                                    <label for="HCP-STATE_G">State</label>
                                    <select name="HCP-STATE_G" class="required">
                                        <option selected="selected"></option><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DC">District Of Columbia</option><option value="DE">Delaware</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option>
                                    </select>

                                </div>
                            <?php else : ?>
                                <div class="state-int">
                                    <label for="HCP-STATE_G">State / Province / Region</label>
                                    <input type="text" name="HCP-STATE_G" style="width:150px" />
                                </div>
                            <?php endif; ?>
                            <div>
                                <label for="HCP-ZIP_G">ZIP/Postal Code</label>
                                <input type="text" name="HCP-ZIP_G" style="width:90px" class="required" />
                            </div>
                            <div>
                                <label for="HCP-COUNTRY">Country (must be the same as your shipping address)</label>
                                <select name="HCP-COUNTRY">
                                    <option value="<?php echo $_POST['HCP-COUNTRY'] ?>"><?php echo $_POST['HCP-COUNTRY'] ?></option>
                                </select>
                            </div>
                        </div>
                    <?php endif;  // end billing address form ?>



                    <?php
                    /*
                     * CONVERT INHERITED FORM VALUES FROM 'POST' TO CASHNET FORMAT
                     */
                    ?>

                    <input type="hidden" name="itemcnt" value="<?php echo $_POST['itemcnt']; ?>" />
                    <input type="hidden" name="orderId" value="<?php echo $orderId ?>" />
                    <input type="hidden" name="HCP-ORDERID" value="<?php echo $orderId ?>" />



                    <?php
                    $item=0;
                    if ($items) :
                        foreach($items as $key => $value) {
                            $item = $key+1;

                            if ($value == "HCP-CUSTOM") {
                                ?>
                                <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="HCP-CUSTOM" />
                                <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo number_format($amounts[$key],2) ?>" />
                                <input type="hidden" name="<?php echo 'qty'.$item ?>" value="1" />
                                <input type="hidden" name="<?php echo 'ref1type'.$item ?>" value="HCP-DRIVE FORMAT" />
                                <input type="hidden" name="<?php echo 'ref1val'.$item ?>" value="<?php echo $driveFormat[$key] ?>" />
                                <input type="hidden" name="<?php echo 'ref2type'.$item ?>" value="HCP-DRIVE ID" />
                                <input type="hidden" name="<?php echo 'ref2val'.$item ?>" value="<?php echo $customDriveId[$key] ?>" />
                            <?php
                            } elseif ($_POST['order-type'] == "preorder") {
                                ?>
                                <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="HCP-PREORDER" />
                                <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo number_format($preorderAmount,2) ?>" />
                                <input type="hidden" name="<?php echo 'qty'.$item ?>" value="1" />
                                <input type="hidden" name="<?php echo 'ref1type'.$item ?>" value="HCP-PREORD" />
                                <input type="hidden" name="<?php echo 'ref1val'.$item ?>" value="<?php echo $value ?>" />
                                <input type="hidden" name="<?php echo 'ref2type'.$item ?>" value="HCP-PREORD-ID" />
                                <input type="hidden" name="<?php echo 'ref2val'.$item ?>" value="<?php echo $preorderId ?>" />
                            <?php
                            } else {
                                ?>
                                <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="<?php echo $value ?>" />
                                <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo number_format($amounts[$key],2) ?>" />
                                <input type="hidden" name="<?php echo 'qty'.$item ?>" value="1" />
                                <input type="hidden" name="<?php echo 'ref1type'.$item ?>" value="HCP-DRIVE FORMAT" />
                                <input type="hidden" name="<?php echo 'ref1val'.$item ?>" value="<?php echo $driveFormat[$key] ?>" />

                            <?php
                            }
                            ?>
                            <!-- extra form fields for final receipt -->
                            <input type="hidden" name="drive-format[]" value="<?php echo $driveFormat[$key] ?>" />
                        <?php
                        } // end item (drive) entry
                    endif;

                    if ($_POST['order-type'] == "preorder") :
                        $q = "INSERT INTO receipt (order_id,preorder_amount) VALUES ('".$orderId."','".$preorderAmount."');";
                        $r = mysqli_query($db,$q) or die($q);
                    else :
                        $q = "INSERT INTO receipt (order_id,total_drive_cost) VALUES ('".$orderId."','".array_sum($amounts)."');";
                        $r = mysqli_query($db,$q) or die($q);
                    endif;

                    if ($_POST['justification']) :
                        // store justification for price change in receipt notes.
                        $q = "UPDATE receipt SET receipt_notes = '".stripslashes($_POST['justification'])."' WHERE order_id='".$orderId."';";
                        $r = mysqli_query($db,$q) or die($q);
                    endif;

                    /* add drive enclosure if ordered */
                    if ($_POST['enclosureQty'] > 0) :
                        $item++;
                        $enclosureCost = $_POST['enclosureQty'] *39;

                        $q = "UPDATE receipt SET enclosure_cost='".$enclosureCost."' WHERE order_id='".$orderId."';";
                        $r = mysqli_query($db,$q) or die($q);

                        ?>
                        <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="HCP-CABLE" />
                        <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo number_format($enclosureCost,2); ?>" />
                        <input type="hidden" name="<?php echo 'qty'.$item ?>" value="<?php echo $_POST['enclosureQty'] ?>" />

                    <?php
                    endif; // enclosure

                    /* add sales tax as a new line-item on the invoice if necessary */
                    if (isset($salesTax) && $_POST['order-type'] != "preorder") :
                        $item++;

                        $q = "UPDATE receipt SET sales_tax='".number_format($salesTax,2)."' WHERE order_id='".$orderId."';";
                        $r = mysqli_query($db,$q) or die($q);

                        ?>
                        <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="HCP-SALESTAX" />
                        <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo $salesTax ?>" />
                    <?php
                    endif; // sales tax

                    if ($_POST['order-type'] != "preorder") :
                        /* Add shipping cost as a new line item on the invoice */
                        $item++;

                        $q = "UPDATE receipt SET shipping_cost='".number_format($totalShipping,2)."', cashnet_fee='".number_format($cashNet,2)."' WHERE order_id='".$orderId."';";
                        $r = mysqli_query($db,$q) or die($q);
                    ?>
                    <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="HCP-SHIPPING" />
                    <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo $totalShipping + $cashNet ?>" />

                    <?php
                    endif; // shipping

                    /* add drive recycling discount if ordered */

                    if ($_POST['HCP-RECYCLE'] && $driveRecycle) :

                        /*
                        don't update this until order is confirmed

                        $q = "UPDATE receipt SET drive_discount='".$_POST['HCP-RECYCLE']."' WHERE order_id='".$orderId."'; ";
                        $r = mysqli_query($db,$q) or die($q);

                        $q = "UPDATE orders SET drive_recycle='Yes',drive_recycle_transaction_id='".$_POST['recycle-order']."' WHERE id='".$orderId."'; ";
                        $r = mysqli_query($db,$q) or die($q);
                        */

                        $item++;
                        ?>
                        <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="HCP-RECYCLE" />
                        <input type="hidden" name="<?php echo 'amount'.$item ?>" value="0.00" />
                        <input type="hidden" name="<?php echo 'qty'.$item ?>" value="1" />
                        <input type="hidden" name="<?php echo 'ref1type'.$item ?>" value="HCP-RECYCLE-SERIAL" />
                        <input type="hidden" name="<?php echo 'ref1val'.$item ?>" value="<?php echo $recycle_order['serial'] ?>" />
                    <?php

                    endif; // recycle
                    ?>

                    <p align="right" style="margin-bottom:0;"><input type="submit" value="Proceed to Checkout" /></p>

                </form>
                <p></p>
                <p>Credit card information will be gathered at checkout. Forms of payment we accept:</p>
                <p><img src="/img/icons/payment-American-Express.png" alt="AMEX" /> <img src="/img/icons/payment-Discover.png" alt="Discover" /> <img src="/img/icons/payment-Mastercard.png" alt="Mastercard" /> <img src="/img/icons/payment-Visa.png" alt="Visa" /> <img src="/img/icons/payment-JCB.png" alt="JCB" /></p>

                <!-- condition if DUT was not accepted -->
                <?php else : ?>
                    <h3>Order form error</h3>
                    <p>Acceptance of the Data Use Terms must be registered in order to complete your order. Please <a href="javascript:history.back()">go back to the previous page</a> and review your order.</p>

                <?php endif; ?>

                <div style="background-color:#f0f0f0; padding:6px;" class="hidden">
                    <?php
                    printArray($_POST);

                    function printArray($array){
                        echo "<ul>";
                        foreach ($array as $key => $value){
                            echo "<li>$key => $value</li>";
                            if(is_array($value)){ //If $value is an array, print it as well!
                                printArray($value);
                            }
                        }
                        echo "</ul>";
                    }

                    ?>
                </div>
        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">
                <?php if ($release['status'] == 'current') : ?>
                <div class="sidebox databox">
                    <?php
                    $q = "SELECT count(drive_status) as drives from drive_inventory WHERE release_id='".$driveToQuery."' AND drive_status='NEW';";
                    $r = mysqli_query($db,$q);
                    $inv = mysqli_fetch_array($r);

                    $q= "SELECT count(id) as orders from orders WHERE data_ordered LIKE '%".$orderToQuery."%' AND status='open';";
                    $r = mysqli_query($db,$q);
                    $ord = mysqli_fetch_array($r);
                    ?>
                    <h2>Order Availability</h2>
                    <p><strong>Items In Stock:</strong></p>
                    <div class="storefront-counter stock"><?php echo intval($inv['drives'])+3 ?></div>
                    <p><strong>Orders Already In Queue:</strong></p>
                    <div class="storefront-counter orders"><?php echo intval($ord['orders']) ?></div>
                    <p><strong>Projected Ship Window:</strong>
                        <?php if ( (intval($inv['drives'])+9) > intval($ord['orders']) ) :
                                echo "Within 30 Days";
                        else :
                                echo "Within 45 Days";
                        endif; ?>
                        </p>
                </div>
                <?php endif; ?>

                <div class="sidebox databox">
                    <h2>Shipping &amp; Fulfillment Details</h2>
                    <p><img src="/img/art/icon-shipping-Fedex.png" width="256" height="256" alt="SATA Drive" style="margin-left:-10px" /></p>
                    <p>Please note: we batch-process orders for efficiency. <strong>The fulfillment process may take up to thirty days from the time of your order</strong>. </p>
                    <p>All Connectome Data is shipped from Washington University in Saint Louis via FedEx. When the order is shipped, you will be contacted and provided with a FedEx tracking number. Your order may be shipped in multiple packages, depending on order size.</p>
                    <p>Feel free to contact us at any time in the process with your order number at <a href="mailto:orders@humanconnectome.org">orders@humanconnectome.org</a>.</p>
                </div>
                <!-- /sidebox -->
            </div>
        </div>
        <!-- /sidebar -->


    </div> <!-- middle -->


    <?php
    mysqli_close($db);
    ?>

    <div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
        <p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a> - <a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a></span><br />
            <a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a> - <a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a><br />
            <a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a> - <a href="http://duke.edu/" title="Duke University" target="_new">Duke University</a></p>
        <p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>.
        </p>

        <p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p>

        <!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="http://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
