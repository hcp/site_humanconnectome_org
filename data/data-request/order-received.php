<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Data | Human Connectome Project</title>
<meta http-equiv="Description" content="Public access to HCP data. Currently, Phase 1 Pilot Data is available." />
<meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
<link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
<link href="/css/sortable.css" rel="stylesheet" type="text/css" />
<link href="/css/data.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="/js/jquery.easing.1.2.js"></script>
<script type="text/javascript" src="/js/global.js"></script>


</head>

<body>
<!-- site container -->
 <div id="site_container">

 <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div>

<!-- Header -->
<div id="header"><img src="/img/header-bg.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
  <map name="Header" id="Header">
    <area shape="rect" coords="14,7,400,123" href="/" alt="Human Connectome Project Logo" />
  </map>
</div> 
<!-- end Header -->

<!-- top level navigation --><!-- #BeginLibraryItem "/Library/Top Nav.lbi" -->
<div id="topnav">
  <ul id="nav">
    <li><a href="/">Home</a></li>
    <li><a href="/about/">About the Project</a>
      <ul>
        <li><a href="/about/project/">Project Overview &amp; Components</a></li>
        <li><a href="/about/hcp-investigators.html">HCP Investigators</a></li>
        <li><a href="/about/hcp-colleagues.html">HCP Colleagues</a></li>
        <li><a href="/about/teams.html">Operational Teams</a></li>
        <li><a href="/about/external-advisory-panel.html">External Advisory Panel</a></li>
        <li><a href="/about/publications.html">Publications</a></li>
        <li><a href="/courses">HCP Course Materials</a></li>
        <li><a href="/about/pressroom/">News &amp; Updates</a></li>
      </ul>
    </li>
    <li><a href="/data/">Data</a>
      <ul>
        <li><a href="/data/">Get Access to Public Data Releases</a></li>
        <li><a href="/data/connectome-in-a-box.html">Order Connectome in a Box</a></li>
        <li><a href="/data/data-use-terms/">HCP Data Use Terms</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Home" target="_blank">HCP Wiki: Data Resources</a></li>
      </ul>
    </li>
    <li><a href="/software/">Software</a>
      <ul>
        <li><a href="/software/connectome-workbench.html">What is Connectome Workbench</a></li>
        <li><a href="/software/get-connectome-workbench.html">Get Connectome Workbench</a></li>
        <li><a href="/software/workbench-command.php">Using Workbench Command</a></li>
        <li><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></li>
        <li><a href="/documentation/HCP-pipelines/index.html">HCP MR Pipelines</a></li>
        <li><a href="/documentation/HCP-pipelines/meg-pipeline.html">HCP MEG Pipelines</a></li>
      </ul>
    </li>
    <li><a href="/documentation/">Documentation</a>
      <ul>
      	<li><a href="/documentation/">All Documentation</a></li>
        <li><a href="/documentation/citations.html">How to Cite HCP</a></li>
        <li><a href="/documentation/tutorials/">Software Tutorials</a></li>
        <li><a href="/documentation/S900/">900 Subjects Data Release</a></li>
        <li><a href="/documentation/HCP-pipelines/">HCP Pipelines</a></li>
          <li><a href="/documentation/subject-key">How to Cite HCP Subjects with a Subject Key</a></li>
      </ul>
    </li>
    <li><a href="/contact/">Contact</a>
      <ul class="subnav">
      	<li><a href="/contact/">General Project Inquiries</a></li>
        <li><a href="/contact/#subscribe">Subscribe to Mailing Lists</a></li>
        <li><a href="/careers/">Career Opportunities</a></li>
        <li> <a href="/contact/collaboration-request.php">Collaboration Request</a></li>
        <li> <a href="/contact/feature-request.php">Feature Request</a></li>
        <li><a href="https://wiki.humanconnectome.org/display/PublicData/Media+Images+from+the+Human+Connectome+Project+WU-Minn+Consortium" target="_blank">HCP Wiki: Media Images</a></li>
      </ul>
    </li>
    <li><a href="/resources/">Other Resources</a>
      <ul class="subnav">
        <li><a href="http://wiki.humanconnectome.org/" target="_blank">HCP Wiki</a></li>
        <li><a href="http://lifespan.humanconnectome.org/" target="_blank">HCP Lifespan Pilot Project</a></li>
        <li><a href="/documentation/MGH-diffusion/">MGH Adult Diffusion Project</a></li>        
        <li><a href="/resources/faq-disease-related-connectome-research.html">FAQ: Disease-Related Connectome Research</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="search">
    <!--   If Javascript is not enabled, nothing will display here -->
    <script>
      (function() {
        var cx = '000073750279216385221:vnupk6whx-c';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
            '//www.google.com/cse/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
      })();
    </script>
    <gcse:search></gcse:search>	
</div> <!-- Search box -->
<link rel="stylesheet" href="/css/google-cse-customizations.css" />
<!-- #EndLibraryItem --><!-- End Nav -->

<!-- banner -->
<div id="bannerCollapsed" class="bannerContainer hidden">
  <span>
    <a style="background:url(/img/open.png) no-repeat 2px center #000;" onClick="openBanner();" title="Show banner">Open</a>
    </span>
  <iframe height="36" width="100%" frameborder="0" scrolling="no" src="/data/banner/collapsed/"></iframe>
</div>
<!-- end banner -->

<!-- Breadcrumbs -->
<div id="breadcrumbs">
    <p>&raquo; <a href="/data/">Get HCP Data</a> &gt; connectome in a box &gt; order received</p></div>
<!-- end breadcrumbs -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">
  <div id="content">
  	  <noscript>
      	 <p style="color:#c00; clear:both;"><strong>Alert:</strong> You appear to have JavaScript turned off. Registration and access for ConnectomeDB requires JavaScript. Please enable it in your browser before proceeding.</p>
      </noscript>
      
<?php
$orderId = $_POST['HCP-ORDERID'];
if ($orderId) {
?>
	
	<?php 
    /* mysql connection */
    $dbname = ($_SERVER['HTTP_HOST']=='dev.humanconnectome.org') ? 'devadmin_orders_dev' : 'devadmin_orders';
    $dbuser = 'devadmin_drives';
    $dbpass = '1IXQe6NUdnVB';
    $dbhost = $_ENV{DATABASE_SERVER};
    $db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

    if (!$db) {
        die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
    }
    mysqli_set_charset($db,"utf8")
    ?>
    
    <?php
	
	/* check for successful transaction */
    $success = $_POST['respmessage']; 
    if ($success == 'SUCCESS') {

        // check for order type. If this is a course registration, forward results to the appropriate handling page
        $q = "SELECT order_type FROM orders WHERE id='".$orderId."';";
        $r = mysqli_query($db,$q) or die($q);
        $orderType = mysqli_fetch_array($r);

        if ($orderType['order_type'] == 'course') :
            // build a form out of every variable in POST
            ?>
            <form action="/course-registration/registration-received.php" method="post" id="redirect-form">
                <?php foreach ($_POST as $key => $value): ?>
                <input type="hidden" name="<?php echo $key ?>" value="<?php echo $value ?>" />
                <?php endforeach; ?>
            </form>

            <?php
            // submit that form
            die('<p>Forwarding to Registration Form Handler</p><script>$("#redirect-form").submit();</script>');
        endif;

        if ($orderType['order_type'] == 'preorder') :
            $preorder = true;
            $preorderId = $_POST['ref2val1'];
            $preorderRelease = $_POST['ref1val1'];
        endif;

		?>
        
        <h2>Connectome In A Box:  Order Received</h2>
        <p>Thank you for your order. Your order ID number is #<?php echo $orderId; ?> and your payment transaction ID is #<?php echo $_POST['tx'] ?>. Please save these for your records.</p>
        <?php if (!$preorder) : ?>
        <p>You will receive an email from us soon when this order is shipped with tracking information. If any of this is incorrect, please contact us at <a href="mailto:orders@humanconnectome.org"><strong>orders@humanconnectome.org</strong></a> to correct it.</p> 
        <?php endif; ?>
        <h3>Order Summary</h3>
        <?php 
			// set variables
			$items = $_POST['itemcode'];
			
			
			$q = "SELECT * FROM receipt,orders WHERE receipt.order_id='".$orderId."' AND orders.id='".$orderId."';";
			$r = mysqli_query ($db,$q) or die($q);
			$order = mysqli_fetch_array($r);
			
			/* update order status in DB -- set status according to shipping country */
            if (!$preorder) : 
			    $orderStatus = ($order['shipping_country'] == "United States") ? "open" : "pending"; 
            else : 
                $orderStatus = "open";

                $q = "UPDATE preorders SET status = '".$orderStatus."' WHERE id='".$preorderId."';";
                $r = mysqli_query($db,$q) or die($q); 
            endif;
			
			$q = "UPDATE orders SET status='".$orderStatus."',transaction_id='".$_POST['tx']."' WHERE id='".$orderId."';";
			$r = mysqli_query($db,$q) or die($q);
			
			if ($orderStatus == "pending") : 
				// send email
				$to = "langtonl@wusm.wustl.edu"; 
				$subject = "Connectome In A Box: New International Order Requires Review";
				$message = "A new international order has been received and requires review. Please visit http://orders.humanconnectome.org/dashboard.php?orderNum=".$orderId." to review.";
				$headers   = array();
				$headers[] = "MIME-Version: 1.0";
				$headers[] = "Content-type: text/plain; charset=iso-8859-1";
				$headers[] = "From: HCP Orders <orders@humanconnectome.org>";
				$headers[] = "Cc: orders@humanconnectome.org";
				$headers[] = "Reply-To: HCP Orders <orders@humanconnectome.org>";
				$headers[] = "X-Mailer: PHP/".phpversion();
				
				if ($to && $subject && $message && $headers) : 
					mail($to,$subject,$message,implode("\r\n",$headers));
				else : 
					$error = '<p>A required field was missing. Email could not be sent.</p>';
					$error .= '<p>To: '.$to.' / Subject: '.$subject.' / Message: '.$message.' / Headers: '.implode("\r\n",$headers).'</p>';
					die($error);
				endif;
			endif; 
		
			/*
			$q = "SELECT * FROM drives_ordered WHERE order_id='".$orderId."';";
			$items = mysqli_query ($db,$q) or die($q); 
			*/

            if (!$preorder) : 

    			$q = "SELECT * FROM receipt WHERE order_id='".$orderId."';";
    			$r = mysqli_query ($db,$q) or die($q);
    			$receipt = mysqli_fetch_array($r);
    			
    			// build array of releases to query against
    			$q = "SELECT release_id FROM releases";
    			$r = mysqli_query ($db,$q) or die($q);
    			$releases = array(); 
    			while ($release = mysqli_fetch_array($r)) :
    				$releases[] = $release[0];
    			endwhile;
    			
    			$drives_ordered = $order['drives_ordered'];
    			$enclosures_ordered = $order['enclosures_ordered'];
    			$items=array();
    			
    			for ($i=1; $i<=$drives_ordered; $i++) : 
    				// check for item code, then check for quantity of each drive ordered
    				$itemcode = $_POST['itemcode'.$i];
    				$qty = $_POST['qty'.$i]; 
    				
    				if (in_array($itemcode,$releases)) :
    				//	echo "<p>".$itemcode." found in [".implode(", ",$releases)."]";
    					// add to items array
    					$items[$i]=array('release_id' => $_POST['itemcode'.$i], 'drive_format' => $_POST['ref1val'.$i], 'cost' => $_POST['amount'.$i]);
    					// get drive quantity from bundled products
    					$q = "SELECT drive_qty as dq,contains_drives as drives FROM releases WHERE release_id = '".$_POST['itemcode'.$i]."' ";
    					$r = mysqli_query ($db,$q) or die($q);
    					$drives_in_release = mysqli_fetch_array($r);

    					// add each drive ordered to order database
    					for ($j=1; $j<=$qty; $j++) :  
    						// special condition for drive bundles - add one item in drives ordered for each drive in the bundle.
    						if ($drives_in_release['dq'] > 1) :
                                $drives_array = explode(',',$drives_in_release['drives']);
    							foreach ($drives_array as $key => $value) :
    								$q = "INSERT INTO drives_ordered (order_id, release_id, drive_format, amount_paid, order_notes) VALUES ('".$orderId."','".$value."','".$_POST['ref1val'.$i]."','".$_POST['amount'.$i]."', '".$_POST['justification']."');";
    								$r = mysqli_query($db,$q) or die($q);
    								echo "<p class='hidden'>".$q."</p>";
    							endforeach;
    						else : 
    							$q = "INSERT INTO drives_ordered (order_id, release_id, drive_format, amount_paid, order_notes) VALUES ('".$orderId."','".$drives_in_release['drives']."','".$_POST['ref1val'.$i]."','".$_POST['amount'.$i]."', '".$_POST['justification']."');";
    							$r = mysqli_query($db,$q) or die($q);
    							echo "<p class='hidden'>".$q."</p>";
    						endif; 
    					endfor;
    				elseif ($itemcode == "HCP-CUSTOM") :
                    // special condition for taking single drive orders - the drive ID is passed as a CashNet reference value.
                        $custom_drive_id = $_POST['ref2val'.$i];
                        $items[$i]=array('release_id' => $_POST['itemcode'.$i], 'drive_id' => $custom_drive_id, 'drive_format' => $_POST['ref1val'.$i], 'cost' => $_POST['amount'.$i]);
                        $q = "INSERT INTO drives_ordered (order_id, release_id, drive_format, amount_paid, order_notes) VALUES ('".$orderId."','".$custom_drive_id."','".$_POST['ref1val'.$i]."','".$_POST['amount'.$i]."', '".$_POST['justification']."');";
                        $r = mysqli_query($db,$q) or die($q);
                        echo "<p class='hidden'>".$q."</p>";
                    else :
    					echo "<p class='hidden'>".$itemcode." not found in [".implode(", ",$releases)."]";
    				endif;
    			endfor; 

            endif; // !preorder
					
			?>
         <!-- report to customer -->
        <table cellspacing="1" cellpadding="3" width="100%" class="sortable">
            <thead>
                <tr><th style="background-color:#06c;">Order ID: <?php echo $orderId ?></th>
                <th></th></tr>
            </thead>
            <tbody>
            
            <?php
            if ($preorder) : 
                $q = "SELECT * FROM releases WHERE release_id='".$_POST['ref2val1']."';";
                $r = mysqli_query($db,$q);
                $preorderInfo = mysqli_fetch_array($r);
                ?>
                <tr><td>Preorder: <?php echo $_POST['ref1val1'] ?></td><td align="right">$<?php echo $_POST['amount1'] ?></td></tr>
                <tr><td>Preorder Coupon Code: <?php echo $preorderId ?></td><td></td></tr>
            <?php
            else : 
                foreach ($items as $key => $item) {
    				if (in_array($item['release_id'],$releases)) : 
    				?>
    					<tr><td><?php echo $item['release_id'] ?></td><td align="right"><?php echo $_POST['qty'.$key] ?></td></tr>
    				<?php
                    elseif ($item['release_id'] == "HCP-CUSTOM") :
                    ?>
                        <tr><td>Custom Drive Order: <?php echo $item['drive_id']; ?></td><td align="right"><?php echo $_POST['qty'.$key] ?></td></tr>
                    <?php
    				else :
    					echo "<p>$item not found in [".implode(", ",$releases)."]";
    				endif;
                } // end item listing
                
    			if ($enclosures_ordered) : 
    			?>
                    <tr><td>Drive Enclosures</td><td align="right"><?php echo $enclosures_ordered ?></td></tr>
                <?php
    			endif; 
    			
    			?>
                    <tr><th colspan = 2>Item costs</th></tr>
                    <tr><td>Total Drive Cost:</td><td align="right">$<?php echo number_format($receipt['total_drive_cost'],2) ?></td></tr>
                <?php
    			if ($order['enclosures_ordered'] > 0) {
    				?>
                    <tr><td>Drive Enclosure Cost:</td> <td align="right">$<?php echo number_format($receipt['enclosure_cost'],2) ?></td></tr>
                    <?php 
    			}
    			?>
                <?php
    			if ($receipt['sales_tax']) {
    				?>
    				<tr><td>Sales Tax: </td><td align="right">$<?php echo number_format($receipt['sales_tax'],2); ?></td></tr>
                    <?php
    			}
    			?>
    				<tr><td>Shipping &amp; Handling: </td><td align="right">$<?php echo number_format($receipt['shipping_cost'] + $receipt['cashnet_fee'],2); ?></td></tr>
                	<tr><td colspan="2" align="right" style="padding-top:10px;"><p><strong>Total cost: $<?php echo number_format($receipt['total_drive_cost'] + $receipt['enclosure_cost'] + $receipt['sales_tax'] + $receipt['shipping_cost'] + $receipt['cashnet_fee'],2); ?></strong></p></td></tr>
                    
                    <tr><th colspan="2">Shipping Information</th></tr>
                    <tr><td colspan="2">
                    	<p style="margin-bottom:0;"><?php echo $order['customer_name']?><br />
                        <?php echo ($order['customer_institution'] ? $order['customer_institution'].'<br />' : '')  ?>
    			        <?php echo $order['shipping_address'] ?><br />
                        <?php echo ($order["shipping_address_2"]) ? $order['shipping_address_2'].'<br />' : '' ?>
    					<?php echo $order['shipping_city']?>, <?php echo $order['shipping_state']?><br />
    					<?php echo $order['shipping_postal_code']?><br />
    					<?php echo $order['shipping_country']?><br />
                        <?php echo $order['shipping_phone']?><br />
    					<?php echo $order['customer_email']?></p>
                    </td></tr>

            <?php endif; ?>
            </tbody>
        </table>

        <?php if ($preorder) : ?>
            <p></p>
            <p>You have successfully placed a preorder for this data release. When this data is ready to order, you will receive an email with a custom storefront link that will apply your preorder amount and enable you to complete your order for the data. If you decide to cancel your preorder, please email us at <a href="mailto:orders@humanconnectome.org" title="email us">orders@humanconnectome.org</a> and we will issue you a full refund of your preorder amount.</p>
        <?php endif; ?>
        
        <?php if ($order['drive_recycle']=='Yes') : ?>
        
        	<?php 
			// get the serial number for the drive. It is passed from Cashnet, as a reference value for the last 'item' ordered. 
			$driveSerial = $_POST['ref1val'.$_POST['itemcnt']]; 
			
			$q = "INSERT INTO recycle_orders (timestamp,order_status,customer_email,original_order_id,drive_serial) VALUES (NOW(),'Pending','".$order['customer_email']."','".$order['drive_recycle_transaction_id']."','".$driveSerial."')";
			$r = mysqli_query($db,$q) or die($q); 
			
			$q = "SELECT id FROM recycle_orders ORDER BY timestamp DESC LIMIT 1";
			$r = mysqli_query($db,$q) or die($q);
			$recycle_order = mysqli_fetch_array($r); 
			?>
        
        	<table cellspacing="1" cellpadding="3" width="100%" class="sortable" style="margin-top: 3em;">
                <tr><th colspan="2" style="background: #44bd44; border-right: 1px solid #444">Drive Recycling Rebate - Successful Enrollment</th></tr>
                <tr>
                  <td>
                    <p style="float:left; margin-right: 20px;"><img src="/img/art/icon-SATA-drive-recycle.png" /></p>
                    <p>You have signed up for a Drive Recycling Program Rebate. Your rebate order number is <input type="text" disabled="disabled" style="max-width:30px" value="<?php echo $recycle_order['id'] ?>" />. Please follow the following instructions to ship your current HCP drive to us to process your refund. <strong><a href="/documentation/tutorials/HCP-Drive-Recycle-Instructions.pdf" target="_blank">HCP Drive Recycling Instructions</a></strong></p>
                    <div><label for="recycle_serial">Drive Serial # to return:</label>
                         <input name="recycle_serial" type="text" value="<?php echo $driveSerial ?>" /></div>
                  </td>
                </tr>
            </table>
        <?php endif; ?>
        
        
    <?php 
	/* if order was unsuccessful */
		
	} else {
	
		/* update order DB */
		$q = "UPDATE orders SET status='failed',transaction_id='".$_POST['tx']."' WHERE id='".$orderId."';";
		$r = mysqli_query($db,$q) or die($q);
	?>
    	<h2>Connectome In A Box: Problem With Your Order</h2>
        <p>There was a problem with your order. Please contact <a href="mailto:orders@humanconnectome.org"><strong>orders@humanconnectome.org</strong></a> and inquire about order ID #<?php echo $_POST['HCP-ORDERID'] ?> to rectify it.</p>
        <p><strong>Error Code <?php echo $_POST['result'] ?>:</strong> <span class="error"><?php echo $_POST['respmessage'] ?> </span></p>        
	<?php
    }
	?>
        
        <div style="background-color:#f0f0f0; padding:6px; display:none;">
        <?php 
		
		function printArray($array,$title){
			echo "<h3>".$title."</h3>";
			echo "<ul>";
			 foreach ($array as $key => $value){
				echo "<li>$key => $value</li>";
				if(is_array($value)){ //If $value is an array, print it as well!
					printArray($value);
				}  
			} 
			echo "</ul>";
		}
		printArray($order,"Data in Order table");
		echo '<hr />';
		printArray($_POST,"POST data received through form");

		?>
        </div>
      
        <p style="padding-bottom:6px; border-bottom: 1px #ccc solid">&nbsp;</p>
        <?php 
			mysqli_close($db);
		?>
        
<?php 
} else {
?>
	<h2>Error</h2>
    <p>No order information. If you think you got to this page in error, please contact orders@humanconnectome.org</p>
    
    <div style="background-color:#f0f0f0; padding:6px; display:none;">
        <?php 
		
		function printArray($array){
			echo "<ul>";
			 foreach ($array as $key => $value){
				echo "<li>$key => $value</li>";
				if(is_array($value)){ //If $value is an array, print it as well!
					printArray($value);
				}  
			} 
		}
		printArray($order);
		echo '<hr />';
		printArray($_POST);

		?>
        </div>
<?php 
}
?>
</div>
  <!-- /#Content -->
  
  <div id="sidebar-rt">
      
      <div id="sidebar-content">
              <div class="sidebox databox">
                  <h2>Shipping &amp; Fulfillment Details</h2>
                  <p><img src="/img/art/icon-shipping-Fedex.png" width="256" height="256" alt="SATA Drive" style="margin-left:-10px" /></p>
                  <p>Please note: we batch-process orders for efficiency. <strong>The fulfillment process may take up to thirty days from the time of your order</strong>.</p>
                <p>All Connectome Data is shipped from Washington University in Saint Louis via FedEx. When the order is shipped, you will be contacted and provided with a FedEx tracking number. Your order may be shipped in multiple packages, depending on order size.</p>
                  <p>Feel free to contact us at any time in the process with your order number at <a href="mailto:orders@humanconnectome.org">orders@humanconnectome.org</a>.</p>
          		</div>
              <!-- /sidebox -->
         </div>
  </div>
  <!-- /sidebar -->

    
</div> <!-- middle -->

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a> - <a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a></span><br />
<a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_New">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d’Annunzio</a> - <a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a><br />
<a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a> - <a href="http://duke.edu/" title="Duke University" target="_new">Duke University</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>. 
</p>

<p><a href="/privacy/">Privacy</a> |  <a href="/sitemap/">Site Map</a> | <a href="/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p> 

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

<script src="http://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
/*
*  How to restrict a search to a Custom Search Engine
*  http://www.google.com/cse/
*/

google.load('search', '1');

function OnLoad() {
  // Create a search control
  var searchControl = new google.search.SearchControl();

  // Add in a WebSearch
  var webSearch = new google.search.WebSearch();

  // Restrict our search to pages from our CSE
  webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

  // Add the searcher to the SearchControl
  searchControl.addSearcher(webSearch);

  // tell the searcher to draw itself and tell it where to attach
  searchControl.draw(document.getElementById("search"));

  // execute an inital search
  // searchControl.execute('');
}

google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18630139-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
