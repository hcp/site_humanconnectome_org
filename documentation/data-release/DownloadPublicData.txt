Server="ftp://ftp.humanconnectome.org"
Project="OpenAccess"
StudyFolder="."
Subjlist="100307 114924 125525 138231 159239 192439 197550 249947 255639 499566 672756 792564"

read -p "Username: " Username
stty -echo
read -p "Password: " Password; echo
stty echo

if [ ! -e ${StudyFolder} ] ; then
  mkdir ${StudyFolder}
fi

if [ ! -e ${StudyFolder}/Behavioral_Data ] ; then
  mkdir ${StudyFolder}/Behavioral_Data
fi


curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/Behavioral_Data/Non-Toolbox_Behavioral_Metrics_2012-10.csv -o ${StudyFolder}/Behavioral_Data/Non-Toolbox_Behavioral_Metrics_2012-10.csv
curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/Behavioral_Data/Task-fMRI_Behavioral_Metrics_2012-10.csv -o ${StudyFolder}/Behavioral_Data/Task-fMRI_Behavioral_Metrics_2012-10.csv


for Subject in ${Subjlist} ; do
  DestinationDir="${StudyFolder}/${Subject}"
  if [ ! -e $DestinationDir ] ; then
    mkdir $DestinationDir
  fi

  if [ ! -e $DestinationDir/processed ] ; then
    mkdir $DestinationDir/processed
  fi
  
  if [ ! -e $DestinationDir/NIFTI ] ; then
    mkdir $DestinationDir/NIFTI
  fi

  
  curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/${Subject}/${Subject}.Task-fMRI_EventFiles.tar.gz -o ${DestinationDir}/${Subject}.Task-fMRI_EventFiles.tar.gz
  curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/${Subject}/processed/${Subject}_processed.tar.gz.md5 -o ${DestinationDir}/processed/${Subject}_processed.tar.gz.md5
  curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/${Subject}/processed/${Subject}_processed.tar.gz -o ${DestinationDir}/processed/${Subject}_processed.tar.gz

  curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/${Subject}/NIFTI/${Subject}_fnca.tar.gz -o ${DestinationDir}/NIFTI/${Subject}_fnca.tar.gz
  curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/${Subject}/NIFTI/${Subject}_fnca.tar.gz.md5 -o ${DestinationDir}/NIFTI/${Subject}_fnca.tar.gz.md5
  curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/${Subject}/NIFTI/${Subject}_fncb.tar.gz -o ${DestinationDir}/NIFTI/${Subject}_fncb.tar.gz
  curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/${Subject}/NIFTI/${Subject}_fncb.tar.gz.md5 -o ${DestinationDir}/NIFTI/${Subject}_fncb.tar.gz.md5
  curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/${Subject}/NIFTI/${Subject}_strc.tar.gz -o ${DestinationDir}/NIFTI/${Subject}_strc.tar.gz
  curl -u ${Username}:${Password} --ftp-ssl ${Server}/${Project}/${Subject}/NIFTI/${Subject}_strc.tar.gz.md5 -o ${DestinationDir}/NIFTI/${Subject}_strc.tar.gz.md5
  
  #read -p "Press any key to continue..."
done

