#!/bin/sh

USER_ID=

cat > hcp-subjects.$$.txt <<EOF
100307
114924
125525
138231
159239
192439
197550
249947
255639
499566
672756
792564
EOF

for SUBJECT in `cat hcp-subjects.$$.txt`
do
  sftp "$USER_ID@ftp.humanconnectome.org:/OpenAccess/Behavioral_Data/Non-Toolbox_Behavioral_Metrics_2012-10.csv"
  sftp "$USER_ID@ftp.humanconnectome.org:/OpenAccess/Behavioral_Data/Task-fMRI_Behavioral_Metrics_2012-10.csv"
  sftp "$USER_ID@ftp.humanconnectome.org:/OpenAccess/$SUBJECT/$SUBJECT.Task-fMRI_EventFiles.tar.gz"
  sftp "$USER_ID@ftp.humanconnectome.org:/OpenAccess/$SUBJECT/processed/*"
  sftp "$USER_ID@ftp.humanconnectome.org:/OpenAccess/$SUBJECT/NIFTI/*"
done

###################################################
exit
###################################################

The files in the NIFTI directory are raw (oblique).

The processed volumes are not oblique.

You will need to define your registered HCP user ID at the top. 

You will need to enter your password many times.

Feel free to create versions of this file that download only the files you need.
